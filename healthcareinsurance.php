<?php
include('common.php');
class healthcareinsurance extends common {
	public $dbconn;
	public function __construct(PDO $dbconn){
		$this->dbconn = $dbconn;
	}

	/*Function to get the Search Looking for Details*/
	public function funGetLookingFor($returntype = 1) {
		$ArrLookingFor = array("Planning to have kids","Best cover","Basic dental","Major dental");
		return $returntype == 1 ? $ArrLookingFor : $this->funjsonReturn($ArrLookingFor);
	}

	/*Function to get the Search Needs Cover Details*/
	public function funGetNeedsCover($returntype = 1) {
		$ArrNeedsCover = array();
		$Qry="SELECT DISTINCT WS_Category from tbl_policies";
		$getResRow=$this->funExecuteSelectQuery($Qry,$ExecutecondtionArr='');
        if(count($getResRow)>0){
			foreach($getResRow as $getResCover){
				if ($getResCover["WS_Category"]!="Dependants Only" && $getResCover["WS_Category"]!="Family Plus" && $getResCover["WS_Category"]!="Sole Parent Plus") {
					$ArrNeedsCover[]=$getResCover["WS_Category"];
				}
			}
        }
		return $returntype == 1 ? $ArrNeedsCover : $this->funjsonReturn($ArrNeedsCover);
	}

	/*Function to get the Search Types of Cover Details*/
	public function funGetTypesofCover($returntype = 1) {
		$ArrTypesofCover = array();
		$Qry="SELECT DISTINCT WS_CoverType from tbl_policies order by WS_CoverType asc";
        $getResRow=$this->funExecuteSelectQuery($Qry);
        if(count($getResRow)>0){
           	foreach($getResRow as $getResCovertype) {
                switch ($getResCovertype["WS_CoverType"]) {
                    case "Combined":
                       $wscovertype="Hospital + Extras";
                       $ArrTypesofCover[0]=$wscovertype;
                        break;
                    case "Hospital":
                       $wscovertype="Hospital Only";
                       $ArrTypesofCover[1]=$wscovertype;
                        break;
                    case "General":
                        $wscovertype="Extras Only";
                        $ArrTypesofCover[2]=$wscovertype;
                        break;
                    default:
                         $wscovertype="";
						break;
                }
            } 
            ksort($ArrTypesofCover); 
        }
		return $returntype == 1 ? $ArrTypesofCover : $this->funjsonReturn($ArrTypesofCover);
	}

	/*Function to get the Search Types of Cover Details*/
	public function funGetState($returntype = 1) {
		$ArrState = array();
		$Qry="SELECT DISTINCT(WS_State) from tbl_policies";
        $getResRow=$this->funExecuteSelectQuery($Qry,$ExecutecondtionArr='');
        if(count($getResRow)>0){
			foreach($getResRow as $getResState)
				$ArrState[]=$getResState["WS_State"];
        } 
		return $returntype == 1 ? $ArrState : $this->funjsonReturn($ArrState);
	}

	/*Function to get the Search Popup Hospital Details*/
	public function funGetHospitalDetails($returntype = 1) {
		$ArrHospitalDetails = array("Pregnancy","Fertility treatment (IVF)","Heart surgery (cardiac)","Eye surgery (cataract)","Joints (hip, knee, shoulder)","Renal dialysis (kidney)","Weight Loss (gastric)","Sterilisation","Plastic surgery","Rehab treatment","Psychatric","Palliative");
		return $returntype == 1 ? $ArrHospitalDetails : $this->funjsonReturn($ArrHospitalDetails);
	}

	/*Function to get the Search Popup Hospital Details*/
	public function funGetExtrasDetails($returntype = 1) {
		$ArrExtrasDetails = array("Dental (cleaning, filling)","Dental major","Dental (root canal)","Dental (braces)","Optical","Physio","Chiro","Massage","Actupuncture","Hearing aids","Blood Glucose Monitors","Pharmaceutical (non PBS)","Naturopathy","Podiatry (feet)","Psychology");
		return $returntype == 1 ? $ArrExtrasDetails : $this->funjsonReturn($ArrExtrasDetails);
	}

	/*Function to return Search Display Details*/
	public function funSearchDisplayDetails(){
	
		$ArrResult["looking_for"] = $this->funGetLookingFor();
		$ArrResult["needs_cover"] = $this->funGetNeedsCover();
		$ArrResult["type_of_cover"] = $this->funGetTypesofCover();
		$ArrResult["state"] = $this->funGetState();
		$ArrResult["hospital_cover"] = $this->funGetHospitalDetails();
		$ArrResult["extras_cover"] = $this->funGetExtrasDetails();
        $ArrResult["budget"] = $this->funBudgetValues();
        

        return $this->funjsonReturn($ArrResult);
    }

    public function funBudgetValues($returntype = 1){
        $ArrBudget= array();
        $Qry="SELECT truncate(ceil(MAX(WS_MonthlyPremium_StdLessThanAge65Rebate))+10,-1) as max_value, floor(MIN(WS_MonthlyPremium_StdLessThanAge65Rebate)) as min_value from tbl_policies";
        $getResRow=$this->funExecuteSelectQuery($Qry,$ExecutecondtionArr='');
        if(count($getResRow)>0){
            foreach($getResRow as $getResBudget)
            $ArrBudget["min"]=$getResBudget['min_value'];
            $ArrBudget["max"]=$getResBudget['max_value'];
        } 
        return $returntype == 1 ? $ArrBudget : $this->funjsonReturn($ArrBudget);
    }

    /*Function to get the Search Looking for Result Details*/
    public function funSearchListDisplayDetails($filter_selected) {

        $SelectFieldCondition="";
	    $condtionArr   = array();
	    $QryCondition="";

    	$lookingFor = $filter_selected["looking_for"];
	    $needCover  = $filter_selected["needs_class"];
	    $typeCover  = $filter_selected["type_class"];
	    $state      = $filter_selected["state_class"];
	    $min        = $filter_selected["min"];
	    $max        = $filter_selected["max"];
	    $ArraypopupOptions = $filter_selected["popup_options"];
	    $orderBy    =  $filter_selected['order'];
        $pagecount  =  $filter_selected['page_no'];
        $filterbank =  $filter_selected['fund_name'];

        $addwherelookingForCondition=$this->funlookingForCondition($lookingFor,$ArraypopupOptions,$typeCover,$QryCondition,$condtionArr);

        $SelectFieldCondition.=$addwherelookingForCondition["SelectFieldCondition"];
        $QryCondition.=$addwherelookingForCondition["QryCondition"];
        $condtionArr=$addwherelookingForCondition["condtionArr"];

        $addwhereneedCoverCondition=$this->funneedCoverCondition($needCover,$condtionArr);
		$QryCondition.= $addwhereneedCoverCondition["QryCondition"]; 
        $condtionArr  = $addwhereneedCoverCondition["condtionArr"];

        $addwheretypeCoverCondition=$this->funtypeCoverCondition($typeCover,$condtionArr);
        $QryCondition.= $addwheretypeCoverCondition["QryCondition"]; 
        $condtionArr  = $addwheretypeCoverCondition["condtionArr"];

        $addwherestateCondition=$this->funstateCondition($state,$condtionArr);
        $QryCondition.= $addwherestateCondition["QryCondition"]; 
        $condtionArr  = $addwherestateCondition["condtionArr"];

        $addwherebudgetCondition=$this->funbudgetCondition($min,$max,$condtionArr);
        $QryCondition.= $addwherebudgetCondition["QryCondition"]; 
        $condtionArr  = $addwherebudgetCondition["condtionArr"];

        $addfilterbankCondition=$this->funfilterbankCondition($filterbank,$condtionArr);
        $QryCondition.= $addfilterbankCondition["QryCondition"];
        $condtionArr  = $addfilterbankCondition["condtionArr"];
        
        //echo $QryCondition;
        //print_r($condtionArr); exit;


	    if ($pagecount==0 || $pagecount==1){
        	$limits='LIMIT 0,3';
        } else {
        	// if($pagecount!=""){
        	   $page=$pagecount*3;
        	   $limits="LIMIT ".$page.",3";
        	// }
        }
        if ($orderBy=="" || $orderBy=="asc")
	    	$orderby="ASC";
        else 
        	$orderby="DESC";

        switch ($typeCover) {
	        case "Combined":
	           $wscovertype="Hospital + Extras";
	            break;
	        case "Hospital":
	           $wscovertype="Hospital Only";
	            break;
	        case "General":
	            $wscovertype="Extras Only";
	            break;
	        default:
	             $wscovertype="";
				break;
	    }

		$Qry="SELECT ".$SelectFieldCondition."policies.insurance_id, policies.WS_Fund, policies.WS_PolicyUrl, policies.WS_Corporate, policies.WS_MonthlyPremium_StdLessThanAge65Rebate,policies.WS_Status, policies.WS_PolicyName, wspolicies.WS_Hos_Excess_PerHospitalVisit, wspolicies.WS_Hos_CoPayment_SharedRoom, pspolicies.PS_DentalGeneral_BenefitLimits, pspolicies.PS_DentalMajor_BenefitLimits, pspolicies.PS_DentalEndodontic_BenefitLimits, pspolicies.PS_DOrthodontic_BenefitLimits, pspolicies.PS_DOrthodontic_BenefitLimits_Liefetime, pspolicies.PS_Optical_BenefitLimits, pspolicies.PS_Physiotherapy_BenefitLimits, pspolicies.PS_Chiropractic_BenefitLimits, ps2policies.PS_RemedialMassage_BenefitLimits, pspolicies.PS_NonPbsPharma_BenefitLimits, ps2policies.PS_Acupuncture_BenefitLimits, ps2policies.PS_HearingAids_BenefitLimits, ps2policies.PS_BloodGlucoseMonitors_BenefitLimits, ps2policies.PS_Naturopathy_BenefitLimits, ps2policies.PS_Podiatry_BenefitLimits,ps2policies.PS_Psychology_BenefitLimits, ps2policies.PS_Ambulance_BenefitLimits, pspolicies.PS_DentalGeneral_MaxBenefits_PeriodicOralExamination_Value, pspolicies.PS_DentalGeneral_MaxBenefits_ScaleAndClean_Value, pspolicies.PS_DentalGeneral_MaxBenefits_FluorideTreatment_Value, pspolicies.PS_DentalGeneral_MaxBenefits_SurgicalToothExtraction_Value, pspolicies.PS_DentalMajor_MaxBenefits_SurgicalToothExtraction_Value, pspolicies.PS_DentalMajor_MaxBenefits_FullCrownVeneered_Value, pspolicies.PS_DentalEndodontic_MaxBenefits_FillingOfOneRootCanal_Value, pspolicies.PS_DOrthodontic_MaxBenefits_BracesRemovalFittingRetainer_Value, pspolicies.PS_Optical_MaxBenefits_SingleVisionLensesAndFrames_Value, pspolicies.PS_Optical_MaxBenefits_MultiFocalLensesAndFrames_Value, pspolicies.PS_Physiotherapy_MaxBenefits_InitiaVisit_Value, pspolicies.PS_Physiotherapy_MaxBenefits_SubsequentVisit_Value, pspolicies.PS_Chiropractic_MaxBenefits_InitiaVisit_Value, pspolicies.PS_Chiropractic_MaxBenefits_SubsequentVisit_Value, ps2policies.PS_RemedialMassage_MaxBenefits_InitiaVisit_Value, ps2policies.PS_RemedialMassage_MaxBenefits_SubsequentVisit_Value, ps2policies.PS_Acupuncture_MaxBenefits_InitiaVisit_Value, ps2policies.PS_Acupuncture_MaxBenefits_SubsequentVisit_Value, ps2policies.PS_HearingAids_MaxBenefits_PerHearingAid_Value, ps2policies.PS_BloodGlucoseMonitors_MaxBenefits_PerMonitor_Value, pspolicies.PS_NonPbsPharma_MaxBenefits_PerEligiblePrescription_Value, ps2policies.PS_Naturopathy_MaxBenefits_InitiaVisit_Value, ps2policies.PS_Naturopathy_MaxBenefits_SubsequentVisit_Value, ps2policies.PS_Podiatry_MaxBenefits_InitiaVisit_Value, ps2policies.PS_Podiatry_MaxBenefits_SubsequentVisit_Value, ps2policies.PS_Psychology_MaxBenefits_InitiaVisit_Value, ps2policies.PS_Psychology_MaxBenefits_SubsequentVisit_Value FROM tbl_policies as policies INNER JOIN tbl_ws_policies as wspolicies ON wspolicies.insurance_id=policies.insurance_id INNER JOIN tbl_ps1_policies as pspolicies ON pspolicies.insurance_id=policies.insurance_id INNER JOIN tbl_ps2_policies as ps2policies ON ps2policies.insurance_id=policies.insurance_id where policies.insurance_id<>''".$QryCondition." order by policies.WS_MonthlyPremium_StdLessThanAge65Rebate ".$orderby." ".$limits."";
    	$ArrListDisplay= array();
    	$ArrayCovered=array("1"=>"Covered","0"=>"Not Covered",""=>"n&#47a");
    	$ArrayStatus=array("1"=>"","0"=>"Restricted Fund");
    	$ArrayCorporate=array("1"=>"Corporate Product","0"=>"");
    	$getResRow=$this->funExecuteSelectQuery($Qry,$condtionArr);
    	//print_r($getResRow);
    	if (!empty($getResRow)) {
	    	$policies_count=count($getResRow);
	    	if(count($getResRow)>0){
	    		foreach($getResRow as $getResList){
	    			/******Bank Details***************/
	    			$insurance_id=$getResList["insurance_id"];
	                $ws_fund  = $getResList["WS_Fund"];
	                $WS_PolicyUrl  = $getResList["WS_PolicyUrl"];
	                $WS_Status = $getResList["WS_Status"];
	                $ws_corporate=$ArrayStatus[$WS_Status];                 
	                $WS_Corporate=$getResList["WS_Corporate"];                 
	                $Corporate =$ArrayCorporate[$WS_Corporate];                 
	                if($WS_Corporate=="1"){  
	                	if ($ws_corporate!="")                 
	                		$ws_corporate= $ws_corporate." &#47 ".$Corporate;  
	                	else
	                		$ws_corporate= $Corporate;               
	                } else {
	                	$ws_corporate= $ws_corporate;
	                }
	                $monthly_premium =  checknull(round($getResList["WS_MonthlyPremium_StdLessThanAge65Rebate"]),'amount');
	                $policy_name=$getResList["WS_PolicyName"];
	                $perhospital_visit=checknull($getResList["WS_Hos_Excess_PerHospitalVisit"]);
	                $co_payment= checknull($getResList["WS_Hos_CoPayment_SharedRoom"]);

					/**To Get Hospital Cover value**/
					$pregnancy=checknull($getResList["WS_Hos_PregnancyAndBirthRelatedServices_Covered"]);
					$fertility_treatment=checknull($getResList["WS_Hos_AssistedReproductiveServices_Covered"]); 
					$heart_surgery=checknull($getResList["WS_Hos_CardiacAndRelatedServices_Covered"]); 
					$eye_surgery=checknull($getResList["WS_Hos_CataractAndEyeLensProcedures_Covered"]); 
					$joints= checknull($getResList["WS_Hos_JointReplacement_HipKneeShoulderAndElbow_Covered"]); 
					$renal_dialysis=checknull($getResList["WS_Hos_DialysisForChronicRenalFailure_Covered"]); 
					$weight_loss=checknull($getResList["WS_Hos_GastricBandingAndRelatedServices_Covered"]);
					$plastic_surgery=checknull($getResList["WS_Hos_NonCosmeticPlasticSurgery_Covered"]); 
					$sterilisation=checknull($getResList["WS_Hos_Sterilisation_Covered"]); 
					$rehab_treatment=checknull($getResList["WS_Hos_Rehabilitation_Covered"]); 
					$psychatric=checknull($getResList["WS_Hos_PsychiatricServices_Covered"]); 
					$palliative_care=checknull($getResList["WS_Hos_PalliativeCare_Covered"]); 

					/**To Get Extras Cover value**/
					$dental_cleaning= $getResList["PS_DentalGeneral_Covered"]; 
					$dental_cleaning=$ArrayCovered[$dental_cleaning];
					$dental_major= $getResList["PS_DentalMajor_Covered"]; 
					$dental_major=$ArrayCovered[$dental_major];
					$dental_root= $getResList["PS_DentalEndodontic_Covered"];
					$dental_root=$ArrayCovered[$dental_root]; 
					$dental_braces= $getResList["PS_DentalOrthodontic_Covered"];
					$dental_braces=$ArrayCovered[$dental_braces]; 
					$optical= $getResList["PS_Optical_Covered"];
					$optical=$ArrayCovered[$optical]; 
					$physio= $getResList["PS_Physiotherapy_Covered"]; 
					$physio=$ArrayCovered[$physio];
					$chiro= $getResList["PS_Chiropractic_Covered"]; 
					$chiro=$ArrayCovered[$chiro];
					$massages= $getResList["PS_RemedialMassage_Covered"]; 
					$massages=$ArrayCovered[$massages];
					$acupunture= $getResList["PS_Acupuncture_Covered"];
					$acupunture=$ArrayCovered[$acupunture]; 
					$hearing_aids= $getResList["PS_HearingAids_Covered"];  
					$hearing_aids=$ArrayCovered[$hearing_aids];
					$blood_glucose_monitors= $getResList["PS_BloodGlucoseMonitors_Covered"];
					$blood_glucose_monitors=$ArrayCovered[$blood_glucose_monitors]; 
					$pharmaceutical= $getResList["PS_NonPbsPharma_Covered"];
					$pharmaceutical=$ArrayCovered[$pharmaceutical]; 
					$naturopathy= $getResList["PS_Naturopathy_Covered"]; 
					$naturopathy=$ArrayCovered[$naturopathy];
					$podiatry= $getResList["PS_Podiatry_Covered"]; 
					$podiatry=$ArrayCovered[$podiatry];
					$psychology= $getResList["PS_Psychology_Covered"];
					$psychology=$ArrayCovered[$psychology];

					/****To get value A1 to A16****/
					$annualdental_cleaning= breakAdded(checknull($getResList["PS_DentalGeneral_BenefitLimits"])); 
					$annualdental_major= breakAdded(checknull($getResList["PS_DentalMajor_BenefitLimits"])); 
					$annualdental_root= breakAdded(checknull($getResList["PS_DentalEndodontic_BenefitLimits"])); 
					$annualdental_braceslimit=breakAdded(checknull($getResList["PS_DOrthodontic_BenefitLimits"])); 
					$annualdental_bracestime=breakAdded(checknull($getResList["PS_DOrthodontic_BenefitLimits_Liefetime"]));
					$annualdental_braces=$annualdental_braceslimit." ".$annualdental_bracestime;
					$annualoptical= breakAdded(checknull($getResList["PS_Optical_BenefitLimits"])); 
					$annualphysio= breakAdded(checknull($getResList["PS_Physiotherapy_BenefitLimits"])); 
					$annualchiro= breakAdded(checknull($getResList["PS_Chiropractic_BenefitLimits"])); 
					$annualmassages= breakAdded(checknull($getResList["PS_RemedialMassage_BenefitLimits"])); 
					$annualacupunture= breakAdded(checknull($getResList["PS_Acupuncture_BenefitLimits"])); 
					$annualhearing_aids= breakAdded(checknull($getResList["PS_HearingAids_BenefitLimits"])); 
					$annualblood_glucose= breakAdded(checknull($getResList["PS_BloodGlucoseMonitors_BenefitLimits"]));
					$annualpharmaceutical= breakAdded(checknull($getResList["PS_NonPbsPharma_BenefitLimits"]));  
					$annualnaturopathy= breakAdded(checknull($getResList["PS_Naturopathy_BenefitLimits"])); 
					$annualpodiatry= breakAdded(checknull($getResList["PS_Podiatry_BenefitLimits"]));
					$annualpsychology= breakAdded(checknull($getResList["PS_Psychology_BenefitLimits"])); 
					//$annualambulance= $getResList["PS_Ambulance_BenefitLimits"]; 
	               
					/****To get value P1 to P15****/
					$servicegeneralone= checknull($getResList["PS_DentalGeneral_MaxBenefits_PeriodicOralExamination_Value"],"amount"); 
					$servicegeneraltwo= checknull($getResList["PS_DentalGeneral_MaxBenefits_ScaleAndClean_Value"],"amount"); 
					$servicegeneralthree= checknull($getResList["PS_DentalGeneral_MaxBenefits_FluorideTreatment_Value"],"amount");  
					$servicegeneralfour= checknull($getResList["PS_DentalGeneral_MaxBenefits_SurgicalToothExtraction_Value"],"amount"); 
					$servicedentalmajorp1= checknull($getResList["PS_DentalMajor_MaxBenefits_SurgicalToothExtraction_Value"],"amount");  
					$servicedentalmajorp2= checknull($getResList["PS_DentalMajor_MaxBenefits_FullCrownVeneered_Value"],"amount");  
					$servicedental_root= checknull($getResList["PS_DentalEndodontic_MaxBenefits_FillingOfOneRootCanal_Value"],"amount");  
					$servicedentalbraces= checknull($getResList["PS_DOrthodontic_MaxBenefits_BracesRemovalFittingRetainer_Value"],"amount"); 
					$serviceptical_singleframe= checknull($getResList["PS_Optical_MaxBenefits_SingleVisionLensesAndFrames_Value"],"amount");  
					$serviceptical_multiframe= checknull($getResList["PS_Optical_MaxBenefits_MultiFocalLensesAndFrames_Value"],"amount"); 
					$servicephysio_initial= checknull($getResList["PS_Physiotherapy_MaxBenefits_InitiaVisit_Value"],"amount");  
					$servicephysio_subsequent= checknull($getResList["PS_Physiotherapy_MaxBenefits_SubsequentVisit_Value"],"amount"); 
					$servicechiro_initial= checknull($getResList["PS_Chiropractic_MaxBenefits_InitiaVisit_Value"],"amount");  
					$servicechiro_subsequent= checknull($getResList["PS_Chiropractic_MaxBenefits_SubsequentVisit_Value"],"amount"); 
					$servicemassage_initial= checknull($getResList["PS_RemedialMassage_MaxBenefits_InitiaVisit_Value"],"amount");  
					$servicemassage_subsequent= checknull($getResList["PS_RemedialMassage_MaxBenefits_SubsequentVisit_Value"],"amount"); 
	                $serviceacupuncture_initial= checknull($getResList["PS_Acupuncture_MaxBenefits_InitiaVisit_Value"],"amount");  
					$serviceacupuncture_subsequent= checknull($getResList["PS_Acupuncture_MaxBenefits_SubsequentVisit_Value"],"amount"); 
					$service_hearingaids= checknull($getResList["PS_HearingAids_MaxBenefits_PerHearingAid_Value"],"amount");  
					$serviceblood_glucose_monitors= checknull($getResList["PS_BloodGlucoseMonitors_MaxBenefits_PerMonitor_Value"],"amount"); 
					$servicenonpbspharma= checknull($getResList["PS_NonPbsPharma_MaxBenefits_PerEligiblePrescription_Value"],"amount"); 
					$servicenaturo_initial= checknull($getResList["PS_Naturopathy_MaxBenefits_InitiaVisit_Value"],"amount");  
					$servicenaturo_subsequent= checknull($getResList["PS_Naturopathy_MaxBenefits_SubsequentVisit_Value"],"amount"); 
					$servicepodiatry_initial= checknull($getResList["PS_Podiatry_MaxBenefits_InitiaVisit_Value"],"amount");  
					$servicepodiatry_subsequent= checknull($getResList["PS_Podiatry_MaxBenefits_SubsequentVisit_Value"],"amount"); 
					$servicepsychology_initial= checknull($getResList["PS_Psychology_MaxBenefits_InitiaVisit_Value"],"amount"); 
					$servicepsychology_subsequent= checknull($getResList["PS_Psychology_MaxBenefits_SubsequentVisit_Value"],"amount"); 

					$ArrListDisplay["policies_count"]=$policies_count;
					if (strpos($needCover, '-') !== false) {
				      $needCoverArr = explode('-', $needCover);
				      $ArrListDisplay["need_cover"] = $needCoverArr[0];
				    } else{
				      $ArrListDisplay["need_cover"] = $needCover;
				    }
					$ArrListDisplay["type_cover"]=$wscovertype;
					$ArrListDisplay["state"]=$state;
					$ArrListDisplay["min"]= checknull($min,'amount');
					$ArrListDisplay["max"]=checknull($max,'amount');

					$ArrListDisplay["funds"][]=
					    array(
							"fund_provider"=>$ws_fund,
							"policy_url"=>$WS_PolicyUrl,
							"fund_type"=>$ws_corporate,
							"monthly_premium"=>$monthly_premium,
							"policy_name"=>$policy_name,
							"per_visit"=>$perhospital_visit,
							"co_payment"=>$co_payment,
							"hospital_cover"=>array(
							"pregnancy"=>$pregnancy,
							"fertility_treatment"=>$fertility_treatment,
							"heart_surgery"=>$heart_surgery,
		                    "eye_surgery"=>$eye_surgery,
				            "joints"=>$joints,
				            "renal_dialysis"=>$renal_dialysis,
				            "weight_loss"=>$weight_loss,
				            "sterilisation"=>$sterilisation,
				            "plastic_surgery"=>$plastic_surgery,
				            "rehab_treatment"=>$rehab_treatment,
				            "psychatric"=>$psychatric,
				            "palliative_care"=>$palliative_care
			            ),
					    "extras_cover"=>
					    	array(
					    		"covered"=>array(
								"dental_cleaning"=>$dental_cleaning,
				                "dental_major"=>$dental_major,
				                "dental_root"=>$dental_root,
				                "dental_braces"=>$dental_braces,
				                "optical"=>$optical,
				                "physio"=>$physio,
				                "chiro"=>$chiro,
				                "massages"=>$massages,
				                "acupunture"=>$acupunture,
				                "hearing_aids"=>$hearing_aids,
				                "blood_glucose_monitors"=>$blood_glucose_monitors,
				                "pharmaceutical"=>$pharmaceutical,
				                "naturopathy"=>$naturopathy,
				                "podiatry"=>$podiatry,
				                "psychology"=>$psychology
					            ),
					            "annual_limit"=>array(
					               "dental_cleaning"=>$annualdental_cleaning,
					               "dental_major"=>$annualdental_major,
					               "dental_root"=>$annualdental_root,
					               "dental_braces"=>$annualdental_braces,
					               "optical"=>$annualoptical,
					               "physio"=>$annualphysio,
					               "chiro"=>$annualchiro,
					               "massages"=>$annualmassages,
					               "acupunture"=>$annualacupunture,
					               "hearing_aids"=>$annualhearing_aids,
					               "blood_glucose_monitors"=>$annualblood_glucose,
					               "pharmaceutical"=>$annualpharmaceutical,
					               "naturopathy"=>$annualnaturopathy,
					               "podiatry"=>$annualpodiatry,
					               "psychology"=>$annualpsychology
					            ),
					            "service_limit"=>array(
					            	"dental_cleaning"=>array(
					                  "Cleaning"=>$servicegeneralone,
					                  "Fillings"=>$servicegeneraltwo,
					                  "Fluoride treatment"=>$servicegeneralthree,
					                  "Surgical"=>$servicegeneralfour
					                ),
					                "dental_major"=>array(
					                  "Surgical"=>$servicedentalmajorp1,
					                  "Crown"=>$servicedentalmajorp2
					                ),
					                "dental_root"=>array( 
					                  "1 root canal"=>$servicedental_root
					               ),
					               "dental_braces"=>array(  
					                  "Braces"=>$servicedentalbraces
					               ),
					               "optical"=>array( 
					                  "Single vision"=>$serviceptical_singleframe,
					                  "Multi-focal"=>$serviceptical_multiframe
					               ),
					               "physio"=>array( 
					                  "Initial visit"=>$servicephysio_initial,
					                  "Subsequent visit"=>$servicephysio_subsequent
					               ),
					               "chiro"=>array(  
					                  "Initial visit"=>$servicechiro_initial,
					                  "Subsequent visit"=>$servicechiro_subsequent
					               ),
					               "massages"=>array(  
					                  "Initial visit"=>$servicemassage_initial,
					                  "Subsequent visit"=>$servicemassage_subsequent
					               ),
					               "acupunture"=>array(  
					                  "Initial visit"=>$serviceacupuncture_initial,
					                  "Subsequent visit"=>$serviceacupuncture_subsequent
					               ),
					               "hearing_aids"=>array(  
					                  "Per hearing aid"=>$service_hearingaids
					               ),
					               "blood_glucose_monitors"=>array( 
					                  "Per monitor"=>$serviceblood_glucose_monitors
					               ),
					               "pharmaceutical"=>array(  
					                  "Per prescription"=>$servicenonpbspharma
					                ),
					               "naturopathy"=>array( 
					                  "Initial visit"=>$servicenaturo_initial,
					                  "Subsequent visit"=>$servicenaturo_subsequent
					                ),
					               "podiatry"=>array(  
					                  "Initial visit"=>$servicepodiatry_initial,
					                  "Subsequent visit"=>$servicepodiatry_subsequent
					                ),
					               "psychology"=>array( 
					                  "Initial visit"=>$servicepsychology_initial,
					                  "Subsequent visit"=>$servicepsychology_subsequent
					                ),
								),
					    	),
			        );
				
				}

	            return $this->funjsonReturn($ArrListDisplay);
			}
		} else {
			$ArrListDisplay["policies_count"]=0;
			if (strpos($needCover, '-') !== false) {
		      $needCoverArr = explode('-', $needCover);
		      $ArrListDisplay["need_cover"] = $needCoverArr[0];
		    } else{
		      $ArrListDisplay["need_cover"] = $needCover;
		    }
			$ArrListDisplay["type_cover"]=$wscovertype;
			$ArrListDisplay["state"]=$state;
			$ArrListDisplay["min"]= '$0';
			$ArrListDisplay["max"]=checknull($max,'amount');
			$ArrListDisplay["message"]="No record(s) found";
			return $this->funjsonReturn($ArrListDisplay);
		}
    }

    public function funlookingForCondition($lookingFor,$ArraypopupOptions,$typeCover='',$QryCondition='',$condtionArr=''){

    	$ArrlookingforQry=array("Planning to have kids"=>"policies.WS_Hos_PregnancyAndBirthRelatedServices_Covered","Basic dental"=>"policies.PS_DentalGeneral_Covered","Major dental"=>"policies.PS_DentalMajor_Covered");
  		$ArrlookSearchQry=array("Planning to have kids"=>":planningkids","Basic dental"=>":basicdental","Major dental"=>":majordental");
  		$ArrlookvaluesQry=array("Planning to have kids"=>"Covered","Basic dental"=>"1","Major dental"=>"1");
  
	    if ($lookingFor!="Best cover" && $lookingFor!="More options") {
	    	$lookingForFieldQry=$ArrlookingforQry[$lookingFor];
	    	$lookingForSeachQry=$ArrlookSearchQry[$lookingFor];
	    	$lookingForvalueQry=$ArrlookvaluesQry[$lookingFor];
	    }
  
    	$ArrHospitalExtrasQry= array(
	      "Pregnancy"=>" AND policies.WS_Hos_PregnancyAndBirthRelatedServices_Covered='Covered'",
	      "Fertility treatment (IVF)"=>" AND policies.WS_Hos_AssistedReproductiveServices_Covered ='Covered'",
	      "Heart surgery (cardiac)"=>" AND policies.WS_Hos_CardiacAndRelatedServices_Covered='Covered'",
	      "Eye surgery (cataract)"=>" AND policies.WS_Hos_CataractAndEyeLensProcedures_Covered='Covered'",
	      "Joints (hip, knee, shoulder)"=>" AND policies.WS_Hos_JointReplacement_HipKneeShoulderAndElbow_Covered='Covered'",
	      "Renal dialysis (kidney)"=>" AND policies.WS_Hos_DialysisForChronicRenalFailure_Covered='Covered'",
	      "Weight Loss (gastric)"=>" AND policies.WS_Hos_GastricBandingAndRelatedServices_Covered='Covered'",
	      "Sterilisation"=>" AND policies.WS_Hos_Sterilisation_Covered='Covered'",
	      "Plastic surgery"=>" AND policies.WS_Hos_NonCosmeticPlasticSurgery_Covered='Covered'",
	      "Rehab treatment"=>"  AND policies.WS_Hos_Rehabilitation_Covered='Covered'",
	      "Psychatric"=>" AND policies.WS_Hos_PsychiatricServices_Covered='Covered'",
	      "Palliative"=>" AND policies.WS_Hos_PalliativeCare_Covered='Covered'",
	      "Dental (cleaning, filling)"=>"AND policies.PS_DentalGeneral_Covered='1'",
	      "Dental major"=>" AND policies.PS_DentalMajor_Covered='1'",
	      "Dental (root canal)"=>" AND policies.PS_DentalEndodontic_Covered='1'",
	      "Dental (braces)"=>" AND policies.PS_DentalOrthodontic_Covered='1'",
	      "Optical"=>" AND policies.PS_Optical_Covered='1'",
	      "Physio"=>" AND policies.PS_Physiotherapy_Covered='1'",
	      "Chiro"=>" AND policies.PS_Chiropractic_Covered='1'",
	      "Massage"=>" AND policies.PS_RemedialMassage_Covered='1'",
	      "Hearing aids"=>" AND policies.PS_HearingAids_Covered='1'",
	      "Actupuncture"=>" AND policies.PS_Acupuncture_Covered='1'",
	      "Blood Glucose Monitors"=>" AND  policies.PS_BloodGlucoseMonitors_Covered='1'",
	      "Pharmaceutical (non PBS)"=>" AND policies.PS_NonPbsPharma_Covered='1'",
	      "Naturopathy"=>" AND policies.PS_Naturopathy_Covered='1'",
	      "Podiatry (feet)"=>" AND policies.PS_Podiatry_Covered='1'",
	      "Psychology"=>" AND policies.PS_Psychology_Covered='1'"
        );
        if (($lookingFor=='More options') && ($ArraypopupOptions!='empty')) {    
		    if ($typeCover=="Combined") {
		      	foreach($ArraypopupOptions as $key=>$popoptions){
		        	$QryCondition=$ArrHospitalExtrasQry[$popoptions];
		      	}
		    }
	    	if ($typeCover=="General") {
			    foreach($ArraypopupOptions as $key=>$popoptions){
			        $QryCondition=$ArrHospitalExtrasQry[$popoptions];
			    }
	    	}
	    	if ($typeCover=="Hospital") {
	      		foreach($ArraypopupOptions as $key=>$popoptions){
	        		$QryCondition=$ArrHospitalExtrasQry[$popoptions];
	      		}
	    	}
  		}
  
  		if ($lookingFor!="Best cover" && $lookingFor!="More options") {
    		$QryCondition=" AND ".$lookingForFieldQry."=".$lookingForSeachQry."";
    		$condtionArr[$lookingForSeachQry]   = $lookingForvalueQry;
  		}
		if ($lookingFor=="Best cover") {
    
    		$QryCondition.=" AND policies.WS_Hos_PregnancyAndBirthRelatedServices_Covered='Covered' AND policies.WS_Hos_AssistedReproductiveServices_Covered ='Covered' AND policies.WS_Hos_CardiacAndRelatedServices_Covered='Covered' AND policies.WS_Hos_CataractAndEyeLensProcedures_Covered='Covered' AND policies.WS_Hos_JointReplacement_HipKneeShoulderAndElbow_Covered='Covered' AND policies.WS_Hos_DialysisForChronicRenalFailure_Covered='Covered' AND policies.WS_Hos_GastricBandingAndRelatedServices_Covered='Covered' AND policies.WS_Hos_NonCosmeticPlasticSurgery_Covered='Covered' AND policies.WS_Hos_Sterilisation_Covered='Covered' AND policies.WS_Hos_Rehabilitation_Covered='Covered' AND policies.WS_Hos_PsychiatricServices_Covered='Covered' AND policies.WS_Hos_PalliativeCare_Covered='Covered' AND policies.PS_DentalGeneral_Covered='1' AND policies.PS_DentalMajor_Covered='1' AND policies.PS_DentalEndodontic_Covered='1' AND policies.PS_DentalOrthodontic_Covered='1' AND policies.PS_Optical_Covered='1' AND policies.PS_Physiotherapy_Covered='1' AND policies.PS_Chiropractic_Covered='1' AND policies.PS_RemedialMassage_Covered='1' AND policies.PS_Acupuncture_Covered='1' AND policies.PS_HearingAids_Covered='1' AND  policies.PS_BloodGlucoseMonitors_Covered='1' AND policies.PS_NonPbsPharma_Covered='1' AND policies.PS_Naturopathy_Covered='1' AND policies.PS_Podiatry_Covered='1' AND policies.PS_Psychology_Covered='1'";
    	}

    	$Arrlookingfor=array("Planning to have kids"=>"policies.WS_Hos_PregnancyAndBirthRelatedServices_Covered,","Basic dental"=>"policies.PS_DentalGeneral_Covered,","Major dental"=>"policies.PS_DentalMajor_Covered,");

	    if ($lookingFor!="Best cover" && $lookingFor!="More options") {
		    $SelectFieldCondition.=$Arrlookingfor[$lookingFor];
		}

	    if ($lookingFor=="Best cover") {
	       $SelectFieldCondition.="policies.WS_Hos_PregnancyAndBirthRelatedServices_Covered, policies.WS_Hos_AssistedReproductiveServices_Covered, policies.WS_Hos_CardiacAndRelatedServices_Covered, policies.WS_Hos_CataractAndEyeLensProcedures_Covered, policies.WS_Hos_JointReplacement_HipKneeShoulderAndElbow_Covered, policies.WS_Hos_DialysisForChronicRenalFailure_Covered, policies.WS_Hos_GastricBandingAndRelatedServices_Covered,policies.WS_Hos_NonCosmeticPlasticSurgery_Covered, policies.WS_Hos_Sterilisation_Covered, policies.WS_Hos_Rehabilitation_Covered, policies.WS_Hos_PsychiatricServices_Covered, policies.WS_Hos_PalliativeCare_Covered, policies.PS_DentalGeneral_Covered, policies.PS_DentalMajor_Covered, policies.PS_DentalEndodontic_Covered, policies.PS_DentalOrthodontic_Covered, policies.PS_Optical_Covered, policies.PS_Physiotherapy_Covered, policies.PS_Chiropractic_Covered, policies.PS_RemedialMassage_Covered, policies.PS_Acupuncture_Covered, policies.PS_HearingAids_Covered,  policies.PS_BloodGlucoseMonitors_Covered, policies.PS_NonPbsPharma_Covered, policies.PS_Naturopathy_Covered, policies.PS_Podiatry_Covered, policies.PS_Psychology_Covered,";
	    }

	    $ArrHospitalExtrasDetails = array(
	      "Pregnancy"=>"policies.WS_Hos_PregnancyAndBirthRelatedServices_Covered,",
	      "Fertility treatment (IVF)"=>"policies.WS_Hos_AssistedReproductiveServices_Covered ,",
	      "Heart surgery (cardiac)"=>"policies.WS_Hos_CardiacAndRelatedServices_Covered,",
	      "Eye surgery (cataract)"=>"policies.WS_Hos_CataractAndEyeLensProcedures_Covered,",
	      "Joints (hip, knee, shoulder)"=>"policies.WS_Hos_JointReplacement_HipKneeShoulderAndElbow_Covered,",
	      "Renal dialysis (kidney)"=>"policies.WS_Hos_DialysisForChronicRenalFailure_Covered,",
	      "Weight Loss (gastric)"=>"policies.WS_Hos_GastricBandingAndRelatedServices_Covered,",
	      "Sterilisation"=>"policies.WS_Hos_Sterilisation_Covered,",
	      "Plastic surgery"=>"policies.WS_Hos_NonCosmeticPlasticSurgery_Covered,",
	      "Rehab treatment"=>"policies.WS_Hos_Rehabilitation_Covered,",
	      "Psychatric"=>"policies.WS_Hos_PsychiatricServices_Covered,",
	      "Palliative"=>"policies.WS_Hos_PalliativeCare_Covered,",
	      "Dental (cleaning, filling)"=>"policies.PS_DentalGeneral_Covered,",
	      "Dental major"=>"policies.PS_DentalMajor_Covered,",
	      "Dental (root canal)"=>"policies.PS_DentalEndodontic_Covered,",
	      "Dental (braces)"=>"policies.PS_DentalOrthodontic_Covered,",
	      "Optical"=>"policies.PS_Optical_Covered,",
	      "Physio"=>"policies.PS_Physiotherapy_Covered,",
	      "Chiro"=>"policies.PS_Chiropractic_Covered,",
	      "Massage"=>"policies.PS_RemedialMassage_Covered,",
	      "Hearing aids"=>"policies.PS_HearingAids_Covered,",
	      "Actupuncture"=>"policies.PS_Acupuncture_Covered,",
	      "Blood Glucose Monitors"=>"policies.PS_BloodGlucoseMonitors_Covered,",
	      "Pharmaceutical (non PBS)"=>"policies.PS_NonPbsPharma_Covered,",
	      "Naturopathy"=>"policies.PS_Naturopathy_Covered,",
	      "Podiatry (feet)"=>"policies.PS_Podiatry_Covered,",
	      "Psychology"=>"policies.PS_Psychology_Covered,"
	    );

	    if (($lookingFor=='More options') && ($ArraypopupOptions!='empty')) {    
		    if ($typeCover=="Combined") {
		      	foreach($ArraypopupOptions as $key=>$pop_options){
		        	$SelectFieldCondition.=$ArrHospitalExtrasDetails[$pop_options];
		      	}
		    }
		    if ($typeCover=="General") {
		      	foreach($ArraypopupOptions as $key=>$pop_options){
		        	$SelectFieldCondition.=$ArrHospitalExtrasDetails[$pop_options];
		      	}
		    }
		    if ($typeCover=="Hospital") {
		      	foreach($ArraypopupOptions as $key=>$pop_options){
		        	$SelectFieldCondition.=$ArrHospitalExtrasDetails[$pop_options];
		      	}
		    }
	    }
        return array("SelectFieldCondition"=>$SelectFieldCondition,"QryCondition"=>$QryCondition,"condtionArr"=>$condtionArr);
    }

    public function funneedCoverCondition($needCover,$condtionArr=''){
        if ($needCover!="") {
        	if (strpos($needCover, '-') !== false) {
		      $needCoverArr = explode('-', $needCover);
		      $QryCondition.=" AND (WS_Category=:WS_Category";
		      $condtionArr[":WS_Category"]   = $needCoverArr[0];
		      $QryCondition.=" OR WS_Category=:WS_Category_1)";
		      $condtionArr[":WS_Category_1"]   = $needCoverArr[1];
		    } else{
		      $QryCondition.=" AND WS_Category=:WS_Category";
		      $condtionArr[":WS_Category"]   = $needCover;
		    }
	    }

	    return array("QryCondition"=>$QryCondition,"condtionArr"=>$condtionArr);
    }

    public function funtypeCoverCondition($typeCover,$condtionArr=''){
        if ($typeCover!="") {
		    $QryCondition=" AND policies.WS_CoverType=:WS_CoverType";
		    $condtionArr[":WS_CoverType"]   = $typeCover;
		}
        return array("QryCondition"=>$QryCondition,"condtionArr"=>$condtionArr);
    }

    public function funstateCondition($state,$condtionArr=''){
        if ($state!="") {
		    $QryCondition=" AND policies.WS_State=:WS_State";
		    $condtionArr[":WS_State"]   = $state;
		}
	    return array("QryCondition"=>$QryCondition,"condtionArr"=>$condtionArr);
    }

    public function funbudgetCondition($min,$max,$condtionArr=''){
	    if ($min!="" && $max!="") {
			$QryCondition.=" AND (policies.WS_MonthlyPremium_StdLessThanAge65Rebate >=:WS_min and policies.WS_MonthlyPremium_StdLessThanAge65Rebate <=:WS_max)";
			$condtionArr[":WS_min"]   = $min;
			$condtionArr[":WS_max"]   = $max;
		}
    	return array("QryCondition"=>$QryCondition,"condtionArr"=>$condtionArr);
    }

    public function funfilterbankCondition($filterbank,$condtionArr=''){
        if ($filterbank!="") {
            $QryCondition.=" And policies.WS_Fund like :fundname";
		    $condtionArr[":fundname"]   = "%".$filterbank."%";
        }
        return array("QryCondition"=>$QryCondition,"condtionArr"=>$condtionArr);
    }
}
function checknull($checkval,$type='') {
	if((!empty($checkval)) && ($type=="amount")) {
		return "$".$checkval;
	}
	return (!(empty($checkval)))? $checkval : "n&#47a";
}
function breakAdded($sampleStr){
	$ExplodeStr = explode("(s", $sampleStr);
	$result="";
    if (count($ExplodeStr)>1) {
        $string1 = $ExplodeStr[0];
        $string2 = $ExplodeStr[1];
        $Str2 = str_replace("(","<br>(",$string2);
        return $result = $string1."(s".$Str2;
    } else {
        return $result = str_replace("(","<br>(",$sampleStr);
    }
}
?>