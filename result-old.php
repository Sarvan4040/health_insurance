<?php
include("includes/configure.php");
require_once('api.php');

// if(isset($_POST['filter_selected'])){
	// $filter_selected = json_decode($_POST['filter_selected'],true);
	// print_r($filter_selected);echo "<br>";
	// $popup_selected = $_POST['popup_selected'];
	// print_r(json_decode($popup_selected));

	$resultArr = '{"policies_count":"30","need_cover":"Single","state":"NSW","type_cover":"Combined","min":"$300","max":"$450","funds":[{"fund_provider":"MEDIBANK","fund_type":"Open fund","monthly_premium":"$413","policy_name":"Top Hospital Essential","per_visit":"$500","co_payment":"n/a","hospital_cover":{"pregnancy":"Covered","fertility_treatment":"Covered","heart_surgery":"Covered","eye_surgery":"Covered","joints":"Covered","renal_dialysis":"Covered","weight_loss":"Covered","sterilisation":"Covered","plastic_surgery":"Not Covered","rehab_treatment":"Not Covered","psychatric":"Restrictions apply","palliative_care":"Not Covered"},"extras_cover":{"covered":{"dental_cleaning":"Covered","dental_major":"Covered","dental_root":"Covered","dental_braces":"Covered","optical":"Not Covered","physio":"Not Covered","chiro":"Covered","massages":"Covered","acupunture":"Covered","hearing_aids":"Covered","blood_glucose_monitors":"Not Covered","pharmaceutical":"Covered","naturopathy":"Covered","podiatry":"Covered","psychology":"Covered"},"annual_limit":{"dental_cleaning":"No annual limit (Sub-limits apply)","dental_major":"$1,200 per person (combined limit for major dental & endodontic - Sub-limit apply)","dental_root":"$1,200 per person (combined limit for major dental & endodontic - Sub-limit apply)","dental_braces":"$900 per person $2,800 lifetime limit","optical":"$280 per person (Sub-limits apply)","physio":"$750 per person (Sub-limits apply)","chiro":"$700 per person up to $1,400 per policy (Sub-limits apply)","massages":"$500 per person (combined limit for acupunture, naturopathy, remedial massage & other services Sub-limit apply","acupunture":"$500 per person (combined limit for acupunture, naturopathy, remedial massage & other services Sub-limit apply","hearing_aids":"$850 per person 1 appliance(s) every 3 years (combined limit for hearing aids & other services)","blood_glucose_monitors":"$600 per person 1 appliance(s) every 1 years (combined limit for blood glucose monitors & other services)","pharmaceutical":"$750 per person","naturopathy":"$500 per person (combined limit for acupunture, naturopathy, remedial massage & other services Sub-limit apply)","podiatry":"$750 per person (Sub-limits apply)","psychology":"$750 per person (Sub-limits apply)"},"service_limit":{"dental_cleaning":{"Cleaning":"$23.45","Fillings":"$23.48"},"dental_major":{"Crown":"$1,200"},"dental_root":{"1 root canal":"$1,200"},"dental_braces":{"Braces":"100%"},"optical":{"Single vision":"$23.45","Multi-focal":"$23.48"},"physio":{"Initial visit":"$23.45","Subsequent visit":"$23.48"},"chiro":{"Initial visit":"$23.45","Subsequent visit":"$23.48"},"massages":{"Initial visit":"$23.45","Subsequent visit":"$23.48"},"acupunture":{"Initial visit":"$23.45","Subsequent visit":"$23.48"},"hearing_aids":{"Per hearing aid":"90%"},"blood_glucose_monitors":{"Per monitor":"90%"},"pharmaceutical":{"Per prescription":"90%"},"naturopathy":{"Initial visit":"$23.45","Subsequent visit":"$23.48"},"podiatry":{"Initial visit":"$23.45","Subsequent visit":"$23.48"},"psychology":{"Initial visit":"$23.45","Subsequent visit":"$23.48"}}}},{"fund_provider":"BUPA","fund_type":"Corporate product","monthly_premium":"$420","policy_name":"Top Hospital","per_visit":"$250","co_payment":"n/a","hospital_cover":{"pregnancy":"Covered","fertility_treatment":"Covered","heart_surgery":"Covered","eye_surgery":"Covered","joints":"Covered","renal_dialysis":"Covered","weight_loss":"Covered","sterilisation":"Covered","plastic_surgery":"Not Covered","rehab_treatment":"Not Covered","psychatric":"Excluded","palliative_care":"Not Covered"},"extras_cover":{"covered":{"dental_cleaning":"Covered","dental_major":"Covered","dental_root":"Covered","dental_braces":"Covered","optical":"Not Covered","physio":"Not Covered","chiro":"Covered","massages":"Covered","acupunture":"Covered","hearing_aids":"Covered","blood_glucose_monitors":"Not Covered","pharmaceutical":"Covered","naturopathy":"Covered","podiatry":"Covered","psychology":"Covered"},"annual_limit":{"dental_cleaning":"No annual limit (Sub-limits apply)","dental_major":"$1,200 per person (combined limit for major dental & endodontic - Sub-limit apply)","dental_root":"$1,200 per person (combined limit for major dental & endodontic - Sub-limit apply)","dental_braces":"$900 per person $2,800 lifetime limit","optical":"$280 per person (Sub-limits apply)","physio":"$750 per person (Sub-limits apply)","chiro":"$700 per person up to $1,400 per policy (Sub-limits apply)","massages":"$500 per person (combined limit for acupunture, naturopathy, remedial massage & other services Sub-limit apply","acupunture":"$500 per person (combined limit for acupunture, naturopathy, remedial massage & other services Sub-limit apply","hearing_aids":"$850 per person 1 appliance(s) every 3 years (combined limit for hearing aids & other services)","blood_glucose_monitors":"$600 per person 1 appliance(s) every 1 years (combined limit for blood glucose monitors & other services)","pharmaceutical":"$750 per person","naturopathy":"$500 per person (combined limit for acupunture, naturopathy, remedial massage & other services Sub-limit apply)","podiatry":"$750 per person (Sub-limits apply)","psychology":"$750 per person (Sub-limits apply)"},"service_limit":{"dental_cleaning":{"Cleaning":"$23.45","Fillings":"$23.48"},"dental_major":{"Crown":"$1,200"},"dental_root":{"1 root canal":"$1,200"},"dental_braces":{"Braces":"100%"},"optical":{"Single vision":"$23.45","Multi-focal":"$23.48"},"physio":{"Initial visit":"$23.45","Subsequent visit":"$23.48"},"chiro":{"Initial visit":"$23.45","Subsequent visit":"$23.48"},"massages":{"Initial visit":"$23.45","Subsequent visit":"$23.48"},"acupunture":{"Initial visit":"$23.45","Subsequent visit":"$23.48"},"hearing_aids":{"Per hearing aid":"90%"},"blood_glucose_monitors":{"Per monitor":"90%"},"pharmaceutical":{"Per prescription":"90%"},"naturopathy":{"Initial visit":"$23.45","Subsequent visit":"$23.48"},"podiatry":{"Initial visit":"$23.45","Subsequent visit":"$23.48"},"psychology":{"Initial visit":"$23.45","Subsequent visit":"$23.48"}}}},{"fund_provider":"CBHS","fund_type":"Restricted fund / Corporate product","monthly_premium":"$421","policy_name":"Ultimate Hospital","per_visit":"n/a","co_payment":"n/a","hospital_cover":{"pregnancy":"Covered","fertility_treatment":"Covered","heart_surgery":"Covered","eye_surgery":"Covered","joints":"Covered","renal_dialysis":"Covered","weight_loss":"Covered","sterilisation":"Covered","plastic_surgery":"Not Covered","rehab_treatment":"Not Covered","psychatric":"Excluded","palliative_care":"Not Covered"},"extras_cover":{"covered":{"dental_cleaning":"Covered","dental_major":"Covered","dental_root":"Covered","dental_braces":"Covered","optical":"Not Covered","physio":"Not Covered","chiro":"Covered","massages":"Covered","acupunture":"Covered","hearing_aids":"Covered","blood_glucose_monitors":"Not Covered","pharmaceutical":"Covered","naturopathy":"Covered","podiatry":"Covered","psychology":"Covered"},"annual_limit":{"dental_cleaning":"No annual limit (Sub-limits apply)","dental_major":"$1,200 per person (combined limit for major dental & endodontic - Sub-limit apply)","dental_root":"$1,200 per person (combined limit for major dental & endodontic - Sub-limit apply)","dental_braces":"$900 per person $2,800 lifetime limit","optical":"$280 per person (Sub-limits apply)","physio":"$750 per person (Sub-limits apply)","chiro":"$700 per person up to $1,400 per policy (Sub-limits apply)","massages":"$500 per person (combined limit for acupunture, naturopathy, remedial massage & other services Sub-limit apply","acupunture":"$500 per person (combined limit for acupunture, naturopathy, remedial massage & other services Sub-limit apply","hearing_aids":"$850 per person 1 appliance(s) every 3 years (combined limit for hearing aids & other services)","blood_glucose_monitors":"$600 per person 1 appliance(s) every 1 years (combined limit for blood glucose monitors & other services)","pharmaceutical":"$750 per person","naturopathy":"$500 per person (combined limit for acupunture, naturopathy, remedial massage & other services Sub-limit apply)","podiatry":"$750 per person (Sub-limits apply)","psychology":"$750 per person (Sub-limits apply)"},"service_limit":{"dental_cleaning":{"Cleaning":"$23.45","Fillings":"$23.48"},"dental_major":{"Crown":"$1,200"},"dental_root":{"1 root canal":"$1,200"},"dental_braces":{"Braces":"100%"},"optical":{"Single vision":"$23.45","Multi-focal":"$23.48"},"physio":{"Initial visit":"$23.45","Subsequent visit":"$23.48"},"chiro":{"Initial visit":"$23.45","Subsequent visit":"$23.48"},"massages":{"Initial visit":"$23.45","Subsequent visit":"$23.48"},"acupunture":{"Initial visit":"$23.45","Subsequent visit":"$23.48"},"hearing_aids":{"Per hearing aid":"90%"},"blood_glucose_monitors":{"Per monitor":"90%"},"pharmaceutical":{"Per prescription":"90%"},"naturopathy":{"Initial visit":"$23.45","Subsequent visit":"$23.48"},"podiatry":{"Initial visit":"$23.45","Subsequent visit":"$23.48"},"psychology":{"Initial visit":"$23.45","Subsequent visit":"$23.48"}}}}]}';

		$resultArr = json_decode($resultArr,true);

		$policies_count = $resultArr['policies_count'];
		$need_cover = $resultArr['need_cover']; 
		$state = $resultArr['state']; 
		$type_cover = $resultArr['type_cover']; 
		$ArrCoverType = array("Combined"=>"Hospital + Extras","Hospital"=>"Hospital Only","General"=>"Extras Only");
		$type_cover = $ArrCoverType[$type_cover];
		$min = $resultArr['min']; 
		$max = $resultArr['max'];

		$funds = $resultArr['funds']; 
		// print_r($resultArr['funds'][0]);echo "<br>";
		// print_r($resultArr['funds'][1]);echo "<br>";
		// print_r($resultArr['funds'][2]);
		$fundDetailsArr = array_merge_recursive($resultArr['funds'][0], $resultArr['funds'][1],$resultArr['funds'][2]);
		// $hospitalCoverArr = array_merge_recursive($resultArr['funds'][0]['hospital_cover'], $resultArr['funds'][1]['hospital_cover'],$resultArr['funds'][2]['hospital_cover']);
		// $extrasCoveredArr = array_merge_recursive($resultArr['funds'][0]['extras_cover']['covered'], $resultArr['funds'][1]['extras_cover']['covered'],$resultArr['funds'][2]['extras_cover']['covered']);
		// $extrasAnnualArr = array_merge_recursive($resultArr['funds'][0]['extras_cover']['annual_limit'], $resultArr['funds'][1]['extras_cover']['annual_limit'],$resultArr['funds'][2]['extras_cover']['annual_limit']);
		// $extrasServiceArr = array_merge_recursive($resultArr['funds'][0]['extras_cover']['service_limit'], $resultArr['funds'][1]['extras_cover']['service_limit'],$resultArr['funds'][2]['extras_cover']['service_limit']);

		// print_r($fundDetailsArr);

		function pagination($total,$per_page,$page,$url){   
		    $total = $total;
		    $adjacents = "2"; 
		     
		    $prevlabel = "Prev";
		    $nextlabel = "Next";
		     
		    $page = ($page == 0 ? 1 : $page);  
		    $start = ($page - 1) * $per_page;                               
		     
		    $prev = $page - 1;                          
		    $next = $page + 1;
		     
		    $lastpage = ceil($total/$per_page);
		     
		    $lpm1 = $lastpage - 1;
		     
		    $pagination = "";
		    if($lastpage > 1){   
		        $pagination .= "<ul class='pagination'>";
		        // $pagination .= "<li class='page_info'>Page {$page} of {$lastpage}</li>";
		             
		        if ($page > 1) $pagination.= "<li><a href='{$url}page={$prev}'>{$prevlabel}</a></li>";
		             
		        if ($lastpage < 7 + ($adjacents * 2)){  
		            for ($counter = 1; $counter <= $lastpage; $counter++){
		                if ($counter == $page)
		                    $pagination.= "<li><a class='current'>{$counter}</a></li>";
		                else
		                    $pagination.= "<li><a href='{$url}page={$counter}'>{$counter}</a></li>";                    
		            }
		        } elseif($lastpage > 5 + ($adjacents * 2)){
		            if($page < 1 + ($adjacents * 2)) {
		                 
		                for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++){
		                    if ($counter == $page)
		                        $pagination.= "<li><a class='current'>{$counter}</a></li>";
		                    else
		                        $pagination.= "<li><a href='{$url}page={$counter}'>{$counter}</a></li>";                    
		                }
		                $pagination.= "<li class='dot'>...</li>";
		                $pagination.= "<li><a href='{$url}page={$lpm1}'>{$lpm1}</a></li>";
		                $pagination.= "<li><a href='{$url}page={$lastpage}'>{$lastpage}</a></li>";         
		            } elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
		                 
		                $pagination.= "<li><a href='{$url}page=1'>1</a></li>";
		                $pagination.= "<li><a href='{$url}page=2'>2</a></li>";
		                $pagination.= "<li class='dot'>...</li>";
		                for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
		                    if ($counter == $page)
		                        $pagination.= "<li><a class='current'>{$counter}</a></li>";
		                    else
		                        $pagination.= "<li><a href='{$url}page={$counter}'>{$counter}</a></li>";                    
		                }
		                $pagination.= "<li class='dot'>..</li>";
		                $pagination.= "<li><a href='{$url}page={$lpm1}'>{$lpm1}</a></li>";
		                $pagination.= "<li><a href='{$url}page={$lastpage}'>{$lastpage}</a></li>";       
		            } else {
		                $pagination.= "<li><a href='{$url}page=1'>1</a></li>";
		                $pagination.= "<li><a href='{$url}page=2'>2</a></li>";
		                $pagination.= "<li class='dot'>..</li>";
		                for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
		                    if ($counter == $page)
		                        $pagination.= "<li><a class='current'>{$counter}</a></li>";
		                    else
		                        $pagination.= "<li><a href='{$url}page={$counter}'>{$counter}</a></li>";                    
		                }
		            }
		        }

		        if ($page < $counter - 1) {
					$pagination.= "<li><a href='{$url}page={$next}'>{$nextlabel}</a></li>";
				}
		        $pagination.= "</ul>";        
		    }
		    return $pagination;
		}

		$page = (int)(!isset($_GET["page"]) ? 1 : $_GET["page"]);
		if ($page <= 0) $page = 1;
		$per_page = 3;

// }
require_once("includes/header.php");
?>
<div id="main_container_result">

	<!-- Filters  -->
	<div class="container-fluid result_container">
		<div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8 col-sm-12 col-xs-12 result_main_contain">
			<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 pagination_container">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paging">
				<?php echo pagination($policies_count,3,$page,$url='?'); ?>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 key_filters">
					<h4 class="filter_options_header"><?php echo $policies_count; ?> policies found for <?php echo $need_cover; ?> | <?php echo $state; ?> | <?php echo $type_cover; ?> | between <?php echo $min; ?> and <?php echo $max; ?> a month </h4>
				</div>
			</div>
			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 result_filter_container">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 result_filter"><button type="button" class="result_filter_btn filter_btn_1 more_btn active">Sort Price Low to High</button></div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 result_filter"><button type="button" class="result_filter_btn filter_btn_1 more_btn">Sort Price High to Low</button></div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 result_filter"><input type="text" class="result_search_btn filter_btn_1 more_btn" placeholder="Search by fund (e.g Medibank, Bupa, Nib)" /></div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 results_main" id="dynamic_results">
				<div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bank_headers"></div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 premium_headers head bor_bottom">Monthly premium<br>(after 25.657% rebate)</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 policy_headers head bor_bottom">Policy name</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 visit_headers head bor_bottom">Excess per visit</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payment_headers head bor_bottom">Co-payment</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payment_headers"></div>
				</div>
				<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
					<?php $lastFundProvider = end($fundDetailsArr['fund_provider']);
					foreach ($fundDetailsArr['fund_provider'] as $key => $value) {
						if ($lastFundProvider==$value) {
							echo '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 bank_headers"><h4 class="bank_name">'.$value.'</h4><p class="fund_type">Open fund</p></div>';
						} else {
							echo '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 bank_headers bor_right"><h4 class="bank_name">'.$value.'</h4><p class="fund_type">Open fund</p></div>';
						}
					}
					?>
						<!-- <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 bank_headers bor_right"><h4 class="bank_name">MEDIBANK</h4><p class="fund_type">Open fund</p></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 bank_headers bor_right"><h4 class="bank_name">BUPA</h4><p class="fund_type">Corporate product</p></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 bank_headers"><h4 class="bank_name">CBHS</h4><p class="fund_type">Restricted fund / Corporate product</p></div> -->
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 premium_headers bor_bottom_right"><h4 class="policy_price">$413</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 premium_headers bor_bottom_right"><h4 class="policy_price">$420</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 premium_headers bor_bottom"><h4 class="policy_price">$421</h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 policy_headers bor_bottom_right"><h4 class="policy_name">Top Hospital Essential</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 policy_headers bor_bottom_right"><h4 class="policy_name">Top Hospital</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 policy_headers bor_bottom"><h4 class="policy_name">Ultimate Hospital</h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 visit_headers bor_bottom_right"><h4 class="visit_price">$500</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 visit_headers bor_bottom_right"><h4 class="visit_price">$250</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 visit_headers bor_bottom"><h4 class="visit_price">$nil</h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 payment_headers bor_bottom_right"><h4 class="payment">n/a</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 payment_headers bor_bottom_right"><h4 class="payment">n/a</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 payment_headers bor_bottom"><h4 class="payment">n/a</h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 payment_headers bor_right"></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 payment_headers bor_right"></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 payment_headers"></div>
					</div>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_header">HOSPITAL COVER</div>
				<div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">Pregnancy</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">Fertility treatment (IVF)</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">Heart surgery (cardiac)</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">Eye surgery (cataract)</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">Joints (hip, knee, shoulder</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">Renal dialysis (kidney)</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">Weight loss (gastric)</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">Sterilisation</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">Plastic surgery</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">Rehab treatment</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">Psychatric</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">Palliative care</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head"></div>
					<!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">Monthly premium<br>(after 25.657% rebate)</div> -->
				</div>
				<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom_right"><h4 class="payment"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom_right"><h4 class="payment"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom"><h4 class="payment"><img src="images/tick.png"></h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom_right"><h4 class="payment"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom_right"><h4 class="payment"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom"><h4 class="payment"><img src="images/tick.png"></h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom_right"><h4 class="payment"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom_right"><h4 class="payment"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom"><h4 class="payment"><img src="images/tick.png"></h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom_right"><h4 class="payment"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom_right"><h4 class="payment"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom"><h4 class="payment"><img src="images/tick.png"></h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom_right"><h4 class="payment"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom_right"><h4 class="payment"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom"><h4 class="payment"><img src="images/tick.png"></h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom_right"><h4 class="payment"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom_right"><h4 class="payment"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom"><h4 class="payment"><img src="images/tick.png"></h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom_right"><h4 class="payment"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom_right"><h4 class="payment"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom"><h4 class="payment"><img src="images/tick.png"></h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom_right"><h4 class="payment"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom_right"><h4 class="payment"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom"><h4 class="payment"><img src="images/tick.png"></h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom_right"><h4 class="payment"><img src="images/cross.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom_right"><h4 class="payment"><img src="images/cross.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom"><h4 class="payment"><img src="images/cross.png"></h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom_right"><h4 class="payment"><img src="images/cross.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom_right"><h4 class="payment"><img src="images/cross.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom"><h4 class="payment"><img src="images/cross.png"></h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom_right"><h4 class="payment">Restrictions apply</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom_right"><h4 class="payment">Excluded</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom"><h4 class="payment">Partically covered</h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom_right"><h4 class="payment"><img src="images/cross.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom_right"><h4 class="payment"><img src="images/cross.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom"><h4 class="payment"><img src="images/cross.png"></h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_right"></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_right"></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers"></div>
					</div>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_header extra_header_main">
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-2 extra_header">EXTRAS COVER</div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-2 btn_header"><button type="button" class="result_header_btn filter_btn_1 more_btn active">Covered</button></div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-2 btn_header"><button type="button" class="result_header_btn filter_btn_1 more_btn">Annual limit</button></div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-2 btn_header"><button type="button" class="result_header_btn filter_btn_1 more_btn">Per service limit</button></div>
				</div>
				<div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section covered_section">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cover_extra_headers head bor_bottom">Dental (cleaning, fillings)</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cover_extra_headers head bor_bottom">Dental major (crowns)</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cover_extra_headers head bor_bottom">Dental (root canal)</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cover_extra_headers head bor_bottom">Dental (braces)</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cover_extra_headers head bor_bottom">Optical</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cover_extra_headers head bor_bottom">Physio</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cover_extra_headers head bor_bottom">Chiro</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cover_extra_headers head bor_bottom">Massages</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cover_extra_headers head bor_bottom">Acupunture</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cover_extra_headers head bor_bottom">Hearing aids</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cover_extra_headers head bor_bottom">Blood glucose monitors</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cover_extra_headers head bor_bottom">Pharmaceutical (non PBS)</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cover_extra_headers head bor_bottom">Naturopathy</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cover_extra_headers head bor_bottom">Podiatry (feet)</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cover_extra_headers head bor_bottom">Psychology</div>
				</div>
				<div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section annual_limit_section">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 annual_extra_headers head bor_bottom">Dental (cleaning, fillings)</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 annual_extra_headers head bor_bottom">Dental major (crowns)</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 annual_extra_headers head bor_bottom">Dental (root canal)</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 annual_extra_headers head bor_bottom">Dental (braces)</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 annual_extra_headers head bor_bottom">Optical</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 annual_extra_headers head bor_bottom">Physio</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 annual_extra_headers head bor_bottom">Chiro</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 annual_extra_headers head bor_bottom">Massages</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 annual_extra_headers head bor_bottom">Acupunture</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 annual_extra_headers head bor_bottom">Hearing aids</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 annual_extra_headers head bor_bottom">Blood glucose monitors</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 annual_extra_headers head bor_bottom">Pharmaceutical (non PBS)</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 annual_extra_headers head bor_bottom">Naturopathy</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 annual_extra_headers head bor_bottom">Podiatry (feet)</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 annual_extra_headers head bor_bottom">Psychology</div>
				</div>
				<div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section service_limit_section">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 service_extra_headers head bor_bottom">Dental (cleaning, fillings)</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 service_extra_headers head bor_bottom">Dental major (crowns)</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 service_extra_headers head bor_bottom">Dental (root canal)</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 service_extra_headers head bor_bottom">Dental (braces)</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 service_extra_headers head bor_bottom">Optical</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 service_extra_headers head bor_bottom">Physio</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 service_extra_headers head bor_bottom">Chiro</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 service_extra_headers head bor_bottom">Massages</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 service_extra_headers head bor_bottom">Acupunture</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 service_extra_headers head bor_bottom">Hearing aids</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 service_extra_headers head bor_bottom">Blood glucose monitors</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 service_extra_headers head bor_bottom">Pharmaceutical (non PBS)</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 service_extra_headers head bor_bottom">Naturopathy</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 service_extra_headers head bor_bottom">Podiatry (feet)</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 service_extra_headers head bor_bottom">Psychology</div>
				</div>
				<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section covered_section">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom_right"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom_right"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom_right"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom_right"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom_right"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom_right"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom_right"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom_right"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom_right"><h4 class="payment_img"><img src="images/cross.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom_right"><h4 class="payment_img"><img src="images/cross.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom"><h4 class="payment_img"><img src="images/cross.png"></h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom_right"><h4 class="payment_img"><img src="images/cross.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom_right"><h4 class="payment_img"><img src="images/cross.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom"><h4 class="payment_img"><img src="images/cross.png"></h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom_right"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom_right"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom_right"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom_right"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom_right"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom_right"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom_right"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom_right"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom_right"><h4 class="payment_img"><img src="images/cross.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom_right"><h4 class="payment_img"><img src="images/cross.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom"><h4 class="payment_img"><img src="images/cross.png"></h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom_right"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom_right"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom_right"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom_right"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom_right"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom_right"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom_right"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom_right"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom"><h4 class="payment_img"><img src="images/tick.png"></h4></div>
					</div>
				</div>
				<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section annual_limit_section">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom_right"><h4 class="annual_content">No annual limit (Sub-limits apply)</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom_right"><h4 class="annual_content">No annual limit (Sub-limits apply)</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom"><h4 class="annual_content">No annual limit (Sub-limits apply)</h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom_right"><h4 class="annual_content">$1,200 per person (combined limit for major dental & endodontic - Sub-limit apply)</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom_right"><h4 class="annual_content">$1,200 per person (combined limit for major dental & endodontic - Sub-limit apply)</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom"><h4 class="annual_content">$1,200 per person (combined limit for major dental & endodontic - Sub-limit apply)</h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom_right"><h4 class="annual_content">$1,200 per person (combined limit for major dental & endodontic - Sub-limit apply)</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom_right"><h4 class="annual_content">$1,200 per person (combined limit for major dental & endodontic - Sub-limit apply)</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom"><h4 class="annual_content">$1,200 per person (combined limit for major dental & endodontic - Sub-limit apply)</h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom_right"><h4 class="annual_content">$900 per person $2,800 lifetime limit</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom_right"><h4 class="annual_content">$900 per person $2,800 lifetime limit</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom"><h4 class="annual_content">$900 per person $2,800 lifetime limit</h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom_right"><h4 class="annual_content">$280 per person (Sub-limits apply)</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom_right"><h4 class="annual_content">$280 per person (Sub-limits apply)</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom"><h4 class="annual_content">$280 per person (Sub-limits apply)</h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom_right"><h4 class="annual_content">$750 per person (Sub-limits apply)</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom_right"><h4 class="annual_content">$750 per person (Sub-limits apply)</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom"><h4 class="annual_content">$750 per person (Sub-limits apply)</h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom_right"><h4 class="annual_content">$700 per person up to $1,400 per policy (Sub-limits apply)</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom_right"><h4 class="annual_content">$700 per person up to $1,400 per policy (Sub-limits apply)</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom"><h4 class="annual_content">$700 per person up to $1,400 per policy (Sub-limits apply)</h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom_right"><h4 class="annual_content">$500 per person (combined limit for acupunture, naturopathy, remedial massage & other services Sub-limit apply</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom_right"><h4 class="annual_content">$500 per person (combined limit for acupunture, naturopathy, remedial massage & other services Sub-limit apply</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom"><h4 class="annual_content">$500 per person (combined limit for acupunture, naturopathy, remedial massage & other services Sub-limit apply</h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom_right"><h4 class="annual_content">$500 per person (combined limit for acupunture, naturopathy, remedial massage & other services Sub-limit apply</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom_right"><h4 class="annual_content">$500 per person (combined limit for acupunture, naturopathy, remedial massage & other services Sub-limit apply</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom"><h4 class="annual_content">$500 per person (combined limit for acupunture, naturopathy, remedial massage & other services Sub-limit apply</h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom_right"><h4 class="annual_content">$850 per person 1 appliance(s) every 3 years (combined limit for hearing aids & other services)</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom_right"><h4 class="annual_content">$850 per person 1 appliance(s) every 3 years (combined limit for hearing aids & other services)</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom"><h4 class="annual_content">$850 per person 1 appliance(s) every 3 years (combined limit for hearing aids & other services)</h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom_right"><h4 class="annual_content">$600 per person 1 appliance(s) every 1 years (combined limit for blood glucose monitors & other services)</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom_right"><h4 class="annual_content">$600 per person 1 appliance(s) every 1 years (combined limit for blood glucose monitors & other services)</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom"><h4 class="annual_content">$600 per person 1 appliance(s) every 1 years (combined limit for blood glucose monitors & other services)</h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom_right"><h4 class="annual_content">$750 per person</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom_right"><h4 class="annual_content">$750 per person</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom"><h4 class="annual_content">$750 per person</h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom_right"><h4 class="annual_content">$500 per person (combined limit for acupunture, naturopathy, remedial massage & other services Sub-limit apply)</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom_right"><h4 class="annual_content">$500 per person (combined limit for acupunture, naturopathy, remedial massage & other services Sub-limit apply)</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom"><h4 class="annual_content">$500 per person (combined limit for acupunture, naturopathy, remedial massage & other services Sub-limit apply)</h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom_right"><h4 class="annual_content">$750 per person (Sub-limits apply)</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom_right"><h4 class="annual_content">$750 per person (Sub-limits apply)</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom"><h4 class="annual_content">$750 per person (Sub-limits apply)</h4></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom_right"><h4 class="annual_content">$750 per person (Sub-limits apply)</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom_right"><h4 class="annual_content">$750 per person (Sub-limits apply)</h4></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom"><h4 class="annual_content">$750 per person (Sub-limits apply)</h4></div>
					</div>
				</div>
				<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section service_limit_section">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom_right"><table class="service_content"><tr><td>Cleaning</td><td>$23.45</td></tr><tr><td>Fillings</td><td>$23.48</td></tr></table></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom_right"><table class="service_content"><tr><td>Cleaning</td><td>$23.45</td></tr><tr><td>Fillings</td><td>$23.48</td></tr></table></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom"><table class="service_content"><tr><td>Cleaning</td><td>$23.45</td></tr><tr><td>Fillings</td><td>$23.48</td></tr></table></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom_right"><table class="service_content"><tr><td>Crown</td><td>$1,200</td></tr></table></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom_right"><table class="service_content"><tr><td>Crown</td><td>$1,200</td></tr></table></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom"><table class="service_content"><tr><td>Crown</td><td>$1,200</td></tr></table></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom_right"><table class="service_content"><tr><td>1 root canal</td><td>$1,200</td></tr></table></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom_right"><table class="service_content"><tr><td>1 root canal</td><td>$1,200</td></tr></table></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom"><table class="service_content"><tr><td>1 root canal</td><td>$1,200</td></tr></table></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom_right"><table class="service_content"><tr><td>Braces</td><td>100%</td></tr></table></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom_right"><table class="service_content"><tr><td>Braces</td><td>100%</td></tr></table></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom"><table class="service_content"><tr><td>Braces</td><td>100%</td></tr></table></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom_right"><table class="service_content"><tr><td>Single vision</td><td>$23.45</td></tr><tr><td>Multi-focal</td><td>$23.48</td></tr></table></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom_right"><table class="service_content"><tr><td>Single vision</td><td>$23.45</td></tr><tr><td>Multi-focal</td><td>$23.48</td></tr></table></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom"><table class="service_content"><tr><td>Single vision</td><td>$23.45</td></tr><tr><td>Multi-focal</td><td>$23.48</td></tr></table></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom_right"><table class="service_content"><tr><td>Initial visit</td><td>$23.45</td></tr><tr><td>Subsequent visit</td><td>$23.48</td></tr></table></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom_right"><table class="service_content"><tr><td>Initial visit</td><td>$23.45</td></tr><tr><td>Subsequent visit</td><td>$23.48</td></tr></table></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom"><table class="service_content"><tr><td>Initial visit</td><td>$23.45</td></tr><tr><td>Subsequent visit</td><td>$23.48</td></tr></table></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom_right"><table class="service_content"><tr><td>Initial visit</td><td>$23.45</td></tr><tr><td>Subsequent visit</td><td>$23.48</td></tr></table></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom_right"><table class="service_content"><tr><td>Initial visit</td><td>$23.45</td></tr><tr><td>Subsequent visit</td><td>$23.48</td></tr></table></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom"><table class="service_content"><tr><td>Initial visit</td><td>$23.45</td></tr><tr><td>Subsequent visit</td><td>$23.48</td></tr></table></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom_right"><table class="service_content"><tr><td>Initial visit</td><td>$23.45</td></tr><tr><td>Subsequent visit</td><td>$23.48</td></tr></table></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom_right"><table class="service_content"><tr><td>Initial visit</td><td>$23.45</td></tr><tr><td>Subsequent visit</td><td>$23.48</td></tr></table></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom"><table class="service_content"><tr><td>Initial visit</td><td>$23.45</td></tr><tr><td>Subsequent visit</td><td>$23.48</td></tr></table></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom_right"><table class="service_content"><tr><td>Initial visit</td><td>$23.45</td></tr><tr><td>Subsequent visit</td><td>$23.48</td></tr></table></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom_right"><table class="service_content"><tr><td>Initial visit</td><td>$23.45</td></tr><tr><td>Subsequent visit</td><td>$23.48</td></tr></table></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom"><table class="service_content"><tr><td>Initial visit</td><td>$23.45</td></tr><tr><td>Subsequent visit</td><td>$23.48</td></tr></table></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom_right"><table class="service_content"><tr><td>Per hearing aid</td><td>90%</td></tr></table></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom_right"><table class="service_content"><tr><td>Per hearing aid</td><td>90%</td></tr></table></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom"><table class="service_content"><tr><td>Per hearing aid</td><td>90%</td></tr></table></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom_right"><table class="service_content"><tr><td>Per monitor</td><td>90%</td></tr></table></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom_right"><table class="service_content"><tr><td>Per monitor</td><td>90%</td></tr></table></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom"><table class="service_content"><tr><td>Per monitor</td><td>90%</td></tr></table></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom_right"><table class="service_content"><tr><td>Per prescription</td><td>90%</td></tr></table></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom_right"><table class="service_content"><tr><td>Per prescription</td><td>90%</td></tr></table></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom"><table class="service_content"><tr><td>Per prescription</td><td>90%</td></tr></table></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom_right"><table class="service_content"><tr><td>Initial visit</td><td>$23.45</td></tr><tr><td>Subsequent visit</td><td>$23.48</td></tr></table></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom_right"><table class="service_content"><tr><td>Initial visit</td><td>$23.45</td></tr><tr><td>Subsequent visit</td><td>$23.48</td></tr></table></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom"><table class="service_content"><tr><td>Initial visit</td><td>$23.45</td></tr><tr><td>Subsequent visit</td><td>$23.48</td></tr></table></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom_right"><table class="service_content"><tr><td>Initial visit</td><td>$23.45</td></tr><tr><td>Subsequent visit</td><td>$23.48</td></tr></table></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom_right"><table class="service_content"><tr><td>Initial visit</td><td>$23.45</td></tr><tr><td>Subsequent visit</td><td>$23.48</td></tr></table></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom"><table class="service_content"><tr><td>Initial visit</td><td>$23.45</td></tr><tr><td>Subsequent visit</td><td>$23.48</td></tr></table></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom_right"><table class="service_content"><tr><td>Initial visit</td><td>$23.45</td></tr><tr><td>Subsequent visit</td><td>$23.48</td></tr></table></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom_right"><table class="service_content"><tr><td>Initial visit</td><td>$23.45</td></tr><tr><td>Subsequent visit</td><td>$23.48</td></tr></table></div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom"><table class="service_content"><tr><td>Initial visit</td><td>$23.45</td></tr><tr><td>Subsequent visit</td><td>$23.48</td></tr></table></div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('.annual_limit_section').hide();
		$('.service_limit_section').hide();

		$( ".btn_header" ).on( "click", function() {
	    	var btn_name = $(this).find('.result_header_btn').text();
	    	$('.result_header_btn').removeClass('active');
	    	$(this).find('.result_header_btn').addClass('active');
	    	if (btn_name=='Covered') {
	    		$('.covered_section').show();
	    		$('.annual_limit_section').hide();
				$('.service_limit_section').hide();
	    	} else if(btn_name=='Annual limit') {
	    		$('.covered_section').hide();
	    		$('.annual_limit_section').show();
				$('.service_limit_section').hide();
	    	} else {
	    		$('.covered_section').hide();
	    		$('.annual_limit_section').hide();
				$('.service_limit_section').show();
	    	} 
		});
	});
</script>
<?php
require_once("includes/footer.php");
?>		