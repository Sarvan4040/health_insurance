<?php
ini_set('memory_limit', '-1');
require_once("includes/configure.php");
// require_once("includes/common_function.php");
require_once("includes/session_check.php");

require_once("includes/PHPExcel.php");

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');
	$objPHPExcel = new PHPExcel();
	
	// Set document properties
	$objPHPExcel->getProperties()->setCreator("HealthCare Insurance Policy Reports")
									 ->setLastModifiedBy("Policy Reports")
									 ->setTitle("Office 2007 XLSX Event Reports Document")
									 ->setSubject("Office 2007 XLSX Event Reports Document")
									 ->setDescription("Event Reports")
									 ->setKeywords("Office 2007 XLSX Event Reports Document")
									 ->setCategory("Event Reports");
	$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A1', 'Insurance Id')
					->setCellValue('B1', 'WS_BatchId')
					->setCellValue('C1', 'WS_PolicyCode')
					->setCellValue('D1', 'WS_PolicyUrl')
					->setCellValue('E1', 'WS_PolicyName')
					->setCellValue('F1', 'WS_ProductName')
					->setCellValue('G1', 'WS_Status')
					->setCellValue('H1', 'WS_Corporate')
					->setCellValue('I1', 'WS_OverseasVisitors')
					->setCellValue('J1', 'WS_Enabled')
					->setCellValue('K1', 'WS_Visible')
					->setCellValue('L1', 'WS_FundId')
					->setCellValue('M1', 'WS_Fund')
					->setCellValue('N1', 'WS_FundName')
					->setCellValue('O1', 'WS_StateId')
					->setCellValue('P1', 'WS_State')
					->setCellValue('Q1', 'WS_StateName')
					->setCellValue('R1', 'WS_CoverTypeId')
					->setCellValue('S1', 'WS_CoverType')
					->setCellValue('T1', 'WS_CoverLevelHospitalId')
					->setCellValue('U1', 'WS_CoverLevelHospital')
					->setCellValue('V1', 'WS_CoverLevelGeneralId')
					->setCellValue('W1', 'WS_CoverLevelGeneral')
					->setCellValue('X1', 'WS_CategoryId')
					->setCellValue('Y1', 'WS_Category')
					->setCellValue('Z1', 'WS_CategoryOriginalName')
					->setCellValue('AA1', 'WS_Hos_Excess_PerHospitalVisit')
					->setCellValue('AB1', 'WS_Hos_Excess_MaximumPerPerson')
					->setCellValue('AC1', 'WS_Hos_Excess_MaximumPerAnnum')
					->setCellValue('AD1', 'WS_Hos_CoPayments')
					->setCellValue('AE1', 'WS_Hos_CoPayment_SharedRoom')
					->setCellValue('AF1', 'WS_Hos_CoPayment_PrivateRoom')
					->setCellValue('AG1', 'WS_Hos_CoPayment_DaySurgery')
					->setCellValue('AH1', 'WS_Hos_OtherFeatures')
					->setCellValue('AI1', 'WS_Hos_Ambulance')
					->setCellValue('AJ1', 'WS_Hos_AmbulanceCover')
					->setCellValue('AK1', 'WS_Gen_AmbulanceCover')
					->setCellValue('AL1', 'WS_YearlyPremium')
					->setCellValue('AM1', 'WS_YearlyPremium_StdLessThanAge65Rebate')
					->setCellValue('AN1', 'WS_YearlyPremium_StdAge65ToAge69Rebate')
					->setCellValue('AO1', 'WS_YearlyPremium_StdAge70AndGreaterRebate')
					->setCellValue('AP1', 'WS_YearlyPremium_T1LessThanAge65Rebate')
					->setCellValue('AQ1', 'WS_YearlyPremium_T1Age65ToAge69Rebate')
					->setCellValue('AR1', 'WS_YearlyPremium_T1Age70AndGreaterRebate')
					->setCellValue('AS1', 'WS_YearlyPremium_T2LessThanAge65Rebate')
					->setCellValue('AT1', 'WS_YearlyPremium_T2Age65ToAge69Rebate')
					->setCellValue('AU1', 'WS_YearlyPremium_T2Age70AndGreaterRebate')
					->setCellValue('AV1', 'WS_HalfYearlyPremium')
					->setCellValue('AW1', 'WS_HalfYearlyPremium_StdLessThanAge65Rebate')
					->setCellValue('AX1', 'WS_HalfYearlyPremium_StdAge65ToAge69Rebate')
					->setCellValue('AY1', 'WS_HalfYearlyPremium_StdAge70AndGreaterRebate')
					->setCellValue('AZ1', 'WS_HalfYearlyPremium_T1LessThanAge65Rebate')
					->setCellValue('BA1', 'WS_HalfYearlyPremium_T1Age65ToAge69Rebate')
					->setCellValue('BB1', 'WS_HalfYearlyPremium_T1Age70AndGreaterRebate')
					->setCellValue('BC1', 'WS_HalfYearlyPremium_T2LessThanAge65Rebate')
					->setCellValue('BD1', 'WS_HalfYearlyPremium_T2Age65ToAge69Rebate')
					->setCellValue('BE1', 'WS_HalfYearlyPremium_T2Age70AndGreaterRebate')
					->setCellValue('BF1', 'WS_QuarterlyPremium')
					->setCellValue('BG1', 'WS_QuarterlyPremium_StdLessThanAge65Rebate')
					->setCellValue('BH1', 'WS_QuarterlyPremium_StdAge65ToAge69Rebate')
					->setCellValue('BI1', 'WS_QuarterlyPremium_StdAge70AndGreaterRebate')
					->setCellValue('BJ1', 'WS_QuarterlyPremium_T1LessThanAge65Rebate')
					->setCellValue('BK1', 'WS_QuarterlyPremium_T1Age65ToAge69Rebate')
					->setCellValue('BL1', 'WS_QuarterlyPremium_T1Age70AndGreaterRebate')
					->setCellValue('BM1', 'WS_QuarterlyPremium_T2LessThanAge65Rebate')
					->setCellValue('BN1', 'WS_QuarterlyPremium_T2Age65ToAge69Rebate')
					->setCellValue('BO1', 'WS_QuarterlyPremium_T2Age70AndGreaterRebate')
					->setCellValue('BP1', 'WS_MonthlyPremium')
					->setCellValue('BQ1', 'WS_MonthlyPremium_StdLessThanAge65Rebate')
					->setCellValue('BR1', 'WS_MonthlyPremium_StdAge65ToAge69Rebate')
					->setCellValue('BS1', 'WS_MonthlyPremium_StdAge70AndGreaterRebate')
					->setCellValue('BT1', 'WS_MonthlyPremium_T1LessThanAge65Rebate')
					->setCellValue('BU1', 'WS_MonthlyPremium_T1Age65ToAge69Rebate')
					->setCellValue('BV1', 'WS_MonthlyPremium_T1Age70AndGreaterRebate')
					->setCellValue('BW1', 'WS_MonthlyPremium_T2LessThanAge65Rebate')
					->setCellValue('BX1', 'WS_MonthlyPremium_T2Age65ToAge69Rebate')
					->setCellValue('BY1', 'WS_MonthlyPremium_T2Age70AndGreaterRebate')
					->setCellValue('BZ1', 'WS_FortnightlyPremium')
					->setCellValue('CA1', 'WS_FortnightlyPremium_StdLessThanAge65Rebate')
					->setCellValue('CB1', 'WS_FortnightlyPremium_StdAge65ToAge69Rebate')
					->setCellValue('CC1', 'WS_FortnightlyPremium_StdAge70AndGreaterRebate')
					->setCellValue('CD1', 'WS_FortnightlyPremium_T1LessThanAge65Rebate')
					->setCellValue('CE1', 'WS_FortnightlyPremium_T1Age65ToAge69Rebate')
					->setCellValue('CF1', 'WS_FortnightlyPremium_T1Age70AndGreaterRebate')
					->setCellValue('CG1', 'WS_FortnightlyPremium_T2LessThanAge65Rebate')
					->setCellValue('CH1', 'WS_FortnightlyPremium_T2Age65ToAge69Rebate')
					->setCellValue('CI1', 'WS_FortnightlyPremium_T2Age70AndGreaterRebate')
					->setCellValue('CJ1', 'WS_WeeklyPremium')
					->setCellValue('CK1', 'WS_WeeklyPremium_StdLessThanAge65Rebate')
					->setCellValue('CL1', 'WS_WeeklyPremium_StdAge65ToAge69Rebate')
					->setCellValue('CM1', 'WS_WeeklyPremium_StdAge70AndGreaterRebate')
					->setCellValue('CN1', 'WS_WeeklyPremium_T1LessThanAge65Rebate')
					->setCellValue('CO1', 'WS_WeeklyPremium_T1Age65ToAge69Rebate')
					->setCellValue('CP1', 'WS_WeeklyPremium_T1Age70AndGreaterRebate')
					->setCellValue('CQ1', 'WS_WeeklyPremium_T2LessThanAge65Rebate')
					->setCellValue('CR1', 'WS_WeeklyPremium_T2Age65ToAge69Rebate')
					->setCellValue('CS1', 'WS_WeeklyPremium_T2Age70AndGreaterRebate')
					->setCellValue('CT1', 'WS_Hos_Accommodation_Covered')
					->setCellValue('CU1', 'WS_Hos_Accommodation_Covered_Value')
					->setCellValue('CV1', 'WS_Hos_CardiacAndRelatedServices_Covered')
					->setCellValue('CW1', 'WS_Hos_CardiacAndRelatedServices_Covered_Value')
					->setCellValue('CX1', 'WS_Hos_CataractAndEyeLensProcedures_Covered')
					->setCellValue('CY1', 'WS_Hos_CataractAndEyeLensProcedures_Covered_Value')
					->setCellValue('CZ1', 'WS_Hos_PregnancyAndBirthRelatedServices_Covered')
					->setCellValue('DA1', 'WS_Hos_PregnancyAndBirthRelatedServices_Covered_Value')
					->setCellValue('DB1', 'WS_Hos_AssistedReproductiveServices_Covered')
					->setCellValue('DC1', 'WS_Hos_AssistedReproductiveServices_Covered_Value')
					->setCellValue('DD1', 'WS_Hos_JointReplacement_HipKneeShoulderAndElbow_Covered')
					->setCellValue('DE1', 'WS_Hos_JointReplacement_HipKneeShoulderAndElbow_Covered_Value')
					->setCellValue('DF1', 'WS_Hos_JointReplacement_HipKneeOnly_Covered')
					->setCellValue('DG1', 'WS_Hos_JointReplacement_HipKneeOnly_Covered_Value')
					->setCellValue('DH1', 'WS_Hos_JointReplacement_HipOnly_Covered')
					->setCellValue('DI1', 'WS_Hos_JointReplacement_HipOnly_Covered_Value')
					->setCellValue('DJ1', 'WS_Hos_DialysisForChronicRenalFailure_Covered')
					->setCellValue('DK1', 'WS_Hos_DialysisForChronicRenalFailure_Covered_Value')
					->setCellValue('DL1', 'WS_Hos_GastricBandingAndRelatedServices_Covered')
					->setCellValue('DM1', 'WS_Hos_GastricBandingAndRelatedServices_Covered_Value')
					->setCellValue('DN1', 'WS_Hos_Sterilisation_Covered')
					->setCellValue('DO1', 'WS_Hos_Sterilisation_Covered_Value')
					->setCellValue('DP1', 'WS_Hos_NonCosmeticPlasticSurgery_Covered')
					->setCellValue('DQ1', 'WS_Hos_NonCosmeticPlasticSurgery_Covered_Value')
					->setCellValue('DR1', 'WS_Hos_Rehabilitation_Covered')
					->setCellValue('DS1', 'WS_Hos_Rehabilitation_Covered_Value')
					->setCellValue('DT1', 'WS_Hos_PsychiatricServices_Covered')
					->setCellValue('DU1', 'WS_Hos_PsychiatricServices_Covered_Value')
					->setCellValue('DV1', 'WS_Hos_PalliativeCare_Covered')
					->setCellValue('DW1', 'WS_Hos_PalliativeCare_Covered_Value')
					->setCellValue('DX1', 'WS_Gen_Dental_GeneralDental_Benefits_Stars')
					->setCellValue('DY1', 'WS_Gen_Dental_GeneralDental_Limits_Stars')
					->setCellValue('DZ1', 'WS_Gen_Dental_MajorDental_Benefits_Stars')
					->setCellValue('EA1', 'WS_Gen_Dental_MajorDental_Limits_Stars')
					->setCellValue('EB1', 'WS_Gen_Dental_Endodontic_Benefits_Stars')
					->setCellValue('EC1', 'WS_Gen_Dental_Endodontic_Limits_Stars')
					->setCellValue('ED1', 'WS_Gen_Dental_Orthodontic_Benefits_Stars')
					->setCellValue('EE1', 'WS_Gen_Dental_Orthodontic_Limits_Stars')
					->setCellValue('EF1', 'WS_Gen_Optical_Benefits_Stars')
					->setCellValue('EG1', 'WS_Gen_Optical_Limits_Stars')
					->setCellValue('EH1', 'WS_Gen_NonPbsPharmaceuticals_Benefits_Stars')
					->setCellValue('EI1', 'WS_Gen_NonPbsPharmaceuticals_Limits_Stars')
					->setCellValue('EJ1', 'WS_Gen_Physiotherapy_Benefits_Stars')
					->setCellValue('EK1', 'WS_Gen_Physiotherapy_Limits_Stars')
					->setCellValue('EL1', 'WS_Gen_Chiropractic_Benefits_Stars')
					->setCellValue('EM1', 'WS_Gen_Chiropractic_Limits_Stars')
					->setCellValue('EN1', 'WS_Gen_Podiatry_Benefits_Stars')
					->setCellValue('EO1', 'WS_Gen_Podiatry_Limits_Stars')
					->setCellValue('EP1', 'WS_Gen_Psychology_Benefits_Stars')
					->setCellValue('EQ1', 'WS_Gen_Psychology_Limits_Stars')
					->setCellValue('ER1', 'WS_Gen_Acupuncture_Benefits_Stars')
					->setCellValue('ES1', 'WS_Gen_Acupuncture_Limits_Stars')
					->setCellValue('ET1', 'WS_Gen_Naturopathy_Benefits_Stars')
					->setCellValue('EU1', 'WS_Gen_Naturopathy_Limits_Stars')
					->setCellValue('EV1', 'WS_Gen_RemedialMassage_Benefits_Stars')
					->setCellValue('EW1', 'WS_Gen_RemedialMassage_Limits_Stars')
					->setCellValue('EX1', 'WS_Gen_HearingAids_Benefits_Stars')
					->setCellValue('EY1', 'WS_Gen_HearingAids_Limits_Stars')
					->setCellValue('EZ1', 'WS_Gen_BloodGlucoseMonitors_Benefits_Stars')
					->setCellValue('FA1', 'WS_Gen_BloodGlucoseMonitors_Limits_Stars')
					->setCellValue('FB1', 'WS_Gen_PreferredProviders')
					->setCellValue('FC1', 'PS_BatchId')
					->setCellValue('FD1', 'PS_PolicyCode')
					->setCellValue('FE1', 'PS_PolicyFileCode')
					->setCellValue('FF1', 'PS_PolicyUrl')
					->setCellValue('FG1', 'PS_FundName')
					->setCellValue('FH1', 'PS_ProductName')
					->setCellValue('FI1', 'PS_AvailableFor')
					->setCellValue('FJ1', 'PS_CoverFor')
					->setCellValue('FK1', 'PS_MonthlyPremium')
					->setCellValue('FL1', 'PS_MonthlyPremium_Value')
					->setCellValue('FM1', 'PS_Closed')
					->setCellValue('FN1', 'PS_Closed_Value')
					->setCellValue('FO1', 'PS_Corporate_Value')
					->setCellValue('FP1', 'PS_MedicareLevySurchargeStatus')
					->setCellValue('FQ1', 'PS_DentalGeneral')
					->setCellValue('FR1', 'PS_DentalGeneral_Covered')
					->setCellValue('FS1', 'PS_DentalGeneral_WaitingPeriod')
					->setCellValue('FT1', 'PS_DentalGeneral_WaitingPeriod_Days')
					->setCellValue('FU1', 'PS_DentalGeneral_CellValueBenefitLimits')
					->setCellValue('FV1', 'PS_DentalGeneral_CellValueMaxBenefits')
					->setCellValue('FW1', 'PS_DentalGeneral_BenefitLimits')
					->setCellValue('FX1', 'PS_DentalGeneral_BenefitLimits_Value')
					->setCellValue('FY1', 'PS_DentalGeneral_BenefitLimits_Description')
					->setCellValue('FZ1', 'PS_DentalGeneral_BenefitLimits_Liefetime')
					->setCellValue('GA1', 'PS_DentalGeneral_BenefitLimits_Liefetime_Value')
					->setCellValue('GB1', 'PS_DentalGeneral_BenefitLimits_Liefetime_Description')
					->setCellValue('GC1', 'PS_DentalGeneral_MaxBenefits_PeriodicOralExamination')
					->setCellValue('GD1', 'PS_DentalGeneral_MaxBenefits_ScaleAndClean')
					->setCellValue('GE1', 'PS_DentalGeneral_MaxBenefits_FluorideTreatment')
					->setCellValue('GF1', 'PS_DentalGeneral_MaxBenefits_SurgicalToothExtraction')
					->setCellValue('GG1', 'PS_DentalGeneral_MaxBenefits_PeriodicOralExamination_Value')
					->setCellValue('GH1', 'PS_DentalGeneral_MaxBenefits_ScaleAndClean_Value')
					->setCellValue('GI1', 'PS_DentalGeneral_MaxBenefits_FluorideTreatment_Value')
					->setCellValue('GJ1', 'PS_DentalGeneral_MaxBenefits_SurgicalToothExtraction_Value')
					->setCellValue('GK1', 'PS_DentalMajor')
					->setCellValue('GL1', 'PS_DentalMajor_Covered')
					->setCellValue('GM1', 'PS_DentalMajor_WaitingPeriod')
					->setCellValue('GN1', 'PS_DentalMajor_WaitingPeriod_Days')
					->setCellValue('GO1', 'PS_DentalMajor_CellValueBenefitLimits')
					->setCellValue('GP1', 'PS_DentalMajor_CellValueMaxBenefits')
					->setCellValue('GQ1', 'PS_DentalMajor_BenefitLimits')
					->setCellValue('GR1', 'PS_DentalMajor_BenefitLimits_Value')
					->setCellValue('GS1', 'PS_DentalMajor_BenefitLimits_Description')
					->setCellValue('GT1', 'PS_DentalMajor_BenefitLimits_Liefetime')
					->setCellValue('GU1', 'PS_DentalMajor_BenefitLimits_Liefetime_Value')
					->setCellValue('GV1', 'PS_DentalMajor_BenefitLimits_Liefetime_Description')
					->setCellValue('GW1', 'PS_DentalMajor_MaxBenefits_SurgicalToothExtraction')
					->setCellValue('GX1', 'PS_DentalMajor_MaxBenefits_FullCrownVeneered')
					->setCellValue('GY1', 'PS_DentalMajor_MaxBenefits_SurgicalToothExtraction_Value')
					->setCellValue('GZ1', 'PS_DentalMajor_MaxBenefits_FullCrownVeneered_Value')
					->setCellValue('HA1', 'PS_DentalEndodontic')
					->setCellValue('HB1', 'PS_DentalEndodontic_Covered')
					->setCellValue('HC1', 'PS_DentalEndodontic_WaitingPeriod')
					->setCellValue('HD1', 'PS_DentalEndodontic_WaitingPeriod_Days')
					->setCellValue('HE1', 'PS_DentalEndodontic_CellValueBenefitLimits')
					->setCellValue('HF1', 'PS_DentalEndodontic_CellValueMaxBenefits')
					->setCellValue('HG1', 'PS_DentalEndodontic_BenefitLimits')
					->setCellValue('HH1', 'PS_DentalEndodontic_BenefitLimits_Value')
					->setCellValue('HI1', 'PS_DentalEndodontic_BenefitLimits_Description')
					->setCellValue('HJ1', 'PS_DentalEndodontic_BenefitLimits_Liefetime')
					->setCellValue('HK1', 'PS_DentalEndodontic_BenefitLimits_Liefetime_Value')
					->setCellValue('HL1', 'PS_DentalEndodontic_BenefitLimits_Liefetime_Description')
					->setCellValue('HM1', 'PS_DentalEndodontic_MaxBenefits_FillingOfOneRootCanal')
					->setCellValue('HN1', 'PS_DentalEndodontic_MaxBenefits_FillingOfOneRootCanal_Value')
					->setCellValue('HO1', 'PS_DentalOrthodontic')
					->setCellValue('HP1', 'PS_DentalOrthodontic_Covered')
					->setCellValue('HQ1', 'PS_DentalOrthodontic_WaitingPeriod')
					->setCellValue('HR1', 'PS_DentalOrthodontic_WaitingPeriod_Days')
					->setCellValue('HS1', 'PS_DentalOrthodontic_CellValueBenefitLimits')
					->setCellValue('HT1', 'PS_DentalOrthodontic_CellValueMaxBenefits')
					->setCellValue('HU1', 'PS_DentalOrthodontic_BenefitLimits')
					->setCellValue('HV1', 'PS_DentalOrthodontic_BenefitLimits_Value')
					->setCellValue('HW1', 'PS_DentalOrthodontic_BenefitLimits_Description')
					->setCellValue('HX1', 'PS_DentalOrthodontic_BenefitLimits_Liefetime')
					->setCellValue('HY1', 'PS_DentalOrthodontic_BenefitLimits_Liefetime_Value')
					->setCellValue('HZ1', 'PS_DentalOrthodontic_BenefitLimits_Liefetime_Description')
					->setCellValue('IA1', 'PS_DentalOrthodontic_MaxBenefits_BracesRemovalFittingRetainer')
					->setCellValue('IB1', 'PS_DentalOrthodontic_MaxBenefits_BracesRemovalFittingRetainer_Value')
					->setCellValue('IC1', 'PS_Optical')
					->setCellValue('ID1', 'PS_Optical_Covered')
					->setCellValue('IE1', 'PS_Optical_WaitingPeriod')
					->setCellValue('IF1', 'PS_Optical_WaitingPeriod_Days')
					->setCellValue('IG1', 'PS_Optical_CellValueBenefitLimits')
					->setCellValue('IH1', 'PS_Optical_CellValueMaxBenefits')
					->setCellValue('II1', 'PS_Optical_BenefitLimits')
					->setCellValue('IJ1', 'PS_Optical_BenefitLimits_Value')
					->setCellValue('IK1', 'PS_Optical_BenefitLimits_Description')
					->setCellValue('IL1', 'PS_Optical_BenefitLimits_Liefetime')
					->setCellValue('IM1', 'PS_Optical_BenefitLimits_Liefetime_Value')
					->setCellValue('IN1', 'PS_Optical_BenefitLimits_Liefetime_Description')
					->setCellValue('IO1', 'PS_Optical_MaxBenefits_SingleVisionLensesAndFrames')
					->setCellValue('IP1', 'PS_Optical_MaxBenefits_MultiFocalLensesAndFrames')
					->setCellValue('IQ1', 'PS_Optical_MaxBenefits_SingleVisionLensesAndFrames_Value')
					->setCellValue('IR1', 'PS_Optical_MaxBenefits_MultiFocalLensesAndFrames_Value')
					->setCellValue('IS1', 'PS_NonPbsPharma')
					->setCellValue('IT1', 'PS_NonPbsPharma_Covered')
					->setCellValue('IU1', 'PS_NonPbsPharma_WaitingPeriod')
					->setCellValue('IV1', 'PS_NonPbsPharma_WaitingPeriod_Days')
					->setCellValue('IW1', 'PS_NonPbsPharma_CellValueBenefitLimits')
					->setCellValue('IX1', 'PS_NonPbsPharma_CellValueMaxBenefits')
					->setCellValue('IY1', 'PS_NonPbsPharma_BenefitLimits')
					->setCellValue('IZ1', 'PS_NonPbsPharma_BenefitLimits_Value')
					->setCellValue('JA1', 'PS_NonPbsPharma_BenefitLimits_Description')
					->setCellValue('JB1', 'PS_NonPbsPharma_BenefitLimits_Liefetime')
					->setCellValue('JC1', 'PS_NonPbsPharma_BenefitLimits_Liefetime_Value')
					->setCellValue('JD1', 'PS_NonPbsPharma_BenefitLimits_Liefetime_Description')
					->setCellValue('JE1', 'PS_NonPbsPharma_MaxBenefits_PerEligiblePrescription')
					->setCellValue('JF1', 'PS_NonPbsPharma_MaxBenefits_PerEligiblePrescription_Value')
					->setCellValue('JG1', 'PS_Physiotherapy')
					->setCellValue('JH1', 'PS_Physiotherapy_Covered')
					->setCellValue('JI1', 'PS_Physiotherapy_WaitingPeriod')
					->setCellValue('JJ1', 'PS_Physiotherapy_WaitingPeriod_Days')
					->setCellValue('JK1', 'PS_Physiotherapy_CellValueBenefitLimits')
					->setCellValue('JL1', 'PS_Physiotherapy_CellValueMaxBenefits')
					->setCellValue('JM1', 'PS_Physiotherapy_BenefitLimits')
					->setCellValue('JN1', 'PS_Physiotherapy_BenefitLimits_Value')
					->setCellValue('JO1', 'PS_Physiotherapy_BenefitLimits_Description')
					->setCellValue('JP1', 'PS_Physiotherapy_BenefitLimits_Liefetime')
					->setCellValue('JQ1', 'PS_Physiotherapy_BenefitLimits_Liefetime_Value')
					->setCellValue('JR1', 'PS_Physiotherapy_BenefitLimits_Liefetime_Description')
					->setCellValue('JS1', 'PS_Physiotherapy_MaxBenefits_InitiaVisit')
					->setCellValue('JT1', 'PS_Physiotherapy_MaxBenefits_SubsequentVisit')
					->setCellValue('JU1', 'PS_Physiotherapy_MaxBenefits_InitiaVisit_Value')
					->setCellValue('JV1', 'PS_Physiotherapy_MaxBenefits_SubsequentVisit_Value')
					->setCellValue('JW1', 'PS_Chiropractic')
					->setCellValue('JX1', 'PS_Chiropractic_Covered')
					->setCellValue('JY1', 'PS_Chiropractic_WaitingPeriod')
					->setCellValue('JZ1', 'PS_Chiropractic_WaitingPeriod_Days')
					->setCellValue('KA1', 'PS_Chiropractic_CellValueBenefitLimits')
					->setCellValue('KB1', 'PS_Chiropractic_CellValueMaxBenefits')
					->setCellValue('KC1', 'PS_Chiropractic_BenefitLimits')
					->setCellValue('KD1', 'PS_Chiropractic_BenefitLimits_Value')
					->setCellValue('KE1', 'PS_Chiropractic_BenefitLimits_Description')
					->setCellValue('KF1', 'PS_Chiropractic_BenefitLimits_Liefetime')
					->setCellValue('KG1', 'PS_Chiropractic_BenefitLimits_Liefetime_Value')
					->setCellValue('KH1', 'PS_Chiropractic_BenefitLimits_Liefetime_Description')
					->setCellValue('KI1', 'PS_Chiropractic_MaxBenefits_InitiaVisit')
					->setCellValue('KJ1', 'PS_Chiropractic_MaxBenefits_SubsequentVisit')
					->setCellValue('KK1', 'PS_Chiropractic_MaxBenefits_InitiaVisit_Value')
					->setCellValue('KL1', 'PS_Chiropractic_MaxBenefits_SubsequentVisit_Value')
					->setCellValue('KM1', 'PS_Podiatry')
					->setCellValue('KN1', 'PS_Podiatry_Covered')
					->setCellValue('KO1', 'PS_Podiatry_WaitingPeriod')
					->setCellValue('KP1', 'PS_Podiatry_WaitingPeriod_Days')
					->setCellValue('KQ1', 'PS_Podiatry_CellValueBenefitLimits')
					->setCellValue('KR1', 'PS_Podiatry_CellValueMaxBenefits')
					->setCellValue('KS1', 'PS_Podiatry_BenefitLimits')
					->setCellValue('KT1', 'PS_Podiatry_BenefitLimits_Value')
					->setCellValue('KU1', 'PS_Podiatry_BenefitLimits_Description')
					->setCellValue('KV1', 'PS_Podiatry_BenefitLimits_Liefetime')
					->setCellValue('KW1', 'PS_Podiatry_BenefitLimits_Liefetime_Value')
					->setCellValue('KX1', 'PS_Podiatry_BenefitLimits_Liefetime_Description')
					->setCellValue('KY1', 'PS_Podiatry_MaxBenefits_InitiaVisit')
					->setCellValue('KZ1', 'PS_Podiatry_MaxBenefits_SubsequentVisit')
					->setCellValue('LA1', 'PS_Podiatry_MaxBenefits_InitiaVisit_Value')
					->setCellValue('LB1', 'PS_Podiatry_MaxBenefits_SubsequentVisit_Value')
					->setCellValue('LC1', 'PS_Psychology')
					->setCellValue('LD1', 'PS_Psychology_Covered')
					->setCellValue('LE1', 'PS_Psychology_WaitingPeriod')
					->setCellValue('LF1', 'PS_Psychology_WaitingPeriod_Days')
					->setCellValue('LG1', 'PS_Psychology_CellValueBenefitLimits')
					->setCellValue('LH1', 'PS_Psychology_CellValueMaxBenefits')
					->setCellValue('LI1', 'PS_Psychology_BenefitLimits')
					->setCellValue('LJ1', 'PS_Psychology_BenefitLimits_Value')
					->setCellValue('LK1', 'PS_Psychology_BenefitLimits_Description')
					->setCellValue('LL1', 'PS_Psychology_BenefitLimits_Liefetime')
					->setCellValue('LM1', 'PS_Psychology_BenefitLimits_Liefetime_Value')
					->setCellValue('LN1', 'PS_Psychology_BenefitLimits_Liefetime_Description')
					->setCellValue('LO1', 'PS_Psychology_MaxBenefits_InitiaVisit')
					->setCellValue('LP1', 'PS_Psychology_MaxBenefits_SubsequentVisit')
					->setCellValue('LQ1', 'PS_Psychology_MaxBenefits_InitiaVisit_Value')
					->setCellValue('LR1', 'PS_Psychology_MaxBenefits_SubsequentVisit_Value')
					->setCellValue('LS1', 'PS_Acupuncture')
					->setCellValue('LT1', 'PS_Acupuncture_Covered')
					->setCellValue('LU1', 'PS_Acupuncture_WaitingPeriod')
					->setCellValue('LV1', 'PS_Acupuncture_WaitingPeriod_Days')
					->setCellValue('LW1', 'PS_Acupuncture_CellValueBenefitLimits')
					->setCellValue('LX1', 'PS_Acupuncture_CellValueMaxBenefits')
					->setCellValue('LY1', 'PS_Acupuncture_BenefitLimits')
					->setCellValue('LZ1', 'PS_Acupuncture_BenefitLimits_Value')
					->setCellValue('MA1', 'PS_Acupuncture_BenefitLimits_Description')
					->setCellValue('MB1', 'PS_Acupuncture_BenefitLimits_Liefetime')
					->setCellValue('MC1', 'PS_Acupuncture_BenefitLimits_Liefetime_Value')
					->setCellValue('MD1', 'PS_Acupuncture_BenefitLimits_Liefetime_Description')
					->setCellValue('ME1', 'PS_Acupuncture_MaxBenefits_InitiaVisit')
					->setCellValue('MF1', 'PS_Acupuncture_MaxBenefits_SubsequentVisit')
					->setCellValue('MG1', 'PS_Acupuncture_MaxBenefits_InitiaVisit_Value')
					->setCellValue('MH1', 'PS_Acupuncture_MaxBenefits_SubsequentVisit_Value')
					->setCellValue('MI1', 'PS_Naturopathy')
					->setCellValue('MJ1', 'PS_Naturopathy_Covered')
					->setCellValue('MK1', 'PS_Naturopathy_WaitingPeriod')
					->setCellValue('ML1', 'PS_Naturopathy_WaitingPeriod_Days')
					->setCellValue('MM1', 'PS_Naturopathy_CellValueBenefitLimits')
					->setCellValue('MN1', 'PS_Naturopathy_CellValueMaxBenefits')
					->setCellValue('MO1', 'PS_Naturopathy_BenefitLimits')
					->setCellValue('MP1', 'PS_Naturopathy_BenefitLimits_Value')
					->setCellValue('MQ1', 'PS_Naturopathy_BenefitLimits_Description')
					->setCellValue('MR1', 'PS_Naturopathy_BenefitLimits_Liefetime')
					->setCellValue('MS1', 'PS_Naturopathy_BenefitLimits_Liefetime_Value')
					->setCellValue('MT1', 'PS_Naturopathy_BenefitLimits_Liefetime_Description')
					->setCellValue('MU1', 'PS_Naturopathy_MaxBenefits_InitiaVisit')
					->setCellValue('MV1', 'PS_Naturopathy_MaxBenefits_SubsequentVisit')
					->setCellValue('MW1', 'PS_Naturopathy_MaxBenefits_InitiaVisit_Value')
					->setCellValue('MX1', 'PS_Naturopathy_MaxBenefits_SubsequentVisit_Value')
					->setCellValue('MY1', 'PS_RemedialMassage')
					->setCellValue('MZ1', 'PS_RemedialMassage_Covered')
					->setCellValue('NA1', 'PS_RemedialMassage_WaitingPeriod')
					->setCellValue('NB1', 'PS_RemedialMassage_WaitingPeriod_Days')
					->setCellValue('NC1', 'PS_RemedialMassage_CellValueBenefitLimits')
					->setCellValue('ND1', 'PS_RemedialMassage_CellValueMaxBenefits')
					->setCellValue('NE1', 'PS_RemedialMassage_BenefitLimits')
					->setCellValue('NF1', 'PS_RemedialMassage_BenefitLimits_Value')
					->setCellValue('NG1', 'PS_RemedialMassage_BenefitLimits_Description')
					->setCellValue('NH1', 'PS_RemedialMassage_BenefitLimits_Liefetime')
					->setCellValue('NI1', 'PS_RemedialMassage_BenefitLimits_Liefetime_Value')
					->setCellValue('NJ1', 'PS_RemedialMassage_BenefitLimits_Liefetime_Description')
					->setCellValue('NK1', 'PS_RemedialMassage_MaxBenefits_InitiaVisit')
					->setCellValue('NL1', 'PS_RemedialMassage_MaxBenefits_SubsequentVisit')
					->setCellValue('NM1', 'PS_RemedialMassage_MaxBenefits_InitiaVisit_Value')
					->setCellValue('NN1', 'PS_RemedialMassage_MaxBenefits_SubsequentVisit_Value')
					->setCellValue('NO1', 'PS_HearingAids')
					->setCellValue('NP1', 'PS_HearingAids_Covered')
					->setCellValue('NQ1', 'PS_HearingAids_WaitingPeriod')
					->setCellValue('NR1', 'PS_HearingAids_WaitingPeriod_Days')
					->setCellValue('NS1', 'PS_HearingAids_CellValueBenefitLimits')
					->setCellValue('NT1', 'PS_HearingAids_CellValueMaxBenefits')
					->setCellValue('NU1', 'PS_HearingAids_BenefitLimits')
					->setCellValue('NV1', 'PS_HearingAids_BenefitLimits_Value')
					->setCellValue('NW1', 'PS_HearingAids_BenefitLimits_Description')
					->setCellValue('NX1', 'PS_HearingAids_BenefitLimits_Liefetime')
					->setCellValue('NY1', 'PS_HearingAids_BenefitLimits_Liefetime_Value')
					->setCellValue('NZ1', 'PS_HearingAids_BenefitLimits_Liefetime_Description')
					->setCellValue('OA1', 'PS_HearingAids_MaxBenefits_PerHearingAid')
					->setCellValue('OB1', 'PS_HearingAids_MaxBenefits_PerHearingAid_Value')
					->setCellValue('OC1', 'PS_BloodGlucoseMonitors')
					->setCellValue('OD1', 'PS_BloodGlucoseMonitors_Covered')
					->setCellValue('OE1', 'PS_BloodGlucoseMonitors_WaitingPeriod')
					->setCellValue('OF1', 'PS_BloodGlucoseMonitors_WaitingPeriod_Days')
					->setCellValue('OG1', 'PS_BloodGlucoseMonitors_CellValueBenefitLimits')
					->setCellValue('OH1', 'PS_BloodGlucoseMonitors_CellValueMaxBenefits')
					->setCellValue('OI1', 'PS_BloodGlucoseMonitors_BenefitLimits')
					->setCellValue('OJ1', 'PS_BloodGlucoseMonitors_BenefitLimits_Value')
					->setCellValue('OK1', 'PS_BloodGlucoseMonitors_BenefitLimits_Description')
					->setCellValue('OL1', 'PS_BloodGlucoseMonitors_BenefitLimits_Liefetime')
					->setCellValue('OM1', 'PS_BloodGlucoseMonitors_BenefitLimits_Liefetime_Value')
					->setCellValue('ON1', 'PS_BloodGlucoseMonitors_BenefitLimits_Liefetime_Description')
					->setCellValue('OO1', 'PS_BloodGlucoseMonitors_MaxBenefits_PerMonitor')
					->setCellValue('OP1', 'PS_BloodGlucoseMonitors_MaxBenefits_PerMonitor_Value')
					->setCellValue('OQ1', 'PS_Ambulance')
					->setCellValue('OR1', 'PS_Ambulance_Covered')
					->setCellValue('OS1', 'PS_Ambulance_WaitingPeriod')
					->setCellValue('OT1', 'PS_Ambulance_WaitingPeriod_Days')
					->setCellValue('OU1', 'PS_Ambulance_CellValueBenefitLimits')
					->setCellValue('OV1', 'PS_Ambulance_CellValueMaxBenefits')
					->setCellValue('OW1', 'PS_Ambulance_BenefitLimits')
					->setCellValue('OX1', 'PS_Ambulance_BenefitLimits_Value')
					->setCellValue('OY1', 'PS_Ambulance_BenefitLimits_Description')
					->setCellValue('OZ1', 'PS_Ambulance_BenefitLimits_Liefetime')
					->setCellValue('PA1', 'PS_Ambulance_BenefitLimits_Liefetime_Value')
					->setCellValue('PB1', 'PS_Ambulance_BenefitLimits_Liefetime_Description')
					->setCellValue('PC1', 'PS_PreferredServiceProviderArrangements')
					->setCellValue('PD1', 'PS_OtherFeatures')
					->setCellValue('PE1', 'PS_DateIssued')
					->setCellValue('PF1', 'WS_Hos_Excess_Filter_Value')
					->setCellValue('PG1', 'WS_Hos_CoPayment_Filter_Value');

	$styleArray = array(
			'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => '800000'),
				'size'  => 8,
				'name'  => 'Verdana'
			));
	$styleArray2=array(
			'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => '000000'),
				'size'  => 11,
				'name'  => 'Calibri'
			),
			'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
				));
	$styleArray3 = array(
			'font'  => array(
				'bold'  => false,
				'size'  => 8,
				'name'  => 'Verdana'
			));
	$styleArray4 = array(
			'font'  => array(
				'bold'  => false,
				'size'  => 8,
				'name'  => 'Verdana'
			),
			'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
				)
			);
	
	//*******************************************Export Excel Heading Start Here ****************************//
    $objPHPExcel->getActiveSheet()->getStyle('A1:B1:C1:D1:E1:F1');
	$objPHPExcel->getActiveSheet()->getStyle("N1:O1:P1:Q1:R1:S1");
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth('20');
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('AM')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('AN')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('AO')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('AP')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('AQ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('AR')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('AS')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('AT')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('AU')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('AV')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('AW')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('AX')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('AY')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('AZ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('BA')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('BB')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('BC')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('BD')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('BE')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('BF')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('BG')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('BH')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('BI')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('BJ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('BK')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('BL')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('BM')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('BN')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('BO')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('BP')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('BQ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('BR')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('BS')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('BT')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('BU')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('BV')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('BW')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('BX')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('BY')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('BZ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('CA')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('CB')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('CC')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('CD')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('CE')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('CF')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('CG')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('CH')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('CI')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('CJ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('CK')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('CL')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('CM')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('CN')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('CO')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('CP')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('CQ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('CR')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('CS')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('CT')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('CU')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('CV')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('CW')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('CX')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('CY')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('CZ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('DA')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('DB')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('DC')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('DD')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('DE')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('DF')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('DG')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('DH')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('DI')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('DJ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('DK')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('DL')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('DM')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('DN')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('DO')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('DP')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('DQ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('DR')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('DS')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('DT')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('DU')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('DV')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('DW')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('DX')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('DY')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('DZ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('EA')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('EB')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('EC')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('ED')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('EE')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('EF')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('EG')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('EH')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('EI')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('EJ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('EK')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('EL')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('EM')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('EN')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('EO')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('EP')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('EQ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('ER')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('ES')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('ET')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('EU')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('EV')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('EW')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('EX')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('EY')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('EZ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('FA')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('FB')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('FC')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('FD')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('FE')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('FF')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('FG')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('FH')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('FI')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('FJ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('FK')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('FL')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('FM')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('FN')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('FO')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('FP')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('FQ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('FR')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('FS')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('FT')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('FU')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('FV')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('FW')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('FX')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('FY')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('FZ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('GA')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('GB')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('GC')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('GD')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('GE')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('GF')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('GG')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('GH')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('GI')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('GJ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('GK')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('GL')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('GM')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('GN')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('GO')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('GP')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('GQ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('GR')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('GS')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('GT')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('GU')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('GV')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('GW')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('GX')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('GY')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('GZ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('HA')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('HB')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('HC')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('HD')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('HE')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('HF')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('HG')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('HH')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('HI')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('HJ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('HK')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('HL')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('HM')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('HN')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('HO')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('HP')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('HQ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('HR')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('HS')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('HT')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('HU')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('HV')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('HW')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('HX')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('HY')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('HZ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('IA')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('IB')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('IC')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('ID')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('IE')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('IF')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('IG')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('IH')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('II')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('IJ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('IK')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('IL')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('IM')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('IN')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('IO')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('IP')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('IQ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('IR')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('IS')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('IT')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('IU')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('IV')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('IW')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('IX')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('IY')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('IZ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('JA')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('JB')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('JC')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('JD')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('JE')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('JF')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('JG')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('JH')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('JI')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('JJ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('JK')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('JL')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('JM')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('JN')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('JO')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('JP')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('JQ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('JR')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('JS')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('JT')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('JU')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('JV')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('JW')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('JX')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('JY')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('JZ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('KA')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('KB')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('KC')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('KD')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('KE')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('KF')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('KG')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('KH')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('KI')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('KJ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('KK')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('KL')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('KM')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('KN')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('KO')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('KP')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('KQ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('KR')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('KS')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('KT')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('KU')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('KV')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('KW')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('KX')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('KY')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('KZ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('LA')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('LB')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('LC')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('LD')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('LE')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('LF')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('LG')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('LH')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('LI')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('LJ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('LK')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('LL')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('LM')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('LN')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('LO')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('LP')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('LQ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('LR')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('LS')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('LT')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('LU')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('LV')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('LW')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('LX')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('LY')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('LZ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('MA')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('MB')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('MC')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('MD')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('ME')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('MF')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('MG')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('MH')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('MI')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('MJ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('MK')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('ML')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('MM')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('MN')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('MO')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('MP')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('MQ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('MR')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('MS')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('MT')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('MU')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('MV')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('MW')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('MX')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('MY')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('MZ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('NA')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('NB')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('NC')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('ND')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('NE')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('NF')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('NG')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('NH')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('NI')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('NJ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('NK')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('NL')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('NM')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('NN')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('NO')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('NP')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('NQ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('NR')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('NS')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('NT')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('NU')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('NV')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('NW')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('NX')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('NY')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('NZ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('OA')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('OB')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('OC')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('OD')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('OE')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('OF')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('OG')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('OH')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('OI')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('OJ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('OK')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('OL')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('OM')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('ON')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('OO')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('OP')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('OQ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('OR')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('OS')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('OT')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('OU')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('OV')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('OW')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('OX')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('OY')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('OZ')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('PA')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('PB')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('PC')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('PD')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('PE')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('PF')->setWidth('30');
	$objPHPExcel->getActiveSheet()->getColumnDimension('PG')->setWidth('30');

	/*******************************Export Excel Heading End Here ****************************/
	$insurance_qry_str ="SELECT * FROM tbl_policies as plcy JOIN tbl_ps1_policies as ps1 on plcy.insurance_id = ps1.insurance_id JOIN tbl_ps2_policies as ps2 on plcy.insurance_id = ps2.insurance_id	JOIN tbl_ws_policies as ws on plcy.insurance_id = ws.insurance_id order by plcy.insurance_id ASC";
    $get_insurance_qry = $dbconn->prepare($insurance_qry_str);
	$get_insurance_qry->execute();
	$get_insurance_rowCount	= $get_insurance_qry->rowCount();
    
    if ($get_insurance_rowCount>0) {					
		$get_insurance_data = $get_insurance_qry->fetchAll(PDO::FETCH_ASSOC);										
		if ($get_insurance_data) {							
			$si=2;
			$sno=1;
			$state_name="";
			foreach ($get_insurance_data as $getResRow){
          		$insurance_id=checknull($getResRow['insurance_id']);
				$WS_BatchId=checknull($getResRow['WS_BatchId']);
				$WS_PolicyCode=checknull($getResRow['WS_PolicyCode']);
				$WS_PolicyUrl=checknull($getResRow['WS_PolicyUrl']);
				$WS_PolicyName=checknull($getResRow['WS_PolicyName']);
				$WS_ProductName=checknull($getResRow['WS_ProductName']);
				$WS_Status=checknull($getResRow['WS_Status']);
				$WS_Corporate=checknull($getResRow['WS_Corporate']);
				$WS_OverseasVisitors=checknull($getResRow['WS_OverseasVisitors']);
				$WS_Enabled=checknull($getResRow['WS_Enabled']);
				$WS_Visible=checknull($getResRow['WS_Visible']);
				$WS_FundId=checknull($getResRow['WS_FundId']);
				$WS_Fund=checknull($getResRow['WS_Fund']);
				$WS_FundName=checknull($getResRow['WS_FundName']);
				$WS_StateId=checknull($getResRow['WS_StateId']);
				$WS_State=checknull($getResRow['WS_State']);
				$WS_StateName=checknull($getResRow['WS_StateName']);
				$WS_CoverTypeId=checknull($getResRow['WS_CoverTypeId']);
				$WS_CoverType=checknull($getResRow['WS_CoverType']);
				$WS_CoverLevelHospitalId=checknull($getResRow['WS_CoverLevelHospitalId']);
				$WS_CoverLevelHospital=checknull($getResRow['WS_CoverLevelHospital']);
				$WS_CoverLevelGeneralId=checknull($getResRow['WS_CoverLevelGeneralId']);
				$WS_CoverLevelGeneral=checknull($getResRow['WS_CoverLevelGeneral']);
				$WS_CategoryId=checknull($getResRow['WS_CategoryId']);
				$WS_Category=checknull($getResRow['WS_Category']);
				$WS_CategoryOriginalName=checknull($getResRow['WS_CategoryOriginalName']);
				$WS_Hos_Excess_PerHospitalVisit=checknull($getResRow['WS_Hos_Excess_PerHospitalVisit']);
				$WS_Hos_Excess_MaximumPerPerson=checknull($getResRow['WS_Hos_Excess_MaximumPerPerson']);
				$WS_Hos_Excess_MaximumPerAnnum=checknull($getResRow['WS_Hos_Excess_MaximumPerAnnum']);
				$WS_Hos_CoPayments=checknull($getResRow['WS_Hos_CoPayments']);
				$WS_Hos_CoPayment_SharedRoom=checknull($getResRow['WS_Hos_CoPayment_SharedRoom']);
				$WS_Hos_CoPayment_PrivateRoom=checknull($getResRow['WS_Hos_CoPayment_PrivateRoom']);
				$WS_Hos_CoPayment_DaySurgery=checknull($getResRow['WS_Hos_CoPayment_DaySurgery']);
				$WS_Hos_OtherFeatures=checknull($getResRow['WS_Hos_OtherFeatures']);
				$WS_Hos_Ambulance=checknull($getResRow['WS_Hos_Ambulance']);
				$WS_Hos_AmbulanceCover=checknull($getResRow['WS_Hos_AmbulanceCover']);
				$WS_Gen_AmbulanceCover=checknull($getResRow['WS_Gen_AmbulanceCover']);
				$WS_YearlyPremium=checknull($getResRow['WS_YearlyPremium']);
				$WS_YearlyPremium_StdLessThanAge65Rebate=checknull($getResRow['WS_YearlyPremium_StdLessThanAge65Rebate']);
				$WS_YearlyPremium_StdAge65ToAge69Rebate=checknull($getResRow['WS_YearlyPremium_StdAge65ToAge69Rebate']);
				$WS_YearlyPremium_StdAge70AndGreaterRebate=checknull($getResRow['WS_YearlyPremium_StdAge70AndGreaterRebate']);
				$WS_YearlyPremium_T1LessThanAge65Rebate=checknull($getResRow['WS_YearlyPremium_T1LessThanAge65Rebate']);
				$WS_YearlyPremium_T1Age65ToAge69Rebate=checknull($getResRow['WS_YearlyPremium_T1Age65ToAge69Rebate']);
				$WS_YearlyPremium_T1Age70AndGreaterRebate=checknull($getResRow['WS_YearlyPremium_T1Age70AndGreaterRebate']);
				$WS_YearlyPremium_T2LessThanAge65Rebate=checknull($getResRow['WS_YearlyPremium_T2LessThanAge65Rebate']);
				$WS_YearlyPremium_T2Age65ToAge69Rebate=checknull($getResRow['WS_YearlyPremium_T2Age65ToAge69Rebate']);
				$WS_YearlyPremium_T2Age70AndGreaterRebate=checknull($getResRow['WS_YearlyPremium_T2Age70AndGreaterRebate']);
				$WS_HalfYearlyPremium=checknull($getResRow['WS_HalfYearlyPremium']);
				$WS_HalfYearlyPremium_StdLessThanAge65Rebate=checknull($getResRow['WS_HalfYearlyPremium_StdLessThanAge65Rebate']);
				$WS_HalfYearlyPremium_StdAge65ToAge69Rebate=checknull($getResRow['WS_HalfYearlyPremium_StdAge65ToAge69Rebate']);
				$WS_HalfYearlyPremium_StdAge70AndGreaterRebate=checknull($getResRow['WS_HalfYearlyPremium_StdAge70AndGreaterRebate']);
				$WS_HalfYearlyPremium_T1LessThanAge65Rebate=checknull($getResRow['WS_HalfYearlyPremium_T1LessThanAge65Rebate']);
				$WS_HalfYearlyPremium_T1Age65ToAge69Rebate=checknull($getResRow['WS_HalfYearlyPremium_T1Age65ToAge69Rebate']);
				$WS_HalfYearlyPremium_T1Age70AndGreaterRebate=checknull($getResRow['WS_HalfYearlyPremium_T1Age70AndGreaterRebate']);
				$WS_HalfYearlyPremium_T2LessThanAge65Rebate=checknull($getResRow['WS_HalfYearlyPremium_T2LessThanAge65Rebate']);
				$WS_HalfYearlyPremium_T2Age65ToAge69Rebate=checknull($getResRow['WS_HalfYearlyPremium_T2Age65ToAge69Rebate']);
				$WS_HalfYearlyPremium_T2Age70AndGreaterRebate=checknull($getResRow['WS_HalfYearlyPremium_T2Age70AndGreaterRebate']);
				$WS_QuarterlyPremium=checknull($getResRow['WS_QuarterlyPremium']);
				$WS_QuarterlyPremium_StdLessThanAge65Rebate=checknull($getResRow['WS_QuarterlyPremium_StdLessThanAge65Rebate']);
				$WS_QuarterlyPremium_StdAge65ToAge69Rebate=checknull($getResRow['WS_QuarterlyPremium_StdAge65ToAge69Rebate']);
				$WS_QuarterlyPremium_StdAge70AndGreaterRebate=checknull($getResRow['WS_QuarterlyPremium_StdAge70AndGreaterRebate']);
				$WS_QuarterlyPremium_T1LessThanAge65Rebate=checknull($getResRow['WS_QuarterlyPremium_T1LessThanAge65Rebate']);
				$WS_QuarterlyPremium_T1Age65ToAge69Rebate=checknull($getResRow['WS_QuarterlyPremium_T1Age65ToAge69Rebate']);
				$WS_QuarterlyPremium_T1Age70AndGreaterRebate=checknull($getResRow['WS_QuarterlyPremium_T1Age70AndGreaterRebate']);
				$WS_QuarterlyPremium_T2LessThanAge65Rebate=checknull($getResRow['WS_QuarterlyPremium_T2LessThanAge65Rebate']);
				$WS_QuarterlyPremium_T2Age65ToAge69Rebate=checknull($getResRow['WS_QuarterlyPremium_T2Age65ToAge69Rebate']);
				$WS_QuarterlyPremium_T2Age70AndGreaterRebate=checknull($getResRow['WS_QuarterlyPremium_T2Age70AndGreaterRebate']);
				$WS_MonthlyPremium=checknull($getResRow['WS_MonthlyPremium']);
				$WS_MonthlyPremium_StdLessThanAge65Rebate=checknull($getResRow['WS_MonthlyPremium_StdLessThanAge65Rebate']);
				$WS_MonthlyPremium_StdAge65ToAge69Rebate=checknull($getResRow['WS_MonthlyPremium_StdAge65ToAge69Rebate']);
				$WS_MonthlyPremium_StdAge70AndGreaterRebate=checknull($getResRow['WS_MonthlyPremium_StdAge70AndGreaterRebate']);
				$WS_MonthlyPremium_T1LessThanAge65Rebate=checknull($getResRow['WS_MonthlyPremium_T1LessThanAge65Rebate']);
				$WS_MonthlyPremium_T1Age65ToAge69Rebate=checknull($getResRow['WS_MonthlyPremium_T1Age65ToAge69Rebate']);
				$WS_MonthlyPremium_T1Age70AndGreaterRebate=checknull($getResRow['WS_MonthlyPremium_T1Age70AndGreaterRebate']);
				$WS_MonthlyPremium_T2LessThanAge65Rebate=checknull($getResRow['WS_MonthlyPremium_T2LessThanAge65Rebate']);
				$WS_MonthlyPremium_T2Age65ToAge69Rebate=checknull($getResRow['WS_MonthlyPremium_T2Age65ToAge69Rebate']);
				$WS_MonthlyPremium_T2Age70AndGreaterRebate=checknull($getResRow['WS_MonthlyPremium_T2Age70AndGreaterRebate']);
				$WS_FortnightlyPremium=checknull($getResRow['WS_FortnightlyPremium']);
				$WS_FortnightlyPremium_StdLessThanAge65Rebate=checknull($getResRow['WS_FortnightlyPremium_StdLessThanAge65Rebate']);
				$WS_FortnightlyPremium_StdAge65ToAge69Rebate=checknull($getResRow['WS_FortnightlyPremium_StdAge65ToAge69Rebate']);
				$WS_FortnightlyPremium_StdAge70AndGreaterRebate=checknull($getResRow['WS_FortnightlyPremium_StdAge70AndGreaterRebate']);
				$WS_FortnightlyPremium_T1LessThanAge65Rebate=checknull($getResRow['WS_FortnightlyPremium_T1LessThanAge65Rebate']);
				$WS_FortnightlyPremium_T1Age65ToAge69Rebate=checknull($getResRow['WS_FortnightlyPremium_T1Age65ToAge69Rebate']);
				$WS_FortnightlyPremium_T1Age70AndGreaterRebate=checknull($getResRow['WS_FortnightlyPremium_T1Age70AndGreaterRebate']);
				$WS_FortnightlyPremium_T2LessThanAge65Rebate=checknull($getResRow['WS_FortnightlyPremium_T2LessThanAge65Rebate']);
				$WS_FortnightlyPremium_T2Age65ToAge69Rebate=checknull($getResRow['WS_FortnightlyPremium_T2Age65ToAge69Rebate']);
				$WS_FortnightlyPremium_T2Age70AndGreaterRebate=checknull($getResRow['WS_FortnightlyPremium_T2Age70AndGreaterRebate']);
				$WS_WeeklyPremium=checknull($getResRow['WS_WeeklyPremium']);
				$WS_WeeklyPremium_StdLessThanAge65Rebate=checknull($getResRow['WS_WeeklyPremium_StdLessThanAge65Rebate']);
				$WS_WeeklyPremium_StdAge65ToAge69Rebate=checknull($getResRow['WS_WeeklyPremium_StdAge65ToAge69Rebate']);
				$WS_WeeklyPremium_StdAge70AndGreaterRebate=checknull($getResRow['WS_WeeklyPremium_StdAge70AndGreaterRebate']);
				$WS_WeeklyPremium_T1LessThanAge65Rebate=checknull($getResRow['WS_WeeklyPremium_T1LessThanAge65Rebate']);
				$WS_WeeklyPremium_T1Age65ToAge69Rebate=checknull($getResRow['WS_WeeklyPremium_T1Age65ToAge69Rebate']);
				$WS_WeeklyPremium_T1Age70AndGreaterRebate=checknull($getResRow['WS_WeeklyPremium_T1Age70AndGreaterRebate']);
				$WS_WeeklyPremium_T2LessThanAge65Rebate=checknull($getResRow['WS_WeeklyPremium_T2LessThanAge65Rebate']);
				$WS_WeeklyPremium_T2Age65ToAge69Rebate=checknull($getResRow['WS_WeeklyPremium_T2Age65ToAge69Rebate']);
				$WS_WeeklyPremium_T2Age70AndGreaterRebate=checknull($getResRow['WS_WeeklyPremium_T2Age70AndGreaterRebate']);
				$WS_Hos_Accommodation_Covered=checknull($getResRow['WS_Hos_Accommodation_Covered']);
				$WS_Hos_Accommodation_Covered_Value=checknull($getResRow['WS_Hos_Accommodation_Covered_Value']);
				$WS_Hos_CardiacAndRelatedServices_Covered=checknull($getResRow['WS_Hos_CardiacAndRelatedServices_Covered']);
				$WS_Hos_CardiacAndRelatedServices_Covered_Value=checknull($getResRow['WS_Hos_CardiacAndRelatedServices_Covered_Value']);
				$WS_Hos_CataractAndEyeLensProcedures_Covered=checknull($getResRow['WS_Hos_CataractAndEyeLensProcedures_Covered']);
				$WS_Hos_CataractAndEyeLensProcedures_Covered_Value=checknull($getResRow['WS_Hos_CataractAndEyeLensProcedures_Covered_Value']);
				$WS_Hos_PregnancyAndBirthRelatedServices_Covered=checknull($getResRow['WS_Hos_PregnancyAndBirthRelatedServices_Covered']);
				$WS_Hos_PregnancyAndBirthRelatedServices_Covered_Value=checknull($getResRow['WS_Hos_PregnancyAndBirthRelatedServices_Covered_Value']);
				$WS_Hos_AssistedReproductiveServices_Covered=checknull($getResRow['WS_Hos_AssistedReproductiveServices_Covered']);
				$WS_Hos_AssistedReproductiveServices_Covered_Value=checknull($getResRow['WS_Hos_AssistedReproductiveServices_Covered_Value']);
				$WS_Hos_JointReplacement_HipKneeShoulderAndElbow_Covered=checknull($getResRow['WS_Hos_JointReplacement_HipKneeShoulderAndElbow_Covered']);
				$WS_Hos_JointReplacement_HipKneeShoulderAndElbow_Covered_Value=checknull($getResRow['WS_Hos_JointReplacement_HipKneeShoulderAndElbow_Covered_Value']);
				$WS_Hos_JointReplacement_HipKneeOnly_Covered=checknull($getResRow['WS_Hos_JointReplacement_HipKneeOnly_Covered']);
				$WS_Hos_JointReplacement_HipKneeOnly_Covered_Value=checknull($getResRow['WS_Hos_JointReplacement_HipKneeOnly_Covered_Value']);
				$WS_Hos_JointReplacement_HipOnly_Covered=checknull($getResRow['WS_Hos_JointReplacement_HipOnly_Covered']);
				$WS_Hos_JointReplacement_HipOnly_Covered_Value=checknull($getResRow['WS_Hos_JointReplacement_HipOnly_Covered_Value']);
				$WS_Hos_DialysisForChronicRenalFailure_Covered=checknull($getResRow['WS_Hos_DialysisForChronicRenalFailure_Covered']);
				$WS_Hos_DialysisForChronicRenalFailure_Covered_Value=checknull($getResRow['WS_Hos_DialysisForChronicRenalFailure_Covered_Value']);
				$WS_Hos_GastricBandingAndRelatedServices_Covered=checknull($getResRow['WS_Hos_GastricBandingAndRelatedServices_Covered']);
				$WS_Hos_GastricBandingAndRelatedServices_Covered_Value=checknull($getResRow['WS_Hos_GastricBandingAndRelatedServices_Covered_Value']);
				$WS_Hos_Sterilisation_Covered=checknull($getResRow['WS_Hos_Sterilisation_Covered']);
				$WS_Hos_Sterilisation_Covered_Value=checknull($getResRow['WS_Hos_Sterilisation_Covered_Value']);
				$WS_Hos_NonCosmeticPlasticSurgery_Covered=checknull($getResRow['WS_Hos_NonCosmeticPlasticSurgery_Covered']);
				$WS_Hos_NonCosmeticPlasticSurgery_Covered_Value=checknull($getResRow['WS_Hos_NonCosmeticPlasticSurgery_Covered_Value']);
				$WS_Hos_Rehabilitation_Covered=checknull($getResRow['WS_Hos_Rehabilitation_Covered']);
				$WS_Hos_Rehabilitation_Covered_Value=checknull($getResRow['WS_Hos_Rehabilitation_Covered_Value']);
				$WS_Hos_PsychiatricServices_Covered=checknull($getResRow['WS_Hos_PsychiatricServices_Covered']);
				$WS_Hos_PsychiatricServices_Covered_Value=checknull($getResRow['WS_Hos_PsychiatricServices_Covered_Value']);
				$WS_Hos_PalliativeCare_Covered=checknull($getResRow['WS_Hos_PalliativeCare_Covered']);
				$WS_Hos_PalliativeCare_Covered_Value=checknull($getResRow['WS_Hos_PalliativeCare_Covered_Value']);
				$WS_Gen_Dental_GeneralDental_Benefits_Stars=checknull($getResRow['WS_Gen_Dental_GeneralDental_Benefits_Stars']);
				$WS_Gen_Dental_GeneralDental_Limits_Stars=checknull($getResRow['WS_Gen_Dental_GeneralDental_Limits_Stars']);
				$WS_Gen_Dental_MajorDental_Benefits_Stars=checknull($getResRow['WS_Gen_Dental_MajorDental_Benefits_Stars']);
				$WS_Gen_Dental_MajorDental_Limits_Stars=checknull($getResRow['WS_Gen_Dental_MajorDental_Limits_Stars']);
				$WS_Gen_Dental_Endodontic_Benefits_Stars=checknull($getResRow['WS_Gen_Dental_Endodontic_Benefits_Stars']);
				$WS_Gen_Dental_Endodontic_Limits_Stars=checknull($getResRow['WS_Gen_Dental_Endodontic_Limits_Stars']);
				$WS_Gen_Dental_Orthodontic_Benefits_Stars=checknull($getResRow['WS_Gen_Dental_Orthodontic_Benefits_Stars']);
				$WS_Gen_Dental_Orthodontic_Limits_Stars=checknull($getResRow['WS_Gen_Dental_Orthodontic_Limits_Stars']);
				$WS_Gen_Optical_Benefits_Stars=checknull($getResRow['WS_Gen_Optical_Benefits_Stars']);
				$WS_Gen_Optical_Limits_Stars=checknull($getResRow['WS_Gen_Optical_Limits_Stars']);
				$WS_Gen_NonPbsPharmaceuticals_Benefits_Stars=checknull($getResRow['WS_Gen_NonPbsPharmaceuticals_Benefits_Stars']);
				$WS_Gen_NonPbsPharmaceuticals_Limits_Stars=checknull($getResRow['WS_Gen_NonPbsPharmaceuticals_Limits_Stars']);
				$WS_Gen_Physiotherapy_Benefits_Stars=checknull($getResRow['WS_Gen_Physiotherapy_Benefits_Stars']);
				$WS_Gen_Physiotherapy_Limits_Stars=checknull($getResRow['WS_Gen_Physiotherapy_Limits_Stars']);
				$WS_Gen_Chiropractic_Benefits_Stars=checknull($getResRow['WS_Gen_Chiropractic_Benefits_Stars']);
				$WS_Gen_Chiropractic_Limits_Stars=checknull($getResRow['WS_Gen_Chiropractic_Limits_Stars']);
				$WS_Gen_Podiatry_Benefits_Stars=checknull($getResRow['WS_Gen_Podiatry_Benefits_Stars']);
				$WS_Gen_Podiatry_Limits_Stars=checknull($getResRow['WS_Gen_Podiatry_Limits_Stars']);
				$WS_Gen_Psychology_Benefits_Stars=checknull($getResRow['WS_Gen_Psychology_Benefits_Stars']);
				$WS_Gen_Psychology_Limits_Stars=checknull($getResRow['WS_Gen_Psychology_Limits_Stars']);
				$WS_Gen_Acupuncture_Benefits_Stars=checknull($getResRow['WS_Gen_Acupuncture_Benefits_Stars']);
				$WS_Gen_Acupuncture_Limits_Stars=checknull($getResRow['WS_Gen_Acupuncture_Limits_Stars']);
				$WS_Gen_Naturopathy_Benefits_Stars=checknull($getResRow['WS_Gen_Naturopathy_Benefits_Stars']);
				$WS_Gen_Naturopathy_Limits_Stars=checknull($getResRow['WS_Gen_Naturopathy_Limits_Stars']);
				$WS_Gen_RemedialMassage_Benefits_Stars=checknull($getResRow['WS_Gen_RemedialMassage_Benefits_Stars']);
				$WS_Gen_RemedialMassage_Limits_Stars=checknull($getResRow['WS_Gen_RemedialMassage_Limits_Stars']);
				$WS_Gen_HearingAids_Benefits_Stars=checknull($getResRow['WS_Gen_HearingAids_Benefits_Stars']);
				$WS_Gen_HearingAids_Limits_Stars=checknull($getResRow['WS_Gen_HearingAids_Limits_Stars']);
				$WS_Gen_BloodGlucoseMonitors_Benefits_Stars=checknull($getResRow['WS_Gen_BloodGlucoseMonitors_Benefits_Stars']);
				$WS_Gen_BloodGlucoseMonitors_Limits_Stars=checknull($getResRow['WS_Gen_BloodGlucoseMonitors_Limits_Stars']);
				$WS_Gen_PreferredProviders=checknull($getResRow['WS_Gen_PreferredProviders']);
				$PS_BatchId=checknull($getResRow['PS_BatchId']);
				$PS_PolicyCode=checknull($getResRow['PS_PolicyCode']);
				$PS_PolicyFileCode=checknull($getResRow['PS_PolicyFileCode']);
				$PS_PolicyUrl=checknull($getResRow['PS_PolicyUrl']);
				$PS_FundName=checknull($getResRow['PS_FundName']);
				$PS_ProductName=checknull($getResRow['PS_ProductName']);
				$PS_AvailableFor=checknull($getResRow['PS_AvailableFor']);
				$PS_CoverFor=checknull($getResRow['PS_CoverFor']);
				$PS_MonthlyPremium=checknull($getResRow['PS_MonthlyPremium']);
				$PS_MonthlyPremium_Value=checknull($getResRow['PS_MonthlyPremium_Value']);
				$PS_Closed=checknull($getResRow['PS_Closed']);
				$PS_Closed_Value=checknull($getResRow['PS_Closed_Value']);
				$PS_Corporate_Value=checknull($getResRow['PS_Corporate_Value']);
				$PS_MedicareLevySurchargeStatus=checknull($getResRow['PS_MedicareLevySurchargeStatus']);
				$PS_DentalGeneral=checknull($getResRow['PS_DentalGeneral']);
				$PS_DentalGeneral_Covered=checknull($getResRow['PS_DentalGeneral_Covered']);
				$PS_DentalGeneral_WaitingPeriod=checknull($getResRow['PS_DentalGeneral_WaitingPeriod']);
				$PS_DentalGeneral_WaitingPeriod_Days=checknull($getResRow['PS_DentalGeneral_WaitingPeriod_Days']);
				$PS_DentalGeneral_CellValueBenefitLimits=checknull($getResRow['PS_DentalGeneral_CellValueBenefitLimits']);
				$PS_DentalGeneral_CellValueMaxBenefits=checknull($getResRow['PS_DentalGeneral_CellValueMaxBenefits']);
				$PS_DentalGeneral_BenefitLimits=checknull($getResRow['PS_DentalGeneral_BenefitLimits']);
				$PS_DentalGeneral_BenefitLimits_Value=checknull($getResRow['PS_DentalGeneral_BenefitLimits_Value']);
				$PS_DentalGeneral_BenefitLimits_Description=checknull($getResRow['PS_DentalGeneral_BenefitLimits_Description']);
				$PS_DentalGeneral_BenefitLimits_Liefetime=checknull($getResRow['PS_DentalGeneral_BenefitLimits_Liefetime']);
				$PS_DentalGeneral_BenefitLimits_Liefetime_Value=checknull($getResRow['PS_DentalGeneral_BenefitLimits_Liefetime_Value']);
				$PS_DentalGeneral_BenefitLimits_Liefetime_Description=checknull($getResRow['PS_DentalGeneral_BenefitLimits_Liefetime_Description']);
				$PS_DentalGeneral_MaxBenefits_PeriodicOralExamination=checknull($getResRow['PS_DentalGeneral_MaxBenefits_PeriodicOralExamination']);
				$PS_DentalGeneral_MaxBenefits_ScaleAndClean=checknull($getResRow['PS_DentalGeneral_MaxBenefits_ScaleAndClean']);
				$PS_DentalGeneral_MaxBenefits_FluorideTreatment=checknull($getResRow['PS_DentalGeneral_MaxBenefits_FluorideTreatment']);
				$PS_DentalGeneral_MaxBenefits_SurgicalToothExtraction=checknull($getResRow['PS_DentalGeneral_MaxBenefits_SurgicalToothExtraction']);
				$PS_DentalGeneral_MaxBenefits_PeriodicOralExamination_Value=checknull($getResRow['PS_DentalGeneral_MaxBenefits_PeriodicOralExamination_Value']);
				$PS_DentalGeneral_MaxBenefits_ScaleAndClean_Value=checknull($getResRow['PS_DentalGeneral_MaxBenefits_ScaleAndClean_Value']);
				$PS_DentalGeneral_MaxBenefits_FluorideTreatment_Value=checknull($getResRow['PS_DentalGeneral_MaxBenefits_FluorideTreatment_Value']);
				$PS_DentalGeneral_MaxBenefits_SurgicalToothExtraction_Value=checknull($getResRow['PS_DentalGeneral_MaxBenefits_SurgicalToothExtraction_Value']);
				$PS_DentalMajor=checknull($getResRow['PS_DentalMajor']);
				$PS_DentalMajor_Covered=checknull($getResRow['PS_DentalMajor_Covered']);
				$PS_DentalMajor_WaitingPeriod=checknull($getResRow['PS_DentalMajor_WaitingPeriod']);
				$PS_DentalMajor_WaitingPeriod_Days=checknull($getResRow['PS_DentalMajor_WaitingPeriod_Days']);
				$PS_DentalMajor_CellValueBenefitLimits=checknull($getResRow['PS_DentalMajor_CellValueBenefitLimits']);
				$PS_DentalMajor_CellValueMaxBenefits=checknull($getResRow['PS_DentalMajor_CellValueMaxBenefits']);
				$PS_DentalMajor_BenefitLimits=checknull($getResRow['PS_DentalMajor_BenefitLimits']);
				$PS_DentalMajor_BenefitLimits_Value=checknull($getResRow['PS_DentalMajor_BenefitLimits_Value']);
				$PS_DentalMajor_BenefitLimits_Description=checknull($getResRow['PS_DentalMajor_BenefitLimits_Description']);
				$PS_DentalMajor_BenefitLimits_Liefetime=checknull($getResRow['PS_DentalMajor_BenefitLimits_Liefetime']);
				$PS_DentalMajor_BenefitLimits_Liefetime_Value=checknull($getResRow['PS_DentalMajor_BenefitLimits_Liefetime_Value']);
				$PS_DentalMajor_BenefitLimits_Liefetime_Description=checknull($getResRow['PS_DentalMajor_BenefitLimits_Liefetime_Description']);
				$PS_DentalMajor_MaxBenefits_SurgicalToothExtraction=checknull($getResRow['PS_DentalMajor_MaxBenefits_SurgicalToothExtraction']);
				$PS_DentalMajor_MaxBenefits_FullCrownVeneered=checknull($getResRow['PS_DentalMajor_MaxBenefits_FullCrownVeneered']);
				$PS_DentalMajor_MaxBenefits_SurgicalToothExtraction_Value=checknull($getResRow['PS_DentalMajor_MaxBenefits_SurgicalToothExtraction_Value']);
				$PS_DentalMajor_MaxBenefits_FullCrownVeneered_Value=checknull($getResRow['PS_DentalMajor_MaxBenefits_FullCrownVeneered_Value']);
				$PS_DentalEndodontic=checknull($getResRow['PS_DentalEndodontic']);
				$PS_DentalEndodontic_Covered=checknull($getResRow['PS_DentalEndodontic_Covered']);
				$PS_DentalEndodontic_WaitingPeriod=checknull($getResRow['PS_DentalEndodontic_WaitingPeriod']);
				$PS_DentalEndodontic_WaitingPeriod_Days=checknull($getResRow['PS_DentalEndodontic_WaitingPeriod_Days']);
				$PS_DentalEndodontic_CellValueBenefitLimits=checknull($getResRow['PS_DentalEndodontic_CellValueBenefitLimits']);
				$PS_DentalEndodontic_CellValueMaxBenefits=checknull($getResRow['PS_DentalEndodontic_CellValueMaxBenefits']);
				$PS_DentalEndodontic_BenefitLimits=checknull($getResRow['PS_DentalEndodontic_BenefitLimits']);
				$PS_DentalEndodontic_BenefitLimits_Value=checknull($getResRow['PS_DentalEndodontic_BenefitLimits_Value']);
				$PS_DentalEndodontic_BenefitLimits_Description=checknull($getResRow['PS_DentalEndodontic_BenefitLimits_Description']);
				$PS_DentalEndodontic_BenefitLimits_Liefetime=checknull($getResRow['PS_DentalEndodontic_BenefitLimits_Liefetime']);
				$PS_DentalEndodontic_BenefitLimits_Liefetime_Value=checknull($getResRow['PS_DentalEndodontic_BenefitLimits_Liefetime_Value']);
				$PS_DentalEndodontic_BenefitLimits_Liefetime_Description=checknull($getResRow['PS_DentalEndodontic_BenefitLimits_Liefetime_Description']);
				$PS_DentalEndodontic_MaxBenefits_FillingOfOneRootCanal=checknull($getResRow['PS_DentalEndodontic_MaxBenefits_FillingOfOneRootCanal']);
				$PS_DentalEndodontic_MaxBenefits_FillingOfOneRootCanal_Value=checknull($getResRow['PS_DentalEndodontic_MaxBenefits_FillingOfOneRootCanal_Value']);
				$PS_DOrthodontic=checknull($getResRow['PS_DOrthodontic']);
				$PS_DOrthodontic_Covered=checknull($getResRow['PS_DOrthodontic_Covered']);
				$PS_DOrthodontic_WaitingPeriod=checknull($getResRow['PS_DOrthodontic_WaitingPeriod']);
				$PS_DOrthodontic_WaitingPeriod_Days=checknull($getResRow['PS_DOrthodontic_WaitingPeriod_Days']);
				$PS_DOrthodontic_CellValueBenefitLimits=checknull($getResRow['PS_DOrthodontic_CellValueBenefitLimits']);
				$PS_DOrthodontic_CellValueMaxBenefits=checknull($getResRow['PS_DOrthodontic_CellValueMaxBenefits']);
				$PS_DOrthodontic_BenefitLimits=checknull($getResRow['PS_DOrthodontic_BenefitLimits']);
				$PS_DOrthodontic_BenefitLimits_Value=checknull($getResRow['PS_DOrthodontic_BenefitLimits_Value']);
				$PS_DOrthodontic_BenefitLimits_Description=checknull($getResRow['PS_DOrthodontic_BenefitLimits_Description']);
				$PS_DOrthodontic_BenefitLimits_Liefetime=checknull($getResRow['PS_DOrthodontic_BenefitLimits_Liefetime']);
				$PS_DOrthodontic_BenefitLimits_Liefetime_Value=checknull($getResRow['PS_DOrthodontic_BenefitLimits_Liefetime_Value']);
				$PS_DOrthodontic_BenefitLimits_Liefetime_Description=checknull($getResRow['PS_DOrthodontic_BenefitLimits_Liefetime_Description']);
				$PS_DOrthodontic_MaxBenefits_BracesRemovalFittingRetainer=checknull($getResRow['PS_DOrthodontic_MaxBenefits_BracesRemovalFittingRetainer']);
				$PS_DOrthodontic_MaxBenefits_BracesRemovalFittingRetainer_Value=checknull($getResRow['PS_DOrthodontic_MaxBenefits_BracesRemovalFittingRetainer_Value']);
				$PS_Optical=checknull($getResRow['PS_Optical']);
				$PS_Optical_Covered=checknull($getResRow['PS_Optical_Covered']);
				$PS_Optical_WaitingPeriod=checknull($getResRow['PS_Optical_WaitingPeriod']);
				$PS_Optical_WaitingPeriod_Days=checknull($getResRow['PS_Optical_WaitingPeriod_Days']);
				$PS_Optical_CellValueBenefitLimits=checknull($getResRow['PS_Optical_CellValueBenefitLimits']);
				$PS_Optical_CellValueMaxBenefits=checknull($getResRow['PS_Optical_CellValueMaxBenefits']);
				$PS_Optical_BenefitLimits=checknull($getResRow['PS_Optical_BenefitLimits']);
				$PS_Optical_BenefitLimits_Value=checknull($getResRow['PS_Optical_BenefitLimits_Value']);
				$PS_Optical_BenefitLimits_Description=checknull($getResRow['PS_Optical_BenefitLimits_Description']);
				$PS_Optical_BenefitLimits_Liefetime=checknull($getResRow['PS_Optical_BenefitLimits_Liefetime']);
				$PS_Optical_BenefitLimits_Liefetime_Value=checknull($getResRow['PS_Optical_BenefitLimits_Liefetime_Value']);
				$PS_Optical_BenefitLimits_Liefetime_Description=checknull($getResRow['PS_Optical_BenefitLimits_Liefetime_Description']);
				$PS_Optical_MaxBenefits_SingleVisionLensesAndFrames=checknull($getResRow['PS_Optical_MaxBenefits_SingleVisionLensesAndFrames']);
				$PS_Optical_MaxBenefits_MultiFocalLensesAndFrames=checknull($getResRow['PS_Optical_MaxBenefits_MultiFocalLensesAndFrames']);
				$PS_Optical_MaxBenefits_SingleVisionLensesAndFrames_Value=checknull($getResRow['PS_Optical_MaxBenefits_SingleVisionLensesAndFrames_Value']);
				$PS_Optical_MaxBenefits_MultiFocalLensesAndFrames_Value=checknull($getResRow['PS_Optical_MaxBenefits_MultiFocalLensesAndFrames_Value']);
				$PS_NonPbsPharma=checknull($getResRow['PS_NonPbsPharma']);
				$PS_NonPbsPharma_Covered=checknull($getResRow['PS_NonPbsPharma_Covered']);
				$PS_NonPbsPharma_WaitingPeriod=checknull($getResRow['PS_NonPbsPharma_WaitingPeriod']);
				$PS_NonPbsPharma_WaitingPeriod_Days=checknull($getResRow['PS_NonPbsPharma_WaitingPeriod_Days']);
				$PS_NonPbsPharma_CellValueBenefitLimits=checknull($getResRow['PS_NonPbsPharma_CellValueBenefitLimits']);
				$PS_NonPbsPharma_CellValueMaxBenefits=checknull($getResRow['PS_NonPbsPharma_CellValueMaxBenefits']);
				$PS_NonPbsPharma_BenefitLimits=checknull($getResRow['PS_NonPbsPharma_BenefitLimits']);
				$PS_NonPbsPharma_BenefitLimits_Value=checknull($getResRow['PS_NonPbsPharma_BenefitLimits_Value']);
				$PS_NonPbsPharma_BenefitLimits_Description=checknull($getResRow['PS_NonPbsPharma_BenefitLimits_Description']);
				$PS_NonPbsPharma_BenefitLimits_Liefetime=checknull($getResRow['PS_NonPbsPharma_BenefitLimits_Liefetime']);
				$PS_NonPbsPharma_BenefitLimits_Liefetime_Value=checknull($getResRow['PS_NonPbsPharma_BenefitLimits_Liefetime_Value']);
				$PS_NonPbsPharma_BenefitLimits_Liefetime_Description=checknull($getResRow['PS_NonPbsPharma_BenefitLimits_Liefetime_Description']);
				$PS_NonPbsPharma_MaxBenefits_PerEligiblePrescription=checknull($getResRow['PS_NonPbsPharma_MaxBenefits_PerEligiblePrescription']);
				$PS_NonPbsPharma_MaxBenefits_PerEligiblePrescription_Value=checknull($getResRow['PS_NonPbsPharma_MaxBenefits_PerEligiblePrescription_Value']);
				$PS_Physiotherapy=checknull($getResRow['PS_Physiotherapy']);
				$PS_Physiotherapy_Covered=checknull($getResRow['PS_Physiotherapy_Covered']);
				$PS_Physiotherapy_WaitingPeriod=checknull($getResRow['PS_Physiotherapy_WaitingPeriod']);
				$PS_Physiotherapy_WaitingPeriod_Days=checknull($getResRow['PS_Physiotherapy_WaitingPeriod_Days']);
				$PS_Physiotherapy_CellValueBenefitLimits=checknull($getResRow['PS_Physiotherapy_CellValueBenefitLimits']);
				$PS_Physiotherapy_CellValueMaxBenefits=checknull($getResRow['PS_Physiotherapy_CellValueMaxBenefits']);
				$PS_Physiotherapy_BenefitLimits=checknull($getResRow['PS_Physiotherapy_BenefitLimits']);
				$PS_Physiotherapy_BenefitLimits_Value=checknull($getResRow['PS_Physiotherapy_BenefitLimits_Value']);
				$PS_Physiotherapy_BenefitLimits_Description=checknull($getResRow['PS_Physiotherapy_BenefitLimits_Description']);
				$PS_Physiotherapy_BenefitLimits_Liefetime=checknull($getResRow['PS_Physiotherapy_BenefitLimits_Liefetime']);
				$PS_Physiotherapy_BenefitLimits_Liefetime_Value=checknull($getResRow['PS_Physiotherapy_BenefitLimits_Liefetime_Value']);
				$PS_Physiotherapy_BenefitLimits_Liefetime_Description=checknull($getResRow['PS_Physiotherapy_BenefitLimits_Liefetime_Description']);
				$PS_Physiotherapy_MaxBenefits_InitiaVisit=checknull($getResRow['PS_Physiotherapy_MaxBenefits_InitiaVisit']);
				$PS_Physiotherapy_MaxBenefits_SubsequentVisit=checknull($getResRow['PS_Physiotherapy_MaxBenefits_SubsequentVisit']);
				$PS_Physiotherapy_MaxBenefits_InitiaVisit_Value=checknull($getResRow['PS_Physiotherapy_MaxBenefits_InitiaVisit_Value']);
				$PS_Physiotherapy_MaxBenefits_SubsequentVisit_Value=checknull($getResRow['PS_Physiotherapy_MaxBenefits_SubsequentVisit_Value']);
				$PS_Chiropractic=checknull($getResRow['PS_Chiropractic']);
				$PS_Chiropractic_Covered=checknull($getResRow['PS_Chiropractic_Covered']);
				$PS_Chiropractic_WaitingPeriod=checknull($getResRow['PS_Chiropractic_WaitingPeriod']);
				$PS_Chiropractic_WaitingPeriod_Days=checknull($getResRow['PS_Chiropractic_WaitingPeriod_Days']);
				$PS_Chiropractic_CellValueBenefitLimits=checknull($getResRow['PS_Chiropractic_CellValueBenefitLimits']);
				$PS_Chiropractic_CellValueMaxBenefits=checknull($getResRow['PS_Chiropractic_CellValueMaxBenefits']);
				$PS_Chiropractic_BenefitLimits=checknull($getResRow['PS_Chiropractic_BenefitLimits']);
				$PS_Chiropractic_BenefitLimits_Value=checknull($getResRow['PS_Chiropractic_BenefitLimits_Value']);
				$PS_Chiropractic_BenefitLimits_Description=checknull($getResRow['PS_Chiropractic_BenefitLimits_Description']);
				$PS_Chiropractic_BenefitLimits_Liefetime=checknull($getResRow['PS_Chiropractic_BenefitLimits_Liefetime']);
				$PS_Chiropractic_BenefitLimits_Liefetime_Value=checknull($getResRow['PS_Chiropractic_BenefitLimits_Liefetime_Value']);
				$PS_Chiropractic_BenefitLimits_Liefetime_Description=checknull($getResRow['PS_Chiropractic_BenefitLimits_Liefetime_Description']);
				$PS_Chiropractic_MaxBenefits_InitiaVisit=checknull($getResRow['PS_Chiropractic_MaxBenefits_InitiaVisit']);
				$PS_Chiropractic_MaxBenefits_SubsequentVisit=checknull($getResRow['PS_Chiropractic_MaxBenefits_SubsequentVisit']);
				$PS_Chiropractic_MaxBenefits_InitiaVisit_Value=checknull($getResRow['PS_Chiropractic_MaxBenefits_InitiaVisit_Value']);
				$PS_Chiropractic_MaxBenefits_SubsequentVisit_Value=checknull($getResRow['PS_Chiropractic_MaxBenefits_SubsequentVisit_Value']);
				$PS_Podiatry=checknull($getResRow['PS_Podiatry']);
				$PS_Podiatry_Covered=checknull($getResRow['PS_Podiatry_Covered']);
				$PS_Podiatry_WaitingPeriod=checknull($getResRow['PS_Podiatry_WaitingPeriod']);
				$PS_Podiatry_WaitingPeriod_Days=checknull($getResRow['PS_Podiatry_WaitingPeriod_Days']);
				$PS_Podiatry_CellValueBenefitLimits=checknull($getResRow['PS_Podiatry_CellValueBenefitLimits']);
				$PS_Podiatry_CellValueMaxBenefits=checknull($getResRow['PS_Podiatry_CellValueMaxBenefits']);
				$PS_Podiatry_BenefitLimits=checknull($getResRow['PS_Podiatry_BenefitLimits']);
				$PS_Podiatry_BenefitLimits_Value=checknull($getResRow['PS_Podiatry_BenefitLimits_Value']);
				$PS_Podiatry_BenefitLimits_Description=checknull($getResRow['PS_Podiatry_BenefitLimits_Description']);
				$PS_Podiatry_BenefitLimits_Liefetime=checknull($getResRow['PS_Podiatry_BenefitLimits_Liefetime']);
				$PS_Podiatry_BenefitLimits_Liefetime_Value=checknull($getResRow['PS_Podiatry_BenefitLimits_Liefetime_Value']);
				$PS_Podiatry_BenefitLimits_Liefetime_Description=checknull($getResRow['PS_Podiatry_BenefitLimits_Liefetime_Description']);
				$PS_Podiatry_MaxBenefits_InitiaVisit=checknull($getResRow['PS_Podiatry_MaxBenefits_InitiaVisit']);
				$PS_Podiatry_MaxBenefits_SubsequentVisit=checknull($getResRow['PS_Podiatry_MaxBenefits_SubsequentVisit']);
				$PS_Podiatry_MaxBenefits_InitiaVisit_Value=checknull($getResRow['PS_Podiatry_MaxBenefits_InitiaVisit_Value']);
				$PS_Podiatry_MaxBenefits_SubsequentVisit_Value=checknull($getResRow['PS_Podiatry_MaxBenefits_SubsequentVisit_Value']);
				$PS_Psychology=checknull($getResRow['PS_Psychology']);
				$PS_Psychology_Covered=checknull($getResRow['PS_Psychology_Covered']);
				$PS_Psychology_WaitingPeriod=checknull($getResRow['PS_Psychology_WaitingPeriod']);
				$PS_Psychology_WaitingPeriod_Days=checknull($getResRow['PS_Psychology_WaitingPeriod_Days']);
				$PS_Psychology_CellValueBenefitLimits=checknull($getResRow['PS_Psychology_CellValueBenefitLimits']);
				$PS_Psychology_CellValueMaxBenefits=checknull($getResRow['PS_Psychology_CellValueMaxBenefits']);
				$PS_Psychology_BenefitLimits=checknull($getResRow['PS_Psychology_BenefitLimits']);
				$PS_Psychology_BenefitLimits_Value=checknull($getResRow['PS_Psychology_BenefitLimits_Value']);
				$PS_Psychology_BenefitLimits_Description=checknull($getResRow['PS_Psychology_BenefitLimits_Description']);
				$PS_Psychology_BenefitLimits_Liefetime=checknull($getResRow['PS_Psychology_BenefitLimits_Liefetime']);
				$PS_Psychology_BenefitLimits_Liefetime_Value=checknull($getResRow['PS_Psychology_BenefitLimits_Liefetime_Value']);
				$PS_Psychology_BenefitLimits_Liefetime_Description=checknull($getResRow['PS_Psychology_BenefitLimits_Liefetime_Description']);
				$PS_Psychology_MaxBenefits_InitiaVisit=checknull($getResRow['PS_Psychology_MaxBenefits_InitiaVisit']);
				$PS_Psychology_MaxBenefits_SubsequentVisit=checknull($getResRow['PS_Psychology_MaxBenefits_SubsequentVisit']);
				$PS_Psychology_MaxBenefits_InitiaVisit_Value=checknull($getResRow['PS_Psychology_MaxBenefits_InitiaVisit_Value']);
				$PS_Psychology_MaxBenefits_SubsequentVisit_Value=checknull($getResRow['PS_Psychology_MaxBenefits_SubsequentVisit_Value']);
				$PS_Acupuncture=checknull($getResRow['PS_Acupuncture']);
				$PS_Acupuncture_Covered=checknull($getResRow['PS_Acupuncture_Covered']);
				$PS_Acupuncture_WaitingPeriod=checknull($getResRow['PS_Acupuncture_WaitingPeriod']);
				$PS_Acupuncture_WaitingPeriod_Days=checknull($getResRow['PS_Acupuncture_WaitingPeriod_Days']);
				$PS_Acupuncture_CellValueBenefitLimits=checknull($getResRow['PS_Acupuncture_CellValueBenefitLimits']);
				$PS_Acupuncture_CellValueMaxBenefits=checknull($getResRow['PS_Acupuncture_CellValueMaxBenefits']);
				$PS_Acupuncture_BenefitLimits=checknull($getResRow['PS_Acupuncture_BenefitLimits']);
				$PS_Acupuncture_BenefitLimits_Value=checknull($getResRow['PS_Acupuncture_BenefitLimits_Value']);
				$PS_Acupuncture_BenefitLimits_Description=checknull($getResRow['PS_Acupuncture_BenefitLimits_Description']);
				$PS_Acupuncture_BenefitLimits_Liefetime=checknull($getResRow['PS_Acupuncture_BenefitLimits_Liefetime']);
				$PS_Acupuncture_BenefitLimits_Liefetime_Value=checknull($getResRow['PS_Acupuncture_BenefitLimits_Liefetime_Value']);
				$PS_Acupuncture_BenefitLimits_Liefetime_Description=checknull($getResRow['PS_Acupuncture_BenefitLimits_Liefetime_Description']);
				$PS_Acupuncture_MaxBenefits_InitiaVisit=checknull($getResRow['PS_Acupuncture_MaxBenefits_InitiaVisit']);
				$PS_Acupuncture_MaxBenefits_SubsequentVisit=checknull($getResRow['PS_Acupuncture_MaxBenefits_SubsequentVisit']);
				$PS_Acupuncture_MaxBenefits_InitiaVisit_Value=checknull($getResRow['PS_Acupuncture_MaxBenefits_InitiaVisit_Value']);
				$PS_Acupuncture_MaxBenefits_SubsequentVisit_Value=checknull($getResRow['PS_Acupuncture_MaxBenefits_SubsequentVisit_Value']);
				$PS_Naturopathy=checknull($getResRow['PS_Naturopathy']);
				$PS_Naturopathy_Covered=checknull($getResRow['PS_Naturopathy_Covered']);
				$PS_Naturopathy_WaitingPeriod=checknull($getResRow['PS_Naturopathy_WaitingPeriod']);
				$PS_Naturopathy_WaitingPeriod_Days=checknull($getResRow['PS_Naturopathy_WaitingPeriod_Days']);
				$PS_Naturopathy_CellValueBenefitLimits=checknull($getResRow['PS_Naturopathy_CellValueBenefitLimits']);
				$PS_Naturopathy_CellValueMaxBenefits=checknull($getResRow['PS_Naturopathy_CellValueMaxBenefits']);
				$PS_Naturopathy_BenefitLimits=checknull($getResRow['PS_Naturopathy_BenefitLimits']);
				$PS_Naturopathy_BenefitLimits_Value=checknull($getResRow['PS_Naturopathy_BenefitLimits_Value']);
				$PS_Naturopathy_BenefitLimits_Description=checknull($getResRow['PS_Naturopathy_BenefitLimits_Description']);
				$PS_Naturopathy_BenefitLimits_Liefetime=checknull($getResRow['PS_Naturopathy_BenefitLimits_Liefetime']);
				$PS_Naturopathy_BenefitLimits_Liefetime_Value=checknull($getResRow['PS_Naturopathy_BenefitLimits_Liefetime_Value']);
				$PS_Naturopathy_BenefitLimits_Liefetime_Description=checknull($getResRow['PS_Naturopathy_BenefitLimits_Liefetime_Description']);
				$PS_Naturopathy_MaxBenefits_InitiaVisit=checknull($getResRow['PS_Naturopathy_MaxBenefits_InitiaVisit']);
				$PS_Naturopathy_MaxBenefits_SubsequentVisit=checknull($getResRow['PS_Naturopathy_MaxBenefits_SubsequentVisit']);
				$PS_Naturopathy_MaxBenefits_InitiaVisit_Value=checknull($getResRow['PS_Naturopathy_MaxBenefits_InitiaVisit_Value']);
				$PS_Naturopathy_MaxBenefits_SubsequentVisit_Value=checknull($getResRow['PS_Naturopathy_MaxBenefits_SubsequentVisit_Value']);
				$PS_RemedialMassage=checknull($getResRow['PS_RemedialMassage']);
				$PS_RemedialMassage_Covered=checknull($getResRow['PS_RemedialMassage_Covered']);
				$PS_RemedialMassage_WaitingPeriod=checknull($getResRow['PS_RemedialMassage_WaitingPeriod']);
				$PS_RemedialMassage_WaitingPeriod_Days=checknull($getResRow['PS_RemedialMassage_WaitingPeriod_Days']);
				$PS_RemedialMassage_CellValueBenefitLimits=checknull($getResRow['PS_RemedialMassage_CellValueBenefitLimits']);
				$PS_RemedialMassage_CellValueMaxBenefits=checknull($getResRow['PS_RemedialMassage_CellValueMaxBenefits']);
				$PS_RemedialMassage_BenefitLimits=checknull($getResRow['PS_RemedialMassage_BenefitLimits']);
				$PS_RemedialMassage_BenefitLimits_Value=checknull($getResRow['PS_RemedialMassage_BenefitLimits_Value']);
				$PS_RemedialMassage_BenefitLimits_Description=checknull($getResRow['PS_RemedialMassage_BenefitLimits_Description']);
				$PS_RemedialMassage_BenefitLimits_Liefetime=checknull($getResRow['PS_RemedialMassage_BenefitLimits_Liefetime']);
				$PS_RemedialMassage_BenefitLimits_Liefetime_Value=checknull($getResRow['PS_RemedialMassage_BenefitLimits_Liefetime_Value']);
				$PS_RemedialMassage_BenefitLimits_Liefetime_Description=checknull($getResRow['PS_RemedialMassage_BenefitLimits_Liefetime_Description']);
				$PS_RemedialMassage_MaxBenefits_InitiaVisit=checknull($getResRow['PS_RemedialMassage_MaxBenefits_InitiaVisit']);
				$PS_RemedialMassage_MaxBenefits_SubsequentVisit=checknull($getResRow['PS_RemedialMassage_MaxBenefits_SubsequentVisit']);
				$PS_RemedialMassage_MaxBenefits_InitiaVisit_Value=checknull($getResRow['PS_RemedialMassage_MaxBenefits_InitiaVisit_Value']);
				$PS_RemedialMassage_MaxBenefits_SubsequentVisit_Value=checknull($getResRow['PS_RemedialMassage_MaxBenefits_SubsequentVisit_Value']);
				$PS_HearingAids=checknull($getResRow['PS_HearingAids']);
				$PS_HearingAids_Covered=checknull($getResRow['PS_HearingAids_Covered']);
				$PS_HearingAids_WaitingPeriod=checknull($getResRow['PS_HearingAids_WaitingPeriod']);
				$PS_HearingAids_WaitingPeriod_Days=checknull($getResRow['PS_HearingAids_WaitingPeriod_Days']);
				$PS_HearingAids_CellValueBenefitLimits=checknull($getResRow['PS_HearingAids_CellValueBenefitLimits']);
				$PS_HearingAids_CellValueMaxBenefits=checknull($getResRow['PS_HearingAids_CellValueMaxBenefits']);
				$PS_HearingAids_BenefitLimits=checknull($getResRow['PS_HearingAids_BenefitLimits']);
				$PS_HearingAids_BenefitLimits_Value=checknull($getResRow['PS_HearingAids_BenefitLimits_Value']);
				$PS_HearingAids_BenefitLimits_Description=checknull($getResRow['PS_HearingAids_BenefitLimits_Description']);
				$PS_HearingAids_BenefitLimits_Liefetime=checknull($getResRow['PS_HearingAids_BenefitLimits_Liefetime']);
				$PS_HearingAids_BenefitLimits_Liefetime_Value=checknull($getResRow['PS_HearingAids_BenefitLimits_Liefetime_Value']);
				$PS_HearingAids_BenefitLimits_Liefetime_Description=checknull($getResRow['PS_HearingAids_BenefitLimits_Liefetime_Description']);
				$PS_HearingAids_MaxBenefits_PerHearingAid=checknull($getResRow['PS_HearingAids_MaxBenefits_PerHearingAid']);
				$PS_HearingAids_MaxBenefits_PerHearingAid_Value=checknull($getResRow['PS_HearingAids_MaxBenefits_PerHearingAid_Value']);
				$PS_BloodGlucoseMonitors=checknull($getResRow['PS_BloodGlucoseMonitors']);
				$PS_BloodGlucoseMonitors_Covered=checknull($getResRow['PS_BloodGlucoseMonitors_Covered']);
				$PS_BloodGlucoseMonitors_WaitingPeriod=checknull($getResRow['PS_BloodGlucoseMonitors_WaitingPeriod']);
				$PS_BloodGlucoseMonitors_WaitingPeriod_Days=checknull($getResRow['PS_BloodGlucoseMonitors_WaitingPeriod_Days']);
				$PS_BloodGlucoseMonitors_CellValueBenefitLimits=checknull($getResRow['PS_BloodGlucoseMonitors_CellValueBenefitLimits']);
				$PS_BloodGlucoseMonitors_CellValueMaxBenefits=checknull($getResRow['PS_BloodGlucoseMonitors_CellValueMaxBenefits']);
				$PS_BloodGlucoseMonitors_BenefitLimits=checknull($getResRow['PS_BloodGlucoseMonitors_BenefitLimits']);
				$PS_BloodGlucoseMonitors_BenefitLimits_Value=checknull($getResRow['PS_BloodGlucoseMonitors_BenefitLimits_Value']);
				$PS_BloodGlucoseMonitors_BenefitLimits_Description=checknull($getResRow['PS_BloodGlucoseMonitors_BenefitLimits_Description']);
				$PS_BloodGlucoseMonitors_BenefitLimits_Liefetime=checknull($getResRow['PS_BloodGlucoseMonitors_BenefitLimits_Liefetime']);
				$PS_BloodGlucoseMonitors_BenefitLimits_Liefetime_Value=checknull($getResRow['PS_BloodGlucoseMonitors_BenefitLimits_Liefetime_Value']);
				$PS_BloodGlucoseMonitors_BenefitLimits_Liefetime_Description=checknull($getResRow['PS_BloodGlucoseMonitors_BenefitLimits_Liefetime_Description']);
				$PS_BloodGlucoseMonitors_MaxBenefits_PerMonitor=checknull($getResRow['PS_BloodGlucoseMonitors_MaxBenefits_PerMonitor']);
				$PS_BloodGlucoseMonitors_MaxBenefits_PerMonitor_Value=checknull($getResRow['PS_BloodGlucoseMonitors_MaxBenefits_PerMonitor_Value']);
				$PS_Ambulance=checknull($getResRow['PS_Ambulance']);
				$PS_Ambulance_Covered=checknull($getResRow['PS_Ambulance_Covered']);
				$PS_Ambulance_WaitingPeriod=checknull($getResRow['PS_Ambulance_WaitingPeriod']);
				$PS_Ambulance_WaitingPeriod_Days=checknull($getResRow['PS_Ambulance_WaitingPeriod_Days']);
				$PS_Ambulance_CellValueBenefitLimits=checknull($getResRow['PS_Ambulance_CellValueBenefitLimits']);
				$PS_Ambulance_CellValueMaxBenefits=checknull($getResRow['PS_Ambulance_CellValueMaxBenefits']);
				$PS_Ambulance_BenefitLimits=checknull($getResRow['PS_Ambulance_BenefitLimits']);
				$PS_Ambulance_BenefitLimits_Value=checknull($getResRow['PS_Ambulance_BenefitLimits_Value']);
				$PS_Ambulance_BenefitLimits_Description=checknull($getResRow['PS_Ambulance_BenefitLimits_Description']);
				$PS_Ambulance_BenefitLimits_Liefetime=checknull($getResRow['PS_Ambulance_BenefitLimits_Liefetime']);
				$PS_Ambulance_BenefitLimits_Liefetime_Value=checknull($getResRow['PS_Ambulance_BenefitLimits_Liefetime_Value']);
				$PS_Ambulance_BenefitLimits_Liefetime_Description=checknull($getResRow['PS_Ambulance_BenefitLimits_Liefetime_Description']);
				$PS_PreferredServiceProviderArrangements=checknull($getResRow['PS_PreferredServiceProviderArrangements']);
				$PS_OtherFeatures=checknull($getResRow['PS_OtherFeatures']);
				$PS_DateIssued=checknull($getResRow['PS_DateIssued']);
				$WS_Hos_Excess_Filter_Value=checknull($getResRow['WS_Hos_Excess_Filter_Value']);
				$WS_Hos_CoPayment_Filter_Value=checknull($getResRow['WS_Hos_CoPayment_Filter_Value']);


                //****************** Export Excel Set The Value Start Here *******************************//

				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$si, $insurance_id)
				->setCellValue('B'.$si, $WS_BatchId)
				->setCellValue('C'.$si, $WS_PolicyCode)
				->setCellValue('D'.$si, $WS_PolicyUrl)
				->setCellValue('E'.$si, $WS_PolicyName)
				->setCellValue('F'.$si, $WS_ProductName)
				->setCellValue('G'.$si, $WS_Status)
				->setCellValue('H'.$si, $WS_Corporate)
				->setCellValue('I'.$si, $WS_OverseasVisitors)
				->setCellValue('J'.$si, $WS_Enabled)
				->setCellValue('K'.$si, $WS_Visible)
				->setCellValue('L'.$si, $WS_FundId)
				->setCellValue('M'.$si, $WS_Fund)
				->setCellValue('N'.$si, $WS_FundName)
				->setCellValue('O'.$si, $WS_StateId)
				->setCellValue('P'.$si, $WS_State)
				->setCellValue('Q'.$si, $WS_StateName)
				->setCellValue('R'.$si, $WS_CoverTypeId)
				->setCellValue('S'.$si, $WS_CoverType)
				->setCellValue('T'.$si, $WS_CoverLevelHospitalId)
				->setCellValue('U'.$si, $WS_CoverLevelHospital)
				->setCellValue('V'.$si, $WS_CoverLevelGeneralId)
				->setCellValue('W'.$si, $WS_CoverLevelGeneral)
				->setCellValue('X'.$si, $WS_CategoryId)
				->setCellValue('Y'.$si, $WS_Category)
				->setCellValue('Z'.$si, $WS_CategoryOriginalName)
				->setCellValue('AA'.$si, $WS_Hos_Excess_PerHospitalVisit)
				->setCellValue('AB'.$si, $WS_Hos_Excess_MaximumPerPerson)
				->setCellValue('AC'.$si, $WS_Hos_Excess_MaximumPerAnnum)
				->setCellValue('AD'.$si, $WS_Hos_CoPayments)
				->setCellValue('AE'.$si, $WS_Hos_CoPayment_SharedRoom)
				->setCellValue('AF'.$si, $WS_Hos_CoPayment_PrivateRoom)
				->setCellValue('AG'.$si, $WS_Hos_CoPayment_DaySurgery)
				->setCellValue('AH'.$si, $WS_Hos_OtherFeatures)
				->setCellValue('AI'.$si, $WS_Hos_Ambulance)
				->setCellValue('AJ'.$si, $WS_Hos_AmbulanceCover)
				->setCellValue('AK'.$si, $WS_Gen_AmbulanceCover)
				->setCellValue('AL'.$si, $WS_YearlyPremium)
				->setCellValue('AM'.$si, $WS_YearlyPremium_StdLessThanAge65Rebate)
				->setCellValue('AN'.$si, $WS_YearlyPremium_StdAge65ToAge69Rebate)
				->setCellValue('AO'.$si, $WS_YearlyPremium_StdAge70AndGreaterRebate)
				->setCellValue('AP'.$si, $WS_YearlyPremium_T1LessThanAge65Rebate)
				->setCellValue('AQ'.$si, $WS_YearlyPremium_T1Age65ToAge69Rebate)
				->setCellValue('AR'.$si, $WS_YearlyPremium_T1Age70AndGreaterRebate)
				->setCellValue('AS'.$si, $WS_YearlyPremium_T2LessThanAge65Rebate)
				->setCellValue('AT'.$si, $WS_YearlyPremium_T2Age65ToAge69Rebate)
				->setCellValue('AU'.$si, $WS_YearlyPremium_T2Age70AndGreaterRebate)
				->setCellValue('AV'.$si, $WS_HalfYearlyPremium)
				->setCellValue('AW'.$si, $WS_HalfYearlyPremium_StdLessThanAge65Rebate)
				->setCellValue('AX'.$si, $WS_HalfYearlyPremium_StdAge65ToAge69Rebate)
				->setCellValue('AY'.$si, $WS_HalfYearlyPremium_StdAge70AndGreaterRebate)
				->setCellValue('AZ'.$si, $WS_HalfYearlyPremium_T1LessThanAge65Rebate)
				->setCellValue('BA'.$si, $WS_HalfYearlyPremium_T1Age65ToAge69Rebate)
				->setCellValue('BB'.$si, $WS_HalfYearlyPremium_T1Age70AndGreaterRebate)
				->setCellValue('BC'.$si, $WS_HalfYearlyPremium_T2LessThanAge65Rebate)
				->setCellValue('BD'.$si, $WS_HalfYearlyPremium_T2Age65ToAge69Rebate)
				->setCellValue('BE'.$si, $WS_HalfYearlyPremium_T2Age70AndGreaterRebate)
				->setCellValue('BF'.$si, $WS_QuarterlyPremium)
				->setCellValue('BG'.$si, $WS_QuarterlyPremium_StdLessThanAge65Rebate)
				->setCellValue('BH'.$si, $WS_QuarterlyPremium_StdAge65ToAge69Rebate)
				->setCellValue('BI'.$si, $WS_QuarterlyPremium_StdAge70AndGreaterRebate)
				->setCellValue('BJ'.$si, $WS_QuarterlyPremium_T1LessThanAge65Rebate)
				->setCellValue('BK'.$si, $WS_QuarterlyPremium_T1Age65ToAge69Rebate)
				->setCellValue('BL'.$si, $WS_QuarterlyPremium_T1Age70AndGreaterRebate)
				->setCellValue('BM'.$si, $WS_QuarterlyPremium_T2LessThanAge65Rebate)
				->setCellValue('BN'.$si, $WS_QuarterlyPremium_T2Age65ToAge69Rebate)
				->setCellValue('BO'.$si, $WS_QuarterlyPremium_T2Age70AndGreaterRebate)
				->setCellValue('BP'.$si, $WS_MonthlyPremium)
				->setCellValue('BQ'.$si, $WS_MonthlyPremium_StdLessThanAge65Rebate)
				->setCellValue('BR'.$si, $WS_MonthlyPremium_StdAge65ToAge69Rebate)
				->setCellValue('BS'.$si, $WS_MonthlyPremium_StdAge70AndGreaterRebate)
				->setCellValue('BT'.$si, $WS_MonthlyPremium_T1LessThanAge65Rebate)
				->setCellValue('BU'.$si, $WS_MonthlyPremium_T1Age65ToAge69Rebate)
				->setCellValue('BV'.$si, $WS_MonthlyPremium_T1Age70AndGreaterRebate)
				->setCellValue('BW'.$si, $WS_MonthlyPremium_T2LessThanAge65Rebate)
				->setCellValue('BX'.$si, $WS_MonthlyPremium_T2Age65ToAge69Rebate)
				->setCellValue('BY'.$si, $WS_MonthlyPremium_T2Age70AndGreaterRebate)
				->setCellValue('BZ'.$si, $WS_FortnightlyPremium)
				->setCellValue('CA'.$si, $WS_FortnightlyPremium_StdLessThanAge65Rebate)
				->setCellValue('CB'.$si, $WS_FortnightlyPremium_StdAge65ToAge69Rebate)
				->setCellValue('CC'.$si, $WS_FortnightlyPremium_StdAge70AndGreaterRebate)
				->setCellValue('CD'.$si, $WS_FortnightlyPremium_T1LessThanAge65Rebate)
				->setCellValue('CE'.$si, $WS_FortnightlyPremium_T1Age65ToAge69Rebate)
				->setCellValue('CF'.$si, $WS_FortnightlyPremium_T1Age70AndGreaterRebate)
				->setCellValue('CG'.$si, $WS_FortnightlyPremium_T2LessThanAge65Rebate)
				->setCellValue('CH'.$si, $WS_FortnightlyPremium_T2Age65ToAge69Rebate)
				->setCellValue('CI'.$si, $WS_FortnightlyPremium_T2Age70AndGreaterRebate)
				->setCellValue('CJ'.$si, $WS_WeeklyPremium)
				->setCellValue('CK'.$si, $WS_WeeklyPremium_StdLessThanAge65Rebate)
				->setCellValue('CL'.$si, $WS_WeeklyPremium_StdAge65ToAge69Rebate)
				->setCellValue('CM'.$si, $WS_WeeklyPremium_StdAge70AndGreaterRebate)
				->setCellValue('CN'.$si, $WS_WeeklyPremium_T1LessThanAge65Rebate)
				->setCellValue('CO'.$si, $WS_WeeklyPremium_T1Age65ToAge69Rebate)
				->setCellValue('CP'.$si, $WS_WeeklyPremium_T1Age70AndGreaterRebate)
				->setCellValue('CQ'.$si, $WS_WeeklyPremium_T2LessThanAge65Rebate)
				->setCellValue('CR'.$si, $WS_WeeklyPremium_T2Age65ToAge69Rebate)
				->setCellValue('CS'.$si, $WS_WeeklyPremium_T2Age70AndGreaterRebate)
				->setCellValue('CT'.$si, $WS_Hos_Accommodation_Covered)
				->setCellValue('CU'.$si, $WS_Hos_Accommodation_Covered_Value)
				->setCellValue('CV'.$si, $WS_Hos_CardiacAndRelatedServices_Covered)
				->setCellValue('CW'.$si, $WS_Hos_CardiacAndRelatedServices_Covered_Value)
				->setCellValue('CX'.$si, $WS_Hos_CataractAndEyeLensProcedures_Covered)
				->setCellValue('CY'.$si, $WS_Hos_CataractAndEyeLensProcedures_Covered_Value)
				->setCellValue('CZ'.$si, $WS_Hos_PregnancyAndBirthRelatedServices_Covered)
				->setCellValue('DA'.$si, $WS_Hos_PregnancyAndBirthRelatedServices_Covered_Value)
				->setCellValue('DB'.$si, $WS_Hos_AssistedReproductiveServices_Covered)
				->setCellValue('DC'.$si, $WS_Hos_AssistedReproductiveServices_Covered_Value)
				->setCellValue('DD'.$si, $WS_Hos_JointReplacement_HipKneeShoulderAndElbow_Covered)
				->setCellValue('DE'.$si, $WS_Hos_JointReplacement_HipKneeShoulderAndElbow_Covered_Value)
				->setCellValue('DF'.$si, $WS_Hos_JointReplacement_HipKneeOnly_Covered)
				->setCellValue('DG'.$si, $WS_Hos_JointReplacement_HipKneeOnly_Covered_Value)
				->setCellValue('DH'.$si, $WS_Hos_JointReplacement_HipOnly_Covered)
				->setCellValue('DI'.$si, $WS_Hos_JointReplacement_HipOnly_Covered_Value)
				->setCellValue('DJ'.$si, $WS_Hos_DialysisForChronicRenalFailure_Covered)
				->setCellValue('DK'.$si, $WS_Hos_DialysisForChronicRenalFailure_Covered_Value)
				->setCellValue('DL'.$si, $WS_Hos_GastricBandingAndRelatedServices_Covered)
				->setCellValue('DM'.$si, $WS_Hos_GastricBandingAndRelatedServices_Covered_Value)
				->setCellValue('DN'.$si, $WS_Hos_Sterilisation_Covered)
				->setCellValue('DO'.$si, $WS_Hos_Sterilisation_Covered_Value)
				->setCellValue('DP'.$si, $WS_Hos_NonCosmeticPlasticSurgery_Covered)
				->setCellValue('DQ'.$si, $WS_Hos_NonCosmeticPlasticSurgery_Covered_Value)
				->setCellValue('DR'.$si, $WS_Hos_Rehabilitation_Covered)
				->setCellValue('DS'.$si, $WS_Hos_Rehabilitation_Covered_Value)
				->setCellValue('DT'.$si, $WS_Hos_PsychiatricServices_Covered)
				->setCellValue('DU'.$si, $WS_Hos_PsychiatricServices_Covered_Value)
				->setCellValue('DV'.$si, $WS_Hos_PalliativeCare_Covered)
				->setCellValue('DW'.$si, $WS_Hos_PalliativeCare_Covered_Value)
				->setCellValue('DX'.$si, $WS_Gen_Dental_GeneralDental_Benefits_Stars)
				->setCellValue('DY'.$si, $WS_Gen_Dental_GeneralDental_Limits_Stars)
				->setCellValue('DZ'.$si, $WS_Gen_Dental_MajorDental_Benefits_Stars)
				->setCellValue('EA'.$si, $WS_Gen_Dental_MajorDental_Limits_Stars)
				->setCellValue('EB'.$si, $WS_Gen_Dental_Endodontic_Benefits_Stars)
				->setCellValue('EC'.$si, $WS_Gen_Dental_Endodontic_Limits_Stars)
				->setCellValue('ED'.$si, $WS_Gen_Dental_Orthodontic_Benefits_Stars)
				->setCellValue('EE'.$si, $WS_Gen_Dental_Orthodontic_Limits_Stars)
				->setCellValue('EF'.$si, $WS_Gen_Optical_Benefits_Stars)
				->setCellValue('EG'.$si, $WS_Gen_Optical_Limits_Stars)
				->setCellValue('EH'.$si, $WS_Gen_NonPbsPharmaceuticals_Benefits_Stars)
				->setCellValue('EI'.$si, $WS_Gen_NonPbsPharmaceuticals_Limits_Stars)
				->setCellValue('EJ'.$si, $WS_Gen_Physiotherapy_Benefits_Stars)
				->setCellValue('EK'.$si, $WS_Gen_Physiotherapy_Limits_Stars)
				->setCellValue('EL'.$si, $WS_Gen_Chiropractic_Benefits_Stars)
				->setCellValue('EM'.$si, $WS_Gen_Chiropractic_Limits_Stars)
				->setCellValue('EN'.$si, $WS_Gen_Podiatry_Benefits_Stars)
				->setCellValue('EO'.$si, $WS_Gen_Podiatry_Limits_Stars)
				->setCellValue('EP'.$si, $WS_Gen_Psychology_Benefits_Stars)
				->setCellValue('EQ'.$si, $WS_Gen_Psychology_Limits_Stars)
				->setCellValue('ER'.$si, $WS_Gen_Acupuncture_Benefits_Stars)
				->setCellValue('ES'.$si, $WS_Gen_Acupuncture_Limits_Stars)
				->setCellValue('ET'.$si, $WS_Gen_Naturopathy_Benefits_Stars)
				->setCellValue('EU'.$si, $WS_Gen_Naturopathy_Limits_Stars)
				->setCellValue('EV'.$si, $WS_Gen_RemedialMassage_Benefits_Stars)
				->setCellValue('EW'.$si, $WS_Gen_RemedialMassage_Limits_Stars)
				->setCellValue('EX'.$si, $WS_Gen_HearingAids_Benefits_Stars)
				->setCellValue('EY'.$si, $WS_Gen_HearingAids_Limits_Stars)
				->setCellValue('EZ'.$si, $WS_Gen_BloodGlucoseMonitors_Benefits_Stars)
				->setCellValue('FA'.$si, $WS_Gen_BloodGlucoseMonitors_Limits_Stars)
				->setCellValue('FB'.$si, $WS_Gen_PreferredProviders)
				->setCellValue('FC'.$si, $PS_BatchId)
				->setCellValue('FD'.$si, $PS_PolicyCode)
				->setCellValue('FE'.$si, $PS_PolicyFileCode)
				->setCellValue('FF'.$si, $PS_PolicyUrl)
				->setCellValue('FG'.$si, $PS_FundName)
				->setCellValue('FH'.$si, $PS_ProductName)
				->setCellValue('FI'.$si, $PS_AvailableFor)
				->setCellValue('FJ'.$si, $PS_CoverFor)
				->setCellValue('FK'.$si, $PS_MonthlyPremium)
				->setCellValue('FL'.$si, $PS_MonthlyPremium_Value)
				->setCellValue('FM'.$si, $PS_Closed)
				->setCellValue('FN'.$si, $PS_Closed_Value)
				->setCellValue('FO'.$si, $PS_Corporate_Value)
				->setCellValue('FP'.$si, $PS_MedicareLevySurchargeStatus)
				->setCellValue('FQ'.$si, $PS_DentalGeneral)
				->setCellValue('FR'.$si, $PS_DentalGeneral_Covered)
				->setCellValue('FS'.$si, $PS_DentalGeneral_WaitingPeriod)
				->setCellValue('FT'.$si, $PS_DentalGeneral_WaitingPeriod_Days)
				->setCellValue('FU'.$si, $PS_DentalGeneral_CellValueBenefitLimits)
				->setCellValue('FV'.$si, $PS_DentalGeneral_CellValueMaxBenefits)
				->setCellValue('FW'.$si, $PS_DentalGeneral_BenefitLimits)
				->setCellValue('FX'.$si, $PS_DentalGeneral_BenefitLimits_Value)
				->setCellValue('FY'.$si, $PS_DentalGeneral_BenefitLimits_Description)
				->setCellValue('FZ'.$si, $PS_DentalGeneral_BenefitLimits_Liefetime)
				->setCellValue('GA'.$si, $PS_DentalGeneral_BenefitLimits_Liefetime_Value)
				->setCellValue('GB'.$si, $PS_DentalGeneral_BenefitLimits_Liefetime_Description)
				->setCellValue('GC'.$si, $PS_DentalGeneral_MaxBenefits_PeriodicOralExamination)
				->setCellValue('GD'.$si, $PS_DentalGeneral_MaxBenefits_ScaleAndClean)
				->setCellValue('GE'.$si, $PS_DentalGeneral_MaxBenefits_FluorideTreatment)
				->setCellValue('GF'.$si, $PS_DentalGeneral_MaxBenefits_SurgicalToothExtraction)
				->setCellValue('GG'.$si, $PS_DentalGeneral_MaxBenefits_PeriodicOralExamination_Value)
				->setCellValue('GH'.$si, $PS_DentalGeneral_MaxBenefits_ScaleAndClean_Value)
				->setCellValue('GI'.$si, $PS_DentalGeneral_MaxBenefits_FluorideTreatment_Value)
				->setCellValue('GJ'.$si, $PS_DentalGeneral_MaxBenefits_SurgicalToothExtraction_Value)
				->setCellValue('GK'.$si, $PS_DentalMajor)
				->setCellValue('GL'.$si, $PS_DentalMajor_Covered)
				->setCellValue('GM'.$si, $PS_DentalMajor_WaitingPeriod)
				->setCellValue('GN'.$si, $PS_DentalMajor_WaitingPeriod_Days)
				->setCellValue('GO'.$si, $PS_DentalMajor_CellValueBenefitLimits)
				->setCellValue('GP'.$si, $PS_DentalMajor_CellValueMaxBenefits)
				->setCellValue('GQ'.$si, $PS_DentalMajor_BenefitLimits)
				->setCellValue('GR'.$si, $PS_DentalMajor_BenefitLimits_Value)
				->setCellValue('GS'.$si, $PS_DentalMajor_BenefitLimits_Description)
				->setCellValue('GT'.$si, $PS_DentalMajor_BenefitLimits_Liefetime)
				->setCellValue('GU'.$si, $PS_DentalMajor_BenefitLimits_Liefetime_Value)
				->setCellValue('GV'.$si, $PS_DentalMajor_BenefitLimits_Liefetime_Description)
				->setCellValue('GW'.$si, $PS_DentalMajor_MaxBenefits_SurgicalToothExtraction)
				->setCellValue('GX'.$si, $PS_DentalMajor_MaxBenefits_FullCrownVeneered)
				->setCellValue('GY'.$si, $PS_DentalMajor_MaxBenefits_SurgicalToothExtraction_Value)
				->setCellValue('GZ'.$si, $PS_DentalMajor_MaxBenefits_FullCrownVeneered_Value)
				->setCellValue('HA'.$si, $PS_DentalEndodontic)
				->setCellValue('HB'.$si, $PS_DentalEndodontic_Covered)
				->setCellValue('HC'.$si, $PS_DentalEndodontic_WaitingPeriod)
				->setCellValue('HD'.$si, $PS_DentalEndodontic_WaitingPeriod_Days)
				->setCellValue('HE'.$si, $PS_DentalEndodontic_CellValueBenefitLimits)
				->setCellValue('HF'.$si, $PS_DentalEndodontic_CellValueMaxBenefits)
				->setCellValue('HG'.$si, $PS_DentalEndodontic_BenefitLimits)
				->setCellValue('HH'.$si, $PS_DentalEndodontic_BenefitLimits_Value)
				->setCellValue('HI'.$si, $PS_DentalEndodontic_BenefitLimits_Description)
				->setCellValue('HJ'.$si, $PS_DentalEndodontic_BenefitLimits_Liefetime)
				->setCellValue('HK'.$si, $PS_DentalEndodontic_BenefitLimits_Liefetime_Value)
				->setCellValue('HL'.$si, $PS_DentalEndodontic_BenefitLimits_Liefetime_Description)
				->setCellValue('HM'.$si, $PS_DentalEndodontic_MaxBenefits_FillingOfOneRootCanal)
				->setCellValue('HN'.$si, $PS_DentalEndodontic_MaxBenefits_FillingOfOneRootCanal_Value)
				->setCellValue('HO'.$si, $PS_DOrthodontic)
				->setCellValue('HP'.$si, $PS_DOrthodontic_Covered)
				->setCellValue('HQ'.$si, $PS_DOrthodontic_WaitingPeriod)
				->setCellValue('HR'.$si, $PS_DOrthodontic_WaitingPeriod_Days)
				->setCellValue('HS'.$si, $PS_DOrthodontic_CellValueBenefitLimits)
				->setCellValue('HT'.$si, $PS_DOrthodontic_CellValueMaxBenefits)
				->setCellValue('HU'.$si, $PS_DOrthodontic_BenefitLimits)
				->setCellValue('HV'.$si, $PS_DOrthodontic_BenefitLimits_Value)
				->setCellValue('HW'.$si, $PS_DOrthodontic_BenefitLimits_Description)
				->setCellValue('HX'.$si, $PS_DOrthodontic_BenefitLimits_Liefetime)
				->setCellValue('HY'.$si, $PS_DOrthodontic_BenefitLimits_Liefetime_Value)
				->setCellValue('HZ'.$si, $PS_DOrthodontic_BenefitLimits_Liefetime_Description)
				->setCellValue('IA'.$si, $PS_DOrthodontic_MaxBenefits_BracesRemovalFittingRetainer)
				->setCellValue('IB'.$si, $PS_DOrthodontic_MaxBenefits_BracesRemovalFittingRetainer_Value)
				->setCellValue('IC'.$si, $PS_Optical)
				->setCellValue('ID'.$si, $PS_Optical_Covered)
				->setCellValue('IE'.$si, $PS_Optical_WaitingPeriod)
				->setCellValue('IF'.$si, $PS_Optical_WaitingPeriod_Days)
				->setCellValue('IG'.$si, $PS_Optical_CellValueBenefitLimits)
				->setCellValue('IH'.$si, $PS_Optical_CellValueMaxBenefits)
				->setCellValue('II'.$si, $PS_Optical_BenefitLimits)
				->setCellValue('IJ'.$si, $PS_Optical_BenefitLimits_Value)
				->setCellValue('IK'.$si, $PS_Optical_BenefitLimits_Description)
				->setCellValue('IL'.$si, $PS_Optical_BenefitLimits_Liefetime)
				->setCellValue('IM'.$si, $PS_Optical_BenefitLimits_Liefetime_Value)
				->setCellValue('IN'.$si, $PS_Optical_BenefitLimits_Liefetime_Description)
				->setCellValue('IO'.$si, $PS_Optical_MaxBenefits_SingleVisionLensesAndFrames)
				->setCellValue('IP'.$si, $PS_Optical_MaxBenefits_MultiFocalLensesAndFrames)
				->setCellValue('IQ'.$si, $PS_Optical_MaxBenefits_SingleVisionLensesAndFrames_Value)
				->setCellValue('IR'.$si, $PS_Optical_MaxBenefits_MultiFocalLensesAndFrames_Value)
				->setCellValue('IS'.$si, $PS_NonPbsPharma)
				->setCellValue('IT'.$si, $PS_NonPbsPharma_Covered)
				->setCellValue('IU'.$si, $PS_NonPbsPharma_WaitingPeriod)
				->setCellValue('IV'.$si, $PS_NonPbsPharma_WaitingPeriod_Days)
				->setCellValue('IW'.$si, $PS_NonPbsPharma_CellValueBenefitLimits)
				->setCellValue('IX'.$si, $PS_NonPbsPharma_CellValueMaxBenefits)
				->setCellValue('IY'.$si, $PS_NonPbsPharma_BenefitLimits)
				->setCellValue('IZ'.$si, $PS_NonPbsPharma_BenefitLimits_Value)
				->setCellValue('JA'.$si, $PS_NonPbsPharma_BenefitLimits_Description)
				->setCellValue('JB'.$si, $PS_NonPbsPharma_BenefitLimits_Liefetime)
				->setCellValue('JC'.$si, $PS_NonPbsPharma_BenefitLimits_Liefetime_Value)
				->setCellValue('JD'.$si, $PS_NonPbsPharma_BenefitLimits_Liefetime_Description)
				->setCellValue('JE'.$si, $PS_NonPbsPharma_MaxBenefits_PerEligiblePrescription)
				->setCellValue('JF'.$si, $PS_NonPbsPharma_MaxBenefits_PerEligiblePrescription_Value)
				->setCellValue('JG'.$si, $PS_Physiotherapy)
				->setCellValue('JH'.$si, $PS_Physiotherapy_Covered)
				->setCellValue('JI'.$si, $PS_Physiotherapy_WaitingPeriod)
				->setCellValue('JJ'.$si, $PS_Physiotherapy_WaitingPeriod_Days)
				->setCellValue('JK'.$si, $PS_Physiotherapy_CellValueBenefitLimits)
				->setCellValue('JL'.$si, $PS_Physiotherapy_CellValueMaxBenefits)
				->setCellValue('JM'.$si, $PS_Physiotherapy_BenefitLimits)
				->setCellValue('JN'.$si, $PS_Physiotherapy_BenefitLimits_Value)
				->setCellValue('JO'.$si, $PS_Physiotherapy_BenefitLimits_Description)
				->setCellValue('JP'.$si, $PS_Physiotherapy_BenefitLimits_Liefetime)
				->setCellValue('JQ'.$si, $PS_Physiotherapy_BenefitLimits_Liefetime_Value)
				->setCellValue('JR'.$si, $PS_Physiotherapy_BenefitLimits_Liefetime_Description)
				->setCellValue('JS'.$si, $PS_Physiotherapy_MaxBenefits_InitiaVisit)
				->setCellValue('JT'.$si, $PS_Physiotherapy_MaxBenefits_SubsequentVisit)
				->setCellValue('JU'.$si, $PS_Physiotherapy_MaxBenefits_InitiaVisit_Value)
				->setCellValue('JV'.$si, $PS_Physiotherapy_MaxBenefits_SubsequentVisit_Value)
				->setCellValue('JW'.$si, $PS_Chiropractic)
				->setCellValue('JX'.$si, $PS_Chiropractic_Covered)
				->setCellValue('JY'.$si, $PS_Chiropractic_WaitingPeriod)
				->setCellValue('JZ'.$si, $PS_Chiropractic_WaitingPeriod_Days)
				->setCellValue('KA'.$si, $PS_Chiropractic_CellValueBenefitLimits)
				->setCellValue('KB'.$si, $PS_Chiropractic_CellValueMaxBenefits)
				->setCellValue('KC'.$si, $PS_Chiropractic_BenefitLimits)
				->setCellValue('KD'.$si, $PS_Chiropractic_BenefitLimits_Value)
				->setCellValue('KE'.$si, $PS_Chiropractic_BenefitLimits_Description)
				->setCellValue('KF'.$si, $PS_Chiropractic_BenefitLimits_Liefetime)
				->setCellValue('KG'.$si, $PS_Chiropractic_BenefitLimits_Liefetime_Value)
				->setCellValue('KH'.$si, $PS_Chiropractic_BenefitLimits_Liefetime_Description)
				->setCellValue('KI'.$si, $PS_Chiropractic_MaxBenefits_InitiaVisit)
				->setCellValue('KJ'.$si, $PS_Chiropractic_MaxBenefits_SubsequentVisit)
				->setCellValue('KK'.$si, $PS_Chiropractic_MaxBenefits_InitiaVisit_Value)
				->setCellValue('KL'.$si, $PS_Chiropractic_MaxBenefits_SubsequentVisit_Value)
				->setCellValue('KM'.$si, $PS_Podiatry)
				->setCellValue('KN'.$si, $PS_Podiatry_Covered)
				->setCellValue('KO'.$si, $PS_Podiatry_WaitingPeriod)
				->setCellValue('KP'.$si, $PS_Podiatry_WaitingPeriod_Days)
				->setCellValue('KQ'.$si, $PS_Podiatry_CellValueBenefitLimits)
				->setCellValue('KR'.$si, $PS_Podiatry_CellValueMaxBenefits)
				->setCellValue('KS'.$si, $PS_Podiatry_BenefitLimits)
				->setCellValue('KT'.$si, $PS_Podiatry_BenefitLimits_Value)
				->setCellValue('KU'.$si, $PS_Podiatry_BenefitLimits_Description)
				->setCellValue('KV'.$si, $PS_Podiatry_BenefitLimits_Liefetime)
				->setCellValue('KW'.$si, $PS_Podiatry_BenefitLimits_Liefetime_Value)
				->setCellValue('KX'.$si, $PS_Podiatry_BenefitLimits_Liefetime_Description)
				->setCellValue('KY'.$si, $PS_Podiatry_MaxBenefits_InitiaVisit)
				->setCellValue('KZ'.$si, $PS_Podiatry_MaxBenefits_SubsequentVisit)
				->setCellValue('LA'.$si, $PS_Podiatry_MaxBenefits_InitiaVisit_Value)
				->setCellValue('LB'.$si, $PS_Podiatry_MaxBenefits_SubsequentVisit_Value)
				->setCellValue('LC'.$si, $PS_Psychology)
				->setCellValue('LD'.$si, $PS_Psychology_Covered)
				->setCellValue('LE'.$si, $PS_Psychology_WaitingPeriod)
				->setCellValue('LF'.$si, $PS_Psychology_WaitingPeriod_Days)
				->setCellValue('LG'.$si, $PS_Psychology_CellValueBenefitLimits)
				->setCellValue('LH'.$si, $PS_Psychology_CellValueMaxBenefits)
				->setCellValue('LI'.$si, $PS_Psychology_BenefitLimits)
				->setCellValue('LJ'.$si, $PS_Psychology_BenefitLimits_Value)
				->setCellValue('LK'.$si, $PS_Psychology_BenefitLimits_Description)
				->setCellValue('LL'.$si, $PS_Psychology_BenefitLimits_Liefetime)
				->setCellValue('LM'.$si, $PS_Psychology_BenefitLimits_Liefetime_Value)
				->setCellValue('LN'.$si, $PS_Psychology_BenefitLimits_Liefetime_Description)
				->setCellValue('LO'.$si, $PS_Psychology_MaxBenefits_InitiaVisit)
				->setCellValue('LP'.$si, $PS_Psychology_MaxBenefits_SubsequentVisit)
				->setCellValue('LQ'.$si, $PS_Psychology_MaxBenefits_InitiaVisit_Value)
				->setCellValue('LR'.$si, $PS_Psychology_MaxBenefits_SubsequentVisit_Value)
				->setCellValue('LS'.$si, $PS_Acupuncture)
				->setCellValue('LT'.$si, $PS_Acupuncture_Covered)
				->setCellValue('LU'.$si, $PS_Acupuncture_WaitingPeriod)
				->setCellValue('LV'.$si, $PS_Acupuncture_WaitingPeriod_Days)
				->setCellValue('LW'.$si, $PS_Acupuncture_CellValueBenefitLimits)
				->setCellValue('LX'.$si, $PS_Acupuncture_CellValueMaxBenefits)
				->setCellValue('LY'.$si, $PS_Acupuncture_BenefitLimits)
				->setCellValue('LZ'.$si, $PS_Acupuncture_BenefitLimits_Value)
				->setCellValue('MA'.$si, $PS_Acupuncture_BenefitLimits_Description)
				->setCellValue('MB'.$si, $PS_Acupuncture_BenefitLimits_Liefetime)
				->setCellValue('MC'.$si, $PS_Acupuncture_BenefitLimits_Liefetime_Value)
				->setCellValue('MD'.$si, $PS_Acupuncture_BenefitLimits_Liefetime_Description)
				->setCellValue('ME'.$si, $PS_Acupuncture_MaxBenefits_InitiaVisit)
				->setCellValue('MF'.$si, $PS_Acupuncture_MaxBenefits_SubsequentVisit)
				->setCellValue('MG'.$si, $PS_Acupuncture_MaxBenefits_InitiaVisit_Value)
				->setCellValue('MH'.$si, $PS_Acupuncture_MaxBenefits_SubsequentVisit_Value)
				->setCellValue('MI'.$si, $PS_Naturopathy)
				->setCellValue('MJ'.$si, $PS_Naturopathy_Covered)
				->setCellValue('MK'.$si, $PS_Naturopathy_WaitingPeriod)
				->setCellValue('ML'.$si, $PS_Naturopathy_WaitingPeriod_Days)
				->setCellValue('MM'.$si, $PS_Naturopathy_CellValueBenefitLimits)
				->setCellValue('MN'.$si, $PS_Naturopathy_CellValueMaxBenefits)
				->setCellValue('MO'.$si, $PS_Naturopathy_BenefitLimits)
				->setCellValue('MP'.$si, $PS_Naturopathy_BenefitLimits_Value)
				->setCellValue('MQ'.$si, $PS_Naturopathy_BenefitLimits_Description)
				->setCellValue('MR'.$si, $PS_Naturopathy_BenefitLimits_Liefetime)
				->setCellValue('MS'.$si, $PS_Naturopathy_BenefitLimits_Liefetime_Value)
				->setCellValue('MT'.$si, $PS_Naturopathy_BenefitLimits_Liefetime_Description)
				->setCellValue('MU'.$si, $PS_Naturopathy_MaxBenefits_InitiaVisit)
				->setCellValue('MV'.$si, $PS_Naturopathy_MaxBenefits_SubsequentVisit)
				->setCellValue('MW'.$si, $PS_Naturopathy_MaxBenefits_InitiaVisit_Value)
				->setCellValue('MX'.$si, $PS_Naturopathy_MaxBenefits_SubsequentVisit_Value)
				->setCellValue('MY'.$si, $PS_RemedialMassage)
				->setCellValue('MZ'.$si, $PS_RemedialMassage_Covered)
				->setCellValue('NA'.$si, $PS_RemedialMassage_WaitingPeriod)
				->setCellValue('NB'.$si, $PS_RemedialMassage_WaitingPeriod_Days)
				->setCellValue('NC'.$si, $PS_RemedialMassage_CellValueBenefitLimits)
				->setCellValue('ND'.$si, $PS_RemedialMassage_CellValueMaxBenefits)
				->setCellValue('NE'.$si, $PS_RemedialMassage_BenefitLimits)
				->setCellValue('NF'.$si, $PS_RemedialMassage_BenefitLimits_Value)
				->setCellValue('NG'.$si, $PS_RemedialMassage_BenefitLimits_Description)
				->setCellValue('NH'.$si, $PS_RemedialMassage_BenefitLimits_Liefetime)
				->setCellValue('NI'.$si, $PS_RemedialMassage_BenefitLimits_Liefetime_Value)
				->setCellValue('NJ'.$si, $PS_RemedialMassage_BenefitLimits_Liefetime_Description)
				->setCellValue('NK'.$si, $PS_RemedialMassage_MaxBenefits_InitiaVisit)
				->setCellValue('NL'.$si, $PS_RemedialMassage_MaxBenefits_SubsequentVisit)
				->setCellValue('NM'.$si, $PS_RemedialMassage_MaxBenefits_InitiaVisit_Value)
				->setCellValue('NN'.$si, $PS_RemedialMassage_MaxBenefits_SubsequentVisit_Value)
				->setCellValue('NO'.$si, $PS_HearingAids)
				->setCellValue('NP'.$si, $PS_HearingAids_Covered)
				->setCellValue('NQ'.$si, $PS_HearingAids_WaitingPeriod)
				->setCellValue('NR'.$si, $PS_HearingAids_WaitingPeriod_Days)
				->setCellValue('NS'.$si, $PS_HearingAids_CellValueBenefitLimits)
				->setCellValue('NT'.$si, $PS_HearingAids_CellValueMaxBenefits)
				->setCellValue('NU'.$si, $PS_HearingAids_BenefitLimits)
				->setCellValue('NV'.$si, $PS_HearingAids_BenefitLimits_Value)
				->setCellValue('NW'.$si, $PS_HearingAids_BenefitLimits_Description)
				->setCellValue('NX'.$si, $PS_HearingAids_BenefitLimits_Liefetime)
				->setCellValue('NY'.$si, $PS_HearingAids_BenefitLimits_Liefetime_Value)
				->setCellValue('NZ'.$si, $PS_HearingAids_BenefitLimits_Liefetime_Description)
				->setCellValue('OA'.$si, $PS_HearingAids_MaxBenefits_PerHearingAid)
				->setCellValue('OB'.$si, $PS_HearingAids_MaxBenefits_PerHearingAid_Value)
				->setCellValue('OC'.$si, $PS_BloodGlucoseMonitors)
				->setCellValue('OD'.$si, $PS_BloodGlucoseMonitors_Covered)
				->setCellValue('OE'.$si, $PS_BloodGlucoseMonitors_WaitingPeriod)
				->setCellValue('OF'.$si, $PS_BloodGlucoseMonitors_WaitingPeriod_Days)
				->setCellValue('OG'.$si, $PS_BloodGlucoseMonitors_CellValueBenefitLimits)
				->setCellValue('OH'.$si, $PS_BloodGlucoseMonitors_CellValueMaxBenefits)
				->setCellValue('OI'.$si, $PS_BloodGlucoseMonitors_BenefitLimits)
				->setCellValue('OJ'.$si, $PS_BloodGlucoseMonitors_BenefitLimits_Value)
				->setCellValue('OK'.$si, $PS_BloodGlucoseMonitors_BenefitLimits_Description)
				->setCellValue('OL'.$si, $PS_BloodGlucoseMonitors_BenefitLimits_Liefetime)
				->setCellValue('OM'.$si, $PS_BloodGlucoseMonitors_BenefitLimits_Liefetime_Value)
				->setCellValue('ON'.$si, $PS_BloodGlucoseMonitors_BenefitLimits_Liefetime_Description)
				->setCellValue('OO'.$si, $PS_BloodGlucoseMonitors_MaxBenefits_PerMonitor)
				->setCellValue('OP'.$si, $PS_BloodGlucoseMonitors_MaxBenefits_PerMonitor_Value)
				->setCellValue('OQ'.$si, $PS_Ambulance)
				->setCellValue('OR'.$si, $PS_Ambulance_Covered)
				->setCellValue('OS'.$si, $PS_Ambulance_WaitingPeriod)
				->setCellValue('OT'.$si, $PS_Ambulance_WaitingPeriod_Days)
				->setCellValue('OU'.$si, $PS_Ambulance_CellValueBenefitLimits)
				->setCellValue('OV'.$si, $PS_Ambulance_CellValueMaxBenefits)
				->setCellValue('OW'.$si, $PS_Ambulance_BenefitLimits)
				->setCellValue('OX'.$si, $PS_Ambulance_BenefitLimits_Value)
				->setCellValue('OY'.$si, $PS_Ambulance_BenefitLimits_Description)
				->setCellValue('OZ'.$si, $PS_Ambulance_BenefitLimits_Liefetime)
				->setCellValue('PA'.$si, $PS_Ambulance_BenefitLimits_Liefetime_Value)
				->setCellValue('PB'.$si, $PS_Ambulance_BenefitLimits_Liefetime_Description)
				->setCellValue('PC'.$si, $PS_PreferredServiceProviderArrangements)
				->setCellValue('PD'.$si, $PS_OtherFeatures)
				->setCellValue('PE'.$si, $PS_DateIssued)
				->setCellValue('PF'.$si, $WS_Hos_Excess_Filter_Value)
				->setCellValue('PG'.$si, $WS_Hos_CoPayment_Filter_Value);
				$si++;
				//************************** Export Excel Set The Value End Here *******************************//
				$sno++; 
	        }
	    } else {
		    $objPHPExcel= "No Policy(ies) found"."\n";
		}
	} else {
		    $objPHPExcel= "No Policy(ies) found"."\n";
    }
    function checknull($data) {
    	return ($data==NULL)?'NULL':$data;
    }
	function cellColor($cells,$color)
	{
					global $objPHPExcel;
					$objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()
					->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,
					'startcolor' => array('rgb' => $color)
					));
	}
    $strFileName ="HealthCareInsurance_".time().".xlsx";
	$strDirname = "other/excel/";
	$strXlFileNm = $strDirname.$strFileName;

	// Redirect output to a client’s web browser (Excel2007)

	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename='.$strFileName.'');
	header('Cache-Control: max-age=0');

	// If you're serving to IE 9, then the following may be needed

	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed

	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');

	//$objWriter->save($strXlFileNm);
	exit;
		
?>