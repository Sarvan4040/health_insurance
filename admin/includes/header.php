<?php
  // $Control_Usertype=$_SESSION["user_type_name"];
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Health Insurance- Admin Control Panel</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="images/favicon.png" type="image/png" sizes="16x16">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/jquery.dataTables.css">
  <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
  <!-- <link rel="stylesheet" href="css/jquery-ui.css"> -->
  <script src="js/jquery.min.js"></script>  
  <script src="js/jquery.validate.js"></script>
  <script src="js/additional-methods.min.js"></script> 

  <style>
	.inner-drop-down li{
		width:100%;
	}
  </style>
</head>
<body class="body_container">
	
	<div class="container search_container">
		<p class="notice"></p>
		<div class="col-md-9 col-sm-9 col-xs-12 game-stats-logo">
			<!-- <img src="images/logo-white.png" class="img-responsive lg_image" > -->
		</div>
		<div class="col-md-3 col-sm-3 col-xs-12 lg_image">
		   <div class="col-md-6 col-sm-6 col-xs-6 " style="padding-top: 15px;">
		       <div class="col-md-8 col-sm-8 col-xs-8" style="text-align: right;">
                 <img src="images/admin-icon.png">
			   </div>
			   <div class="col-md-4 col-sm-4 col-xs-4" style="padding-left: 0px;">
                <!-- <b><?php echo $_SESSION['admin_name']; ?></b> -->
                <b>Admin</b>
			   </div>
		   </div>
		   <div class="col-md-6 col-sm-6 col-xs-6" style="padding-top: 15px;">
		      <div class="col-md-9 col-sm-9 col-xs-9" style="text-align: right;width: 24px;">
                  <img src="images/logout.png"> 
			   </div>
			    <div class="col-md-3 col-sm-3 col-xs-3">
                  <a href="logout.php" style="color:white;text-decoration:none;"><b style="color:white;text-decoration:none;">Logout</b></a>
			   </div>
			</div>
		</div>
	</div>
	<div class="container navi_container container_shadow">
		<div class="container" class="nav_btn_container">
			<!-- Navigation -->
			<nav class="navbar navbar-default" role="navigation" style="border:none;margin:0;">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
			  	<div class="navbar-collapse collapse navi_menu">
					<ul class="col-md-12 col-sm-12 nav navbar-nav menu_navi_class">
						<li id="cities">
							<a class="menu_center dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);"><p class="menu_title">Policy<span class="caret menu_caret"></span></p></a>
							<ul class="dropdown-menu inner-drop-down">
								<li><a href="upload_policies.php">Upload Policies</a></li>
								<li><a href="policies_listing.php">Policies List</a></li>
							</ul>
						</li>					    
					</ul>
			  	</div>
			</nav>
		</div>
	</div>