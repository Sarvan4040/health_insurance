<?php	
	include("includes/configure.php");
	if(isset($_POST["email"])) {
	    $strEmail		=	trim($_POST["email"]); 
	    $strPass		=   base64_encode($_POST["password"]);
	    $QryChkEmail	=	$dbconn->prepare("SELECT * FROM tbl_admin WHERE email=:username and password=:password");
	    $QryChkEmail->execute(array(":username"=>$strEmail,":password"=>$strPass));
		$num_rows		=	 $QryChkEmail->rowCount();
		if($num_rows>0){
			$row 							=	$QryChkEmail->fetch(PDO::FETCH_ASSOC);		
			$_SESSION["email"]				=	$row['email'];
			$_SESSION["admin_id"] 			=	$row['admin_id'];
			$_SESSION["admin_name"] 		=	$row['admin_name'];	
			$_SESSION["user_type_name"]     =	$row['user_type_name'];		
			header("location:policies_listing.php");
			exit;
		} else{
			$strMsg = "Invalid email address or password";
		}
	}
?>

<html lang="en">
	<head>
		<title>HealthCare Insurance</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" href="images/favicon.png" type="image/png" sizes="16x16">
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="css/font-awesome.min.css">
	</head>
	<body class="body_container">
		<div class="container">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-4"></div>
				<div class="col-md-4 col-sm-12 col-xs-12" style="margin-top:10%;">
					<?php if(!empty($strMsg)){?>
					<div class="alert alert-danger fade in">
						<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">X</a>
						<strong><?php echo $strMsg; ?></strong> 
					</div>
					<?php } ?>
					<!-- <div class="logo_img login_caption">
						<img src="images/logo.png" class="img-responsive lg_image" >
					</div> -->
				  	<form role="form" method="post" id="LogIn">
						<div class="form-group">
					  		<input type="email" class="form-control text_box_login" id="email" name="email" value="" placeholder="Enter email">
						</div>
						<div class="form-group">
					  		<input type="password" class="form-control text_box_login" id="pwd" name="password" value="" placeholder="Enter password">
						</div>
						<button type="submit" class="btn btn-success text_box_login" id="SignIn" style="width:100%;">Sign in</button>
				  	</form>
				</div>
				<div class="col-md-4"></div>
		  </div>
		</div>
		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/jquery.validate.js"></script>
		<script src="js/custom.js"></script>
		<script>
			$("#LogIn").validate({
				rules: {
					email: {
						required: true,
						email: true
					},
					password:"required",	
				},
				messages: {
					email: {
						required: "Please enter email address",
						email: "Please enter valid email address"
					},
					password: "Please enter password",
				},
			});
		</script>
	</body>
</html>
