<?php
set_time_limit(0);
error_reporting(0);
include("includes/configure.php");
include("includes/session_check.php");
/****Paging ***/
$Page=1;$RecordsPerPage=100;
if(isset($_REQUEST['HdnPage']) && is_numeric($_REQUEST['HdnPage']))
	$Page=$_REQUEST['HdnPage'];
/*End of paging*/
$condtionArr=array();
$qrycndn="";
if (isset($_POST['srch_policy'])) {
	$srch_policy=$_POST['srch_policy'];
	$qrycndn.=" And plcy.WS_PolicyName like :policyname";
	$condtionArr[":policyname"]   = "%".$srch_policy."%";
}
if (count($_SESSION['insurance_id'])>0) {
	$insuranceid = implode(",",$_SESSION['insurance_id']);	
	$qrycndn .= "and plcy.insurance_id in (".$insuranceid.")";
}
$exprtdisplay = (!empty($qrycndn))?"none":"";

include("includes/header.php");
?>
<style>
	.policyurl:hover {
    	text-decoration: none;
	}
</style>
<form id="frm_policies_list" name="frm_policies_list" method="post">
<div class="container container_table_shadow">
	<div class="col-md-12 col-sm-12 col-xs-12 open_search" style="margin-top: 25px;margin-bottom: 25px;">
		<div class="col-md-12 col-sm-12 col-xs-12 filterbox">
			<div class="col-md-12 bottom_filters">
		       	<div class="col-md-9 col-sm-11 col-xs-12 mainsearch">
					<div class="col-md-2 col-sm-2 col-xs-12 polcy_filter" >
						<p class="control-label">Policy Name:</p>
					</div>
					<div class="col-md-5 col-sm-5 col-xs-12 bottom_filters policy_inputbox">
						<input type="text" class="form-control" placeholder="Policy Name" name="srch_policy" value="<?php echo $srch_policy; ?>">
					</div>
					<div class="col-md-5 col-sm-5 col-xs-12 policy_btnsearch">
						<button type="submit" class="btn btn-warning modify_button" id="search_btn_id">Search</button>
						<a href="policies_listing.php" class="btn btn-warning modify_button" id="showbtn_id">Show All Policies</a>
			        </div>
			   	</div>				   	
			</div>
		</div>
	</div>
</div>
<div class="container container_table_shadow">
	<p class="policy_result"><i class="fa fa-list" aria-hidden="true"></i> Policies Listing</p>
	<div class="modify_game" id="btnallbox">
	    <input type="button" class="btn btn-warning modify_button" id="exportexcel" value="Export Policies" style='margin-right:5px;display:<?php echo $exprtdisplay?>' >
	    <input type="button" id="viewallpolicies" name="viewallpolicies" class="btn btn-warning modify_button" value="View All Policies" style='margin-right:5px;'>
		<a href="upload_policies.php" class="btn btn-warning modify_button upld_policies">Upload Policies</a>
	</div>

	<div class="table-responsive">
	     
			<input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
			<input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
			<input type="hidden" name="exportexcel" id="exportexcelid" value="">
			<input type="hidden" name="exporteventid" id="exporteventid" value="">
			<table class="table table-hover table-striped table-bordered table-highlight-head text-center search-view listdatatable" id="policy_result">
				<thead>
					<tr>
						<th class="nowrap">Insurance Id</th>
						<th class="nowrap no-sort">Batch Id</th>
						<th class="nowrap no-sort">Policy Name</th>
						<th class="nowrap no-sort">Policy URL</th>
						<th class="nowrap no-sort">State</th>
						<th class="nowrap no-sort">Cover Type</th>
					</tr>
				</thead>
		        <tbody>
					<?php					    
						$Arraustate = array("ACT"=>"Australian Capital Territory","NSW"=>"New South Wales","NT"=>"Northern Territory","QLD"=>"Queensland","SA"=>"South Australia","TAS"=>"Tasmania","VIC"=>"Victoria","WA"=>"Western Australia");
					   	$dbQry="SELECT plcy.insurance_id,plcy.WS_BatchId,plcy.WS_PolicyCode,plcy.WS_PolicyUrl,plcy.WS_State,plcy.WS_PolicyName,plcy.WS_CoverType FROM tbl_policies as plcy JOIN tbl_ps1_policies as ps1 on plcy.insurance_id = ps1.insurance_id JOIN tbl_ps2_policies as ps2 on plcy.insurance_id = ps2.insurance_id JOIN tbl_ws_policies as ws on plcy.insurance_id = ws.insurance_id WHERE plcy.insurance_id <>'' $qrycndn order by plcy.insurance_id asc";
					    $getResQry		=	$dbconn->prepare($dbQry);
						$getResQry->execute($condtionArr);						
						$getResCnt		=	$getResQry->rowCount();
						$getResQry->closeCursor();
						if($getResCnt>0){
						$TotalPages=ceil($getResCnt/$RecordsPerPage);
						$Start=($Page-1)*$RecordsPerPage;
						$sno=$Start+1;							
						$dbQry.=" limit $Start,$RecordsPerPage";								
						$getResQry		=	$dbconn->prepare($dbQry);
						$getResQry->execute($condtionArr);
						$getResCnt		=	$getResQry->rowCount();
						if($getResCnt>0){
					    $getResRows		=	$getResQry->fetchAll();
						$getResQry->closeCursor();
						foreach($getResRows as $getResRow){
							$insurance_id       =	$getResRow['insurance_id'];
							$WS_BatchId			=	$getResRow['WS_BatchId'];
							$WS_PolicyName		=	$getResRow['WS_PolicyName'];
							$WS_PolicyUrl		=	$getResRow['WS_PolicyUrl'];
							$WS_State			=	$getResRow['WS_State'];
							$WS_CoverType		=	$getResRow['WS_CoverType'];							
							?>		        	
							<tr>
								<td><?php echo $insurance_id; ?></td>
								<td><?php echo $WS_BatchId; ?></td>
								<td><?php echo $WS_PolicyName;?></td>
								<td><a class="policyurl" target="_blank" href="<?php echo $WS_PolicyUrl; ?>"><?php echo $WS_PolicyUrl; ?></a></td>
								<td><?php echo $Arraustate[$WS_State];?></td>
								<td><?php echo $WS_CoverType;?></td>											
							</tr>
						<?php
							$sno++;
						}
					}
					else{
						echo "<tr><td colspan=\"10\" style='text-align:center;color:red;'>No Insurance policy(ies) found.</td></tr>";
						}
					}
					else{
							echo "<tr><td colspan='10' style='text-align:center;color:red;'>No  Insurance policy(ies) found.</td></tr>";
					}
				   ?>		            
				</tbody>		            				
			</table>
			
			 <?php
				if($TotalPages > 1) {
					echo "<tr><td style='text-align:center;overflow:none;' colspan='8' valign='middle' class='pagination'>";
					$FormName = "frm_policies_list";
					require_once ("paging.php");
					echo "</td></tr>";
				}
			?>
								
	</div>	
</div>
</form>	
<?php include("includes/footer.php");?>
<script src="js/jquery.dataTables.min.js"></script>
<script>
	$(document).ready(function() {
	    $('#policy_result').DataTable( {	        
	        "searching": false,
	        "paging":   false,        
	        "info":     false,
	        columnDefs: [{
			    orderable: false,
	      		targets: "no-sort"
	    	}]
	    });
	    $('#search_btn_id').click(function (){	
		    var formdata = $("#frm_policies_list").serializeArray();
			$('form').attr('action', '');
			$('form').submit();
	    });
	    $("#exportexcel").click(function(){
			$('#exportexcelid').val('exportexcel');			
			var formdata = $("#frm_policies_list").serializeArray();
			$('form').attr('action', 'export_policies.php');
			$('form').submit();
			return true;
		});
		$("#viewallpolicies").click(function(){			
			$('form').attr('action','policies_listing.php');
			var session = "<?php $_SESSION['insurance_id'] = array();?>";
			$('form').submit();
			return true;
		});
	});
</script>