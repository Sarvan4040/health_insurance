<?php
include("includes/configure.php");
require_once('api.php');
error_reporting(0);

if(isset($_POST['filter_selected'])){
	$filter_selected = json_decode($_POST['filter_selected'],true);
	// print_r($filter_selected);
	//To get individual button click value
	$lookingFor = $filter_selected["looking_for"];
    $needCover  = $filter_selected["needs_class"];
    if (strpos($needCover, '-') !== false) {
      $needCoverArr = explode('-', $needCover);
      $needCover = $needCoverArr[0];
    } else{
      $needCover = $needCover;
    }
    $typeCover  = $filter_selected["type_class"];
    $state      = $filter_selected["state_class"];
    $min        = $filter_selected["min"];
    $max        = $filter_selected["max"];
    $ArraypopupOptions =  $filter_selected['popup_options'];

    $order =  $filter_selected['order'];
    $page =  $filter_selected['page_no'];
    $fund_name =  $filter_selected['fund_name'];

    if ($page <= 0) $page = 1;
	else $page = $page;
	$per_page = 3;
    
    $getInitialResultPage = $returnHealthCareObject->funSearchListDisplayDetails($filter_selected);

  	$resultArr = json_decode($getInitialResultPage,true);

  	// print_r($resultArr);
	$policies_count = $resultArr['policies_count'];
	$need_cover = $resultArr['need_cover']; 
	$state = $resultArr['state']; 
	$type_cover = $resultArr['type_cover']; 

	$min = $resultArr['min']; 
	$max = $resultArr['max'];

	$funds = $resultArr['funds']; 

	$fundArrCount = count($resultArr['funds']);
	if ($fundArrCount == 3) {
		$fundDetailsArr = array_merge_recursive($resultArr['funds'][0], $resultArr['funds'][1],$resultArr['funds'][2]);
	} else if ($fundArrCount == 2) {
		$fundDetailsArr = array_merge_recursive($resultArr['funds'][0], $resultArr['funds'][1]);
	} else {
		$fundDetailsArr = $resultArr['funds'][0];
	}
	
	// print_r($fundDetailsArr['extras_cover']['covered']);
	// echo "<br>";
	// print_r($fundDetailsArr['extras_cover']['annual_limit']);
	function pagination($total,$per_page,$page){   
	    $total = $total;
	    $adjacents = "2"; 
	     
	    $prevlabel = "Prev";
	    $nextlabel = "Next";
	     
	    $page = ($page == 0 ? 1 : $page);  
	    $start = ($page - 1) * $per_page;                               
	     
	    $prev = $page - 1;                          
	    $next = $page + 1;
	     
	    $lastpage = ceil($total/$per_page);
	     
	    $lpm1 = $lastpage - 1;
	     
	    $pagination = "";
	    if($lastpage > 1){   
	        $pagination .= "<ul class='pagination'>";
	        // $pagination .= "<li class='page_info'>Page {$page} of {$lastpage}</li>";
	             
	        if ($page > 1) $pagination.= "<li><a href='#' class='page_no'>{$prevlabel}</a></li>";
	             
	        if ($lastpage < 7 + ($adjacents * 2)){  
	            for ($counter = 1; $counter <= $lastpage; $counter++){
	                if ($counter == $page)
	                    $pagination.= "<li><a class='current page_no'>{$counter}</a></li>";
	                else
	                    $pagination.= "<li><a href='#' class='page_no'>{$counter}</a></li>";                    
	            }
	        } elseif($lastpage > 5 + ($adjacents * 2)){
	            if($page < 1 + ($adjacents * 2)) {
	                 
	                for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++){
	                    if ($counter == $page)
	                        $pagination.= "<li><a class='current page_no'>{$counter}</a></li>";
	                    else
	                        $pagination.= "<li><a href='#' class='page_no'>{$counter}</a></li>";                    
	                }
	                $pagination.= "<li class='dot'>...</li>";
	                $pagination.= "<li><a href='#' class='page_no'>{$lpm1}</a></li>";
	                $pagination.= "<li><a href='#' class='page_no'>{$lastpage}</a></li>";         
	            } elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
	                 
	                $pagination.= "<li><a href='#' class='page_no'>1</a></li>";
	                $pagination.= "<li><a href='#' class='page_no'>2</a></li>";
	                $pagination.= "<li class='dot'>...</li>";
	                for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
	                    if ($counter == $page)
	                        $pagination.= "<li><a class='current page_no'>{$counter}</a></li>";
	                    else
	                        $pagination.= "<li><a href='#' class='page_no'>{$counter}</a></li>";                    
	                }
	                $pagination.= "<li class='dot'>..</li>";
	                $pagination.= "<li><a href='#' class='page_no'>{$lpm1}</a></li>";
	                $pagination.= "<li><a href='#' class='page_no'>{$lastpage}</a></li>";       
	            } else {
	                $pagination.= "<li><a href='#' class='page_no'>1</a></li>";
	                $pagination.= "<li><a href='#' class='page_no'>2</a></li>";
	                $pagination.= "<li class='dot'>..</li>";
	                for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
	                    if ($counter == $page)
	                        $pagination.= "<li><a class='current page_no'>{$counter}</a></li>";
	                    else
	                        $pagination.= "<li><a href='#' class='page_no'>{$counter}</a></li>";                    
	                }
	            }
	        }

	        if ($page < $counter - 1) {
				$pagination.= "<li><a href='#' class='page_no'>{$nextlabel}</a></li>";
			}
	        $pagination.= "</ul>";        
	    }
	    return $pagination;
	}
}
require_once("includes/header.php");
?>
<div id="main_container_result">

	<!-- Filters  -->
	<form name="result_form" id="result_form" action="" method="post">
		<input type="hidden" id="filter_selected" name="filter_selected" value='<?php echo json_encode($filter_selected); ?>'>
		<div class="container-fluid result_container">
			<div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8 col-sm-12 col-xs-12 result_main_contain">
				<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 pagination_container">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 paging">
					<?php echo pagination($policies_count,$per_page,$page); ?>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 key_filters">
						<h4 class="filter_options_header"><?php echo $policies_count; ?> policies found for <?php echo $need_cover; ?> | <?php echo $state; ?> | <?php echo $type_cover; ?> | between <?php echo $min; ?> and <?php echo $max; ?> a month </h4>
					</div>
				</div>
				<div class="col-lg-5 col-md-5 col-sm-5 hidden-xs result_filter_container">
					<?php if ($order=='asc' || $order=='') {
						$asc_class = 'active';
						$desc_class = '';
					} else if ($order=='desc') {
						$desc_class = '';
						$desc_class = 'active';
					} ?>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-6 result_filter"><button type="button" id="asc" class="result_filter_btn filter_btn_1 more_btn <?php echo $asc_class; ?>">Sort Price Low to High</button></div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-6 result_filter"><button type="button" id="desc" class="result_filter_btn filter_btn_1 more_btn <?php echo $desc_class; ?>">Sort Price High to Low</button></div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 result_filter"><input type="text" id="fund_search" class="fund_search result_search_btn filter_btn_1 more_btn" placeholder="Search by fund (e.g Medibank, Bupa, Nib)" value="<?php echo $fund_name; ?>" /></div>
				</div>
				<div class="hidden-lg hidden-md hidden-sm col-xs-offset-1 col-xs-10 result_filter_container">
					<?php if ($order=='asc' || $order=='') {
						$asc_class = 'active';
						$desc_class = '';
					} else if ($order=='desc') {
						$desc_class = '';
						$desc_class = 'active';
					} ?>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-6 result_filter"><button type="button" id="asc" class="result_filter_btn filter_btn_1 more_btn <?php echo $asc_class; ?>">Sort Price Low to High</button></div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-6 result_filter"><button type="button" id="desc" class="result_filter_btn filter_btn_1 more_btn <?php echo $desc_class; ?>">Sort Price High to Low</button></div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 result_filter"><input type="text" id="fund_search" class="fund_search result_search_btn filter_btn_1 more_btn" placeholder="Search by fund (e.g Medibank, Bupa, Nib)" value="<?php echo $fund_name; ?>" /></div>
				</div>
				<?php if($policies_count == 0){
					echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 results_main" id="dynamic_results"><h4 class="bank_name">No insurance(s) found</h4><div class="col-lg-offset-4 col-lg-4 col-md-offset-4 col-md-4 col-sm-offset-4 col-sm-4 col-xs-12 back_btn"><a href="index.php" type="button" id="desc" class="filter_btn filter_btn_1 more_btn active">Back to Home Page</a></div></div>';
				} else { ?>
					<div class="col-lg-12 col-md-12 col-sm-12 hidden-xs results_main" id="dynamic_results">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 result_section fixed_head">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
								<div class="col-lg-4 col-md-3 col-sm-4 col-xs-4 header_section">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bank_headers head"></div>
								</div>
								<div class="col-lg-8 col-md-9 col-sm-8 col-xs-8 result_section fund_section">
								<?php
									$lastFundProvider = end(array_keys($fundDetailsArr['fund_provider']));
									if ($fundArrCount == 3) {
										foreach ($fundDetailsArr['fund_provider'] as $fund_provider_key => $fund_provider_value) {
											if ($lastFundProvider==$fund_provider_key) {
												echo '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 bank_headers"><h4 class="bank_name"><a target="_blank" href="'.$fundDetailsArr['policy_url'][$fund_provider_key].'">'.$fund_provider_value.'</a></h4><p class="fund_type">'.$fundDetailsArr['fund_type'][$fund_provider_key].'</p></div>';
											} else {
												echo '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 bank_headers bor_right"><h4 class="bank_name"><a target="_blank" href="'.$fundDetailsArr['policy_url'][$fund_provider_key].'">'.$fund_provider_value.'</a></h4><p class="fund_type">'.$fundDetailsArr['fund_type'][$fund_provider_key].'</p></div>';
											}
										}
									} else if ($fundArrCount == 2) {
										foreach ($fundDetailsArr['fund_provider'] as $fund_provider_key => $fund_provider_value) {
											if ($lastFundProvider==$fund_provider_key) {
												echo '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 bank_headers"><h4 class="bank_name"><a target="_blank" href="'.$fundDetailsArr['policy_url'][$fund_provider_key].'">'.$fund_provider_value.'</a></h4><p class="fund_type">'.$fundDetailsArr['fund_type'][$fund_provider_key].'</p></div>';
											} else {
												echo '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 bank_headers bor_right"><h4 class="bank_name"><a target="_blank" href="'.$fundDetailsArr['policy_url'][$fund_provider_key].'">'.$fund_provider_value.'</a></h4><p class="fund_type">'.$fundDetailsArr['fund_type'][$fund_provider_key].'</p></div>';
											}
										}
									} else {
										$fund_provider_value = $fundDetailsArr['fund_provider'];
										$fund_provider_type = $fundDetailsArr['fund_type']; 
										$fund_policy_url = $fundDetailsArr['policy_url']; 
										echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bank_headers"><h4 class="bank_name"><a target="_blank" href="'.$fund_policy_url.'">'.$fund_provider_value.'</a></h4><p class="fund_type">'.$fund_provider_type.'</p></div>';
									}
								?>
								</div>
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
								<div class="col-lg-4 col-md-3 col-sm-4 col-xs-4 header_section">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 premium_headers head bor_bottom">Monthly premium<br>(after 25.657% rebate)</div>
								</div>
								<div class="col-lg-8 col-md-9 col-sm-8 col-xs-8 result_section fund_section">
								<?php
									$last_monthly_premium = end(array_keys($fundDetailsArr['monthly_premium']));
									if ($fundArrCount == 3) {
										foreach ($fundDetailsArr['monthly_premium'] as $monthly_premium_key => $monthly_premium_value) {
											if ($last_monthly_premium==$monthly_premium_key) {
												echo '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 premium_headers bor_bottom"><h4 class="policy_price">'.$monthly_premium_value.'</h4></div>';
											} else {
												echo '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 premium_headers bor_bottom_right"><h4 class="policy_price">'.$monthly_premium_value.'</h4></div>';
											}
										}
									} else if ($fundArrCount == 2) {
										foreach ($fundDetailsArr['monthly_premium'] as $monthly_premium_key => $monthly_premium_value) {
											if ($last_monthly_premium==$monthly_premium_key) {
												echo '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 premium_headers bor_bottom"><h4 class="policy_price">'.$monthly_premium_value.'</h4></div>';
											} else {
												echo '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 premium_headers bor_bottom_right"><h4 class="policy_price">'.$monthly_premium_value.'</h4></div>';
											}
										}
									} else {
										$monthly_premium_value = $fundDetailsArr['monthly_premium'];
										echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 premium_headers bor_bottom"><h4 class="policy_price">'.$monthly_premium_value.'</h4></div>';
									}
								?>
								</div>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 result_section">	
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
								<div class="col-lg-4 col-md-3 col-sm-4 col-xs-4 header_section">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 policy_headers head bor_bottom">Policy name</div>
								</div>
								<div class="col-lg-8 col-md-9 col-sm-8 col-xs-8 result_section fund_section">
								<?php
									$last_policy_name = end(array_keys($fundDetailsArr['policy_name']));
									if ($fundArrCount == 3) {
										foreach ($fundDetailsArr['policy_name'] as $policy_name_key => $policy_name_value) {
											if ($last_policy_name==$policy_name_key) {
												echo '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 policy_headers bor_bottom"><h4 class="policy_name">'.$policy_name_value.'</h4></div>';
											} else {
												echo '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 policy_headers bor_bottom_right"><h4 class="policy_name">'.$policy_name_value.'</h4></div>';
											}
										}
									} else if ($fundArrCount == 2) {
										foreach ($fundDetailsArr['policy_name'] as $policy_name_key => $policy_name_value) {
											if ($last_policy_name==$policy_name_key) {
												echo '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 policy_headers bor_bottom"><h4 class="policy_name">'.$policy_name_value.'</h4></div>';
											} else {
												echo '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 policy_headers bor_bottom_right"><h4 class="policy_name">'.$policy_name_value.'</h4></div>';
											}
										}
									} else {
										$policy_name_value = $fundDetailsArr['policy_name'];
										echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 policy_headers bor_bottom"><h4 class="policy_name">'.$policy_name_value.'</h4></div>';
									}
								?>
								</div>
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
								<div class="col-lg-4 col-md-3 col-sm-4 col-xs-4 header_section">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 visit_headers head bor_bottom">Excess per visit</div>
								</div>
								<div class="col-lg-8 col-md-9 col-sm-8 col-xs-8 result_section fund_section">
								<?php
									$last_per_visit = end(array_keys($fundDetailsArr['per_visit']));
									if ($fundArrCount == 3) {
										foreach ($fundDetailsArr['per_visit'] as $per_visit_key => $per_visit_value) {
											if ($last_per_visit==$per_visit_key) {
												echo '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 visit_headers bor_bottom"><h4 class="visit_price">'.$per_visit_value.'</h4></div>';
											} else {
												echo '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 visit_headers bor_bottom_right"><h4 class="visit_price">'.$per_visit_value.'</h4></div>';
											}
										}
									} else if ($fundArrCount == 2) {
										foreach ($fundDetailsArr['per_visit'] as $per_visit_key => $per_visit_value) {
											if ($last_per_visit==$per_visit_key) {
												echo '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 visit_headers bor_bottom"><h4 class="visit_price">'.$per_visit_value.'</h4></div>';
											} else {
												echo '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 visit_headers bor_bottom_right"><h4 class="visit_price">'.$per_visit_value.'</h4></div>';
											}
										}
									} else {
										$per_visit_value = $fundDetailsArr['per_visit'];
										echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 visit_headers bor_bottom"><h4 class="visit_price">'.$per_visit_value.'</h4></div>';
									}
								?>
								</div>
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
								<div class="col-lg-4 col-md-3 col-sm-4 col-xs-4 header_section">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payment_headers head bor_bottom">Co-payment</div>
								</div>
								<div class="col-lg-8 col-md-9 col-sm-8 col-xs-8 result_section fund_section">
								<?php
									$last_co_payment = end(array_keys($fundDetailsArr['co_payment']));
									if ($fundArrCount == 3) {
										foreach ($fundDetailsArr['co_payment'] as $co_payment_key => $co_payment_value) {
											if ($last_co_payment==$co_payment_key) {
												echo '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 payment_headers bor_bottom"><h4 class="payment">'.$co_payment_value.'</h4></div>';
											} else {
												echo '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 payment_headers bor_bottom_right"><h4 class="payment">'.$co_payment_value.'</h4></div>';
											}
										}
									} else if ($fundArrCount == 2) {
										foreach ($fundDetailsArr['co_payment'] as $co_payment_key => $co_payment_value) {
											if ($last_co_payment==$co_payment_key) {
												echo '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 payment_headers bor_bottom"><h4 class="payment">'.$co_payment_value.'</h4></div>';
											} else {
												echo '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 payment_headers bor_bottom_right"><h4 class="payment">'.$co_payment_value.'</h4></div>';
											}
										}
									} else {
										$co_payment_value = $fundDetailsArr['co_payment'];
										echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payment_headers bor_bottom"><h4 class="payment">'.$co_payment_value.'</h4></div>';
									}
								?>
								</div>
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
								<div class="col-lg-4 col-md-3 col-sm-4 col-xs-4 header_section">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payment_headers head"></div>
								</div>
								<div class="col-lg-8 col-md-9 col-sm-8 col-xs-8 result_section fund_section">
								<?php
									if ($fundArrCount == 3) {
										echo '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 payment_headers bor_right"></div><div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 payment_headers bor_right"></div><div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 payment_headers"></div>';
									} else if ($fundArrCount == 2) {
										echo '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 payment_headers bor_right"></div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 payment_headers"></div>';
									} else {
										echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payment_headers"></div>';
									}
								?>
								</div>
							</div>
						</div>
						<?php if ($type_cover=="Hospital Only" || $type_cover=="Hospital + Extras") { ?>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_header">HOSPITAL COVER</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 result_section">
								<?php 
									$ArrHospitalOptionsArr = array("pregnancy" =>"Pregnancy","fertility_treatment" =>"Fertility treatment (IVF)","heart_surgery" =>"Heart surgery (cardiac)","eye_surgery" =>"Eye surgery (cataract)","joints" =>"Joints (hip, knee, shoulder","renal_dialysis" =>"Renal dialysis (kidney)","weight_loss" =>"Weight loss (gastric)","sterilisation" =>"Sterilisation","plastic_surgery" =>"Plastic surgery","rehab_treatment" =>"Rehab treatment","psychatric" =>"Psychatric","palliative_care" =>"Palliative care");
									foreach ($fundDetailsArr['hospital_cover'] as $hospital_cover_key => $hospital_cover_value) {
										echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section"><div class="col-lg-4 col-md-3 col-sm-4 col-xs-4 header_section "><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">'.$ArrHospitalOptionsArr[$hospital_cover_key].'</div></div>';
										echo '<div class="col-lg-8 col-md-9 col-sm-8 col-xs-8 result_section fund_section">';
											$last_hospital_cover_value = end(array_keys($hospital_cover_value));
											if ($fundArrCount == 3) {
												foreach ($hospital_cover_value as $cover_key => $cover_value) {
													if ($cover_value=="Covered"){
														$cover_value = '<img src="images/tick.png">';
													} else if($cover_value=="Not Covered" || $cover_value=="n&#47a"){
														$cover_value = '<img src="images/cross.png">';
													} else{
														$cover_value = $cover_value;
													}
													
													if ($last_hospital_cover_value==$cover_key) {
														echo '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom"><h4 class="payment">'.$cover_value.'</h4></div>';
													} else {
														echo '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_bottom_right"><h4 class="payment">'.$cover_value.'</h4></div>';
													}
												}
											} else if ($fundArrCount == 2) {
												foreach ($hospital_cover_value as $cover_key => $cover_value) {
													if ($cover_value=="Covered"){
														$cover_value = '<img src="images/tick.png">';
													} else if($cover_value=="Not Covered" || $cover_value=="n&#47a"){
														$cover_value = '<img src="images/cross.png">';
													} else{
														$cover_value = $cover_value;
													}

													if ($last_hospital_cover_value==$cover_key) {
														echo '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 section_headers bor_bottom"><h4 class="payment">'.$cover_value.'</h4></div>';
													} else {
														echo '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 section_headers bor_bottom_right"><h4 class="payment">'.$cover_value.'</h4></div>';
													}
												}
											} else {
												$cover_value = $hospital_cover_value;
												if ($cover_value=="Covered"){
													$cover_value = '<img src="images/tick.png">';
												} else if($cover_value=="Not Covered" || $cover_value=="n&#47a"){

													$cover_value = '<img src="images/cross.png">';
												} else{
													$cover_value = $cover_value;
												}
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers bor_bottom"><h4 class="payment">'.$cover_value.'</h4></div>';
											}
										echo '</div></div>';
									}
								?>
								<?php if ($type_cover=="Hospital + Extras") { ?>
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
									<div class="col-lg-4 col-md-3 col-sm-4 col-xs-4 header_section"></div>
									<div class="col-lg-8 col-md-9 col-sm-8 col-xs-8 result_section fund_section">
										<?php
											if ($fundArrCount == 3) {
												echo '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_right"></div><div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers bor_right"></div><div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 section_headers"></div>';
											} else if ($fundArrCount == 2) {
												echo '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 section_headers bor_right"></div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 section_headers"></div>';
											} else {
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers"></div>';
											}
										?>
									</div>
								</div>
								<?php } ?>
							</div>
						<?php } 
						if ($type_cover=="Extras Only" || $type_cover=="Hospital + Extras") { ?>
							<div class="col-lg-12 col-md-12 col-sm-12 hidden-xs section_header extra_header_main">
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-2 extra_header">EXTRAS COVER</div>
								<!-- <div class="col-lg-1 col-md-2 col-sm-1 col-xs-2 btn_header"><button type="button" class="result_header_btn filter_btn_1 more_btn active">Covered</button></div> -->
								<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 btn_header"><button type="button" class="result_header_btn filter_btn_1 more_btn active">Annual limit</button></div>
								<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 btn_header"><button type="button" class="result_header_btn filter_btn_1 more_btn">Per service limit</button></div>
							</div>
							<div class="hidden-lg hidden-md hidden-sm col-xs-12 section_header extra_header_main">
								<div class="col-xs-6 extra_header">EXTRAS COVER</div>
								<div class="col-xs-6">
									<select name="select_div" class="select_div result_header_btn filter_btn_1 more_btn">
										<!-- <option selected="selected" value="Covered">Covered</option> -->
										<option value="Annual limit">Annual limit</option>
										<option value="Per service limit">Per service limit</option>
									</select>
								</div>
							</div>
							<!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 header_section covered_section">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 result_section">
									<?php 
										$ArrextrasOptionsArr = array("dental_cleaning" =>"Dental (cleaning, fillings)","dental_major" =>"Dental major (crowns)","dental_root" =>"Dental (root canal)","dental_braces" =>"Dental (braces)","optical" =>"Optical","physio" =>"Physio","chiro" =>"Chiro","massages" =>"Massages","acupunture" =>"Acupunture","hearing_aids" =>"Hearing aids","blood_glucose_monitors" =>"Blood glucose monitors","pharmaceutical" =>"Pharmaceutical (non PBS)","naturopathy" =>"Naturopathy","podiatry" =>"Podiatry (feet)","psychology" =>"Psychology");
										foreach ($fundDetailsArr['extras_cover']['covered'] as $extras_cover_key => $extras_cover_value) {
											echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section"><div class="col-lg-4 col-md-3 col-sm-4 col-xs-4 header_section "><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">'.$ArrextrasOptionsArr[$extras_cover_key].'</div></div>';
											echo '<div class="col-lg-8 col-md-9 col-sm-8 col-xs-8 result_section fund_section">';
											$last_extras_cover_value = end(array_keys($extras_cover_value));
											if ($fundArrCount == 3) {
												foreach ($extras_cover_value as $extras_cover_key => $extras_cover_value) {
													if ($extras_cover_value=="Covered"){
														$extras_cover_value = '<h4 class="payment_img"><img src="images/tick.png"></h4>';
													} else if($extras_cover_value=="Not Covered" || $extras_cover_value=="n&#47a"){
														$extras_cover_value = '<h4 class="payment_img"><img src="images/cross.png"></h4>';
													} else{
														$extras_cover_value = '<h4 class="cover_payment_text">'.$extras_cover_value.'</h4>';
													}
													
													if ($last_extras_cover_value==$extras_cover_key) {
														echo '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom">'.$extras_cover_value.'</div>';
													} else {
														echo '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 cover_extra_headers_content bor_bottom_right">'.$extras_cover_value.'</div>';
													}
												}
											} else if ($fundArrCount == 2) {
												foreach ($hospital_cover_value as $extras_cover_key => $extras_cover_value) {
													if ($extras_cover_value=="Covered"){
														$extras_cover_value = '<h4 class="payment_img"><img src="images/tick.png"></h4>';
													} else if($extras_cover_value=="Not Covered" || $extras_cover_value=="n&#47a"){
														$extras_cover_value = '<h4 class="payment_img"><img src="images/cross.png"></h4>';
													} else{
														$extras_cover_value = '<h4 class="cover_payment_text">'.$extras_cover_value.'</h4>';
													}

													if ($last_extras_cover_value==$extras_cover_key) {
														echo '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 cover_extra_headers_content bor_bottom">'.$extras_cover_value.'</div>';
													} else {
														echo '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 cover_extra_headers_content bor_bottom_right">'.$extras_cover_value.'</div>';
													}
												}
											} else {
												$extras_cover_value = $extras_cover_value;
												if ($extras_cover_value=="Covered"){
													$extras_cover_value = '<h4 class="payment_img"><img src="images/tick.png"></h4>';
												} else if($extras_cover_value=="Not Covered" || $extras_cover_value=="n&#47a"){
													$extras_cover_value = '<h4 class="payment_img"><img src="images/cross.png"></h4>';
												} else{
													$extras_cover_value = '<h4 class="cover_payment_text">'.$extras_cover_value.'</h4>';
												}
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cover_extra_headers_content bor_bottom">'.$extras_cover_value.'</div>';
											}
											echo '</div></div>';
										}
									?>
								</div>
							</div> -->
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 header_section annual_limit_section">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 result_section">
								<?php
									foreach ($fundDetailsArr['extras_cover']['annual_limit'] as $annual_key => $annual_value) {

										echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section"><div class="col-lg-4 col-md-3 col-sm-4 col-xs-4 header_section "><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">'.$ArrextrasOptionsArr[$annual_key].'</div></div>';
										echo '<div class="col-lg-8 col-md-9 col-sm-8 col-xs-8 result_section fund_section">';
										$last_annual_limit_value = end(array_keys($annual_value));
										if ($fundArrCount == 3) {
											foreach ($annual_value as $annual_limit_key => $annual_limit_value) {
												$cover_value=$fundDetailsArr['extras_cover']['covered'][$annual_key][$annual_limit_key];
												if ($cover_value=="Covered"){
													$cover_value = '<span class="tick"></span>';
												} else {
													$cover_value = '<span class="cross"></span>';
												}

												if ($annual_limit_value=="Covered"){
													$annual_limit_value = '<table class="payment_img"><tr><td><img src="images/tick.png"></td></tr></table>';
												} else if($annual_limit_value=="Not Covered"){
													$annual_limit_value = '<table class="payment_img"><tr><td><img src="images/cross.png"></td></tr></table>';
												} else{
													$annual_limit_value = str_replace("-(", "<br>", $annual_limit_value);
													$annual_limit_value = '<table class="annual_content"><tr><td>'.$cover_value.$annual_limit_value.'</td></tr></table>';
												}
												
												if ($last_annual_limit_value==$annual_limit_key) {
													echo '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom">'.$annual_limit_value.'</div>';
												} else {
													echo '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 annual_extra_headers_content bor_bottom_right">'.$annual_limit_value.'</div>';
												}
											}
										} else if ($fundArrCount == 2) {
											foreach ($annual_value as $annual_limit_key => $annual_limit_value) {
												$cover_value=$fundDetailsArr['extras_cover']['covered'][$annual_key][$annual_limit_key];
												if ($cover_value=="Covered"){
													$cover_value = '<span class="tick"></span>';
												} else {
													$cover_value = '<span class="cross"></span>';
												}
												if ($annual_limit_value=="Covered"){
													$annual_limit_value = '<table class="payment_img"><tr><td><img src="images/tick.png"></td></tr></table>';
												} else if($annual_limit_value=="Not Covered"){
													$annual_limit_value = '<table class="payment_img"><tr><td><img src="images/cross.png"></td></tr></table>';
												} else{
													$annual_limit_value = str_replace("-(", "<br>", $annual_limit_value);
													$annual_limit_value = '<table class="annual_content"><tr><td>'.$cover_value.$annual_limit_value.'</td></tr></table>';
												}

												if ($last_annual_limit_value==$annual_limit_key) {
													echo '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 annual_extra_headers_content bor_bottom">'.$annual_limit_value.'</div>';
												} else {
													echo '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 annual_extra_headers_content bor_bottom_right">'.$annual_limit_value.'</div>';
												}
											}
										} else {
											$cover_value=$fundDetailsArr['extras_cover']['covered'][$annual_key];
											if ($cover_value=="Covered"){
												$cover_value = '<span class="tick"></span>';
											} else {
												$cover_value = '<span class="cross"></span>';
											}
											$annual_limit_value = $annual_value;
											// echo nl2br($annual_limit_value);
											if ($annual_limit_value=="Covered"){
												$annual_limit_value = '<table class="payment_img"><tr><td><img src="images/tick.png"></td></tr></table>';
											} else if($annual_limit_value=="Not Covered"){
												$annual_limit_value = '<table class="payment_img"><tr><td><img src="images/cross.png"></td></tr></table>';
											} else{
												$annual_limit_value = str_replace("-(", "<br>", $annual_limit_value);
												$annual_limit_value = '<table class="annual_content"><tr><td>'.$cover_value.$annual_limit_value.'</td></tr></table>';
											}
											echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 annual_extra_headers_content bor_bottom">'.$annual_limit_value.'</div>';
										}
										echo '</div></div>';
									}
								?>
								</div>
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 header_section service_limit_section">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 result_section">
								<?php
									foreach ($fundDetailsArr['extras_cover']['service_limit'] as $service_key => $service_value) {
										echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section"><div class="col-lg-4 col-md-3 col-sm-4 col-xs-4 header_section "><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">'.$ArrextrasOptionsArr[$service_key].'</div></div>';
										echo '<div class="col-lg-8 col-md-9 col-sm-8 col-xs-8 result_section fund_section">';
										// print_r($cover_value);
										$last_service_limit_value = end(array_keys($service_value));
										$service_limit_table = "";
										$service_limit_1 = "";
										$service_limit_2 = "";
										$service_limit_3 = "";
										if ($fundArrCount == 3) {
											$cover_value_0 = $fundDetailsArr['extras_cover']['covered'][$service_key][0];
											if ($cover_value_0=="Covered")
												$cover_value_0 = '<table class="img_table col-lg-2 col-md-2 col-sm-2 col-xs-2"><tr><td><img class="img_per_class" src="images/tick_small.png"/></td></tr></table>';
											else
												$cover_value_0 = '<table class="img_table col-lg-2 col-md-2 col-sm-2 col-xs-2"><tr><td><img class="img_per_class" src="images/cross_small.png" /></td></tr></table>';

											$cover_value_1 = $fundDetailsArr['extras_cover']['covered'][$service_key][1];
											if ($cover_value_1=="Covered")
												$cover_value_1 = '<table class="img_table col-lg-2 col-md-2 col-sm-2 col-xs-2"><tr><td><img class="img_per_class" src="images/tick_small.png"/></td></tr></table>';
											else
												$cover_value_1 = '<table class="img_table col-lg-2 col-md-2 col-sm-2 col-xs-2"><tr><td><img class="img_per_class" src="images/cross_small.png" /></td></tr></table>';

											$cover_value_2 = $fundDetailsArr['extras_cover']['covered'][$service_key][2];
											if ($cover_value_2=="Covered")
												$cover_value_2 = '<table class="img_table col-lg-2 col-md-2 col-sm-2 col-xs-2"><tr><td><img class="img_per_class" src="images/tick_small.png"/></td></tr></table>';
											else
												$cover_value_2 = '<table class="img_table col-lg-2 col-md-2 col-sm-2 col-xs-2"><tr><td><img class="img_per_class" src="images/cross_small.png" /></td></tr></table>';
										
											$service_limit_1 = $cover_value_0.'<table class="service_content col-lg-10 col-md-10 col-sm-10 col-xs-10">';
											$service_limit_2 = $cover_value_1.'<table class="service_content col-lg-10 col-md-10 col-sm-10 col-xs-10">';
											$service_limit_3 = $cover_value_2.'<table class="service_content col-lg-10 col-md-10 col-sm-10 col-xs-10">';
											foreach ($service_value as $service_limit_key => $service_limit_value) {
												$service_limit_1 .= '<tr><td align="left">'.$service_limit_key.'</td><td align="left">'.$service_limit_value[0].'</td></tr>';
												$service_limit_2 .= '<tr><td align="left">'.$service_limit_key.'</td><td align="left">'.$service_limit_value[1].'</td></tr>';
												$service_limit_3 .= '<tr><td align="left">'.$service_limit_key.'</td><td align="left">'.$service_limit_value[2].'</td></tr>';
											}
											$service_limit_1 .= '</table>';
											$service_limit_2 .= '</table>';
											$service_limit_3 .= '</table>';
											echo '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom_right">'.$service_limit_1.'</div>';
											echo '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom_right">'.$service_limit_2.'</div>';
											echo '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 service_extra_headers_content bor_bottom">'.$service_limit_3.'</div>';
										} else if ($fundArrCount == 2) {
											$cover_value_0 = $fundDetailsArr['extras_cover']['covered'][$service_key][0];
											if ($cover_value_0=="Covered")
												$cover_value_0 = '<table class="img_table col-lg-2 col-md-2 col-sm-2 col-xs-2"><tr><td><img class="img_per_class" src="images/tick_small.png"/></td></tr></table>';
											else
												$cover_value_0 = '<table class="img_table col-lg-2 col-md-2 col-sm-2 col-xs-2"><tr><td><img class="img_per_class" src="images/cross_small.png" /></td></tr></table>';

											$cover_value_1 = $fundDetailsArr['extras_cover']['covered'][$service_key][1];
											if ($cover_value_1=="Covered")
												$cover_value_1 = '<table class="img_table col-lg-2 col-md-2 col-sm-2 col-xs-2"><tr><td><img class="img_per_class" src="images/tick_small.png"/></td></tr></table>';
											else
												$cover_value_1 = '<table class="img_table col-lg-2 col-md-2 col-sm-2 col-xs-2"><tr><td><img class="img_per_class" src="images/cross_small.png" /></td></tr></table>';

											$service_limit_1 = '<table class="service_content col-lg-10 col-md-10 col-sm-10 col-xs-10">';
											$service_limit_2 = '<table class="service_content col-lg-10 col-md-10 col-sm-10 col-xs-10">';
											foreach ($service_value as $service_limit_key => $service_limit_value) {
												$service_limit_1 .= '<tr><td align="left">'.$service_limit_key.'</td><td align="left">'.$service_limit_value[0].'</td></tr>';
												$service_limit_2 .= '<tr><td align="left">'.$service_limit_key.'</td><td align="left">'.$service_limit_value[1].'</td></tr>';
											}
											$service_limit_1 .= '</table>';
											$service_limit_2 .= '</table>';
											echo '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 service_extra_headers_content bor_bottom_right">'.$cover_value_0.$service_limit_1.'</div>';
											echo '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 service_extra_headers_content bor_bottom">'.$cover_value_1.$service_limit_2.'</div>';
										} 
										else {
											$cover_value_0 = $fundDetailsArr['extras_cover']['covered'][$service_key][0];
											if ($cover_value_0=="Covered")
												$cover_value_0 = '<table class="img_table col-lg-2 col-md-2 col-sm-2 col-xs-2"><tr><td><img class="img_per_class" src="images/tick_small.png"/></td></tr></table>';
											else
												$cover_value_0 = '<table class="img_table col-lg-2 col-md-2 col-sm-2 col-xs-2"><tr><td><img class="img_per_class" src="images/cross_small.png" /></td></tr></table>';

											$service_limit_table = $cover_value_0.'<table class="service_content col-lg-10 col-md-10 col-sm-10 col-xs-10">';
											foreach ($service_value as $service_limit_key => $service_limit_value) {
												$service_limit_table .= '<tr><td align="left">'.$service_limit_key.'</td><td align="center">'.$service_limit_value.'</td></tr>';
											}
											$service_limit_table .= '</table>';
											echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 service_extra_headers_content bor_bottom">'.$service_limit_table.'</div>';
										}
										echo '</div></div>';
									}
								?>
								</div>
							</div>
						<?php } ?>
					</div>
					<div class="visible-xs results_main" id="dynamic_results">
						<?php if ($fundArrCount == 3) { ?>
							<div class="col-xs-12 rslt_elements" data-attr="1" id="rslt_element_1">
								<div class="col-xs-12 result_section fixed_head">
									<div class="col-xs-12 rslt_section">
										<div class="col-xs-12 result_section fund_section">
										<?php
											$fund_provider_value = $fundDetailsArr['fund_provider'][0];
											$fund_provider_type = $fundDetailsArr['fund_type'][0];
											$fund_policy_url = $fundDetailsArr['policy_url'][0]; 
											echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_header"><div class="col-xs-1"></div><div class="col-xs-10"><h4 class="bank_name"><a target="_blank" href="'.$fund_policy_url.'">'.$fund_provider_value.'</a></h4><p class="fund_type">'.$fund_provider_type.'</p></div><div class="right_arrow col-xs-1"></div></div>';
										?>
										</div>
									</div>
									<div class="col-xs-12 rslt_section">
										<div class="col-xs-6 header_section">
											<div class="col-xs-12 premium_headers premium head bor_bottom">Monthly premium<br />(after 25.657% rebate)</div>
										</div>
										<div class="col-xs-6 result_section fund_section">
										<?php
											$monthly_premium_value = $fundDetailsArr['monthly_premium'][0];
											echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 premium_headers bor_bottom"><h4 class="policy_price">'.$monthly_premium_value.'</h4></div>';
										?>
										</div>
									</div>
								</div>
								<div class="col-xs-12 result_section">	
									<div class="col-xs-12 rslt_section">
										<div class="col-xs-6 header_section">
											<div class="col-xs-12 policy_headers head bor_bottom">Policy name</div>
										</div>
										<div class="col-xs-6 result_section fund_section">
										<?php
											$policy_name_value = $fundDetailsArr['policy_name'][0];
											echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 policy_headers bor_bottom"><h4 class="policy_name">'.$policy_name_value.'</h4></div>';
										?>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
										<div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section">
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 visit_headers head bor_bottom">Excess per visit</div>
										</div>
										<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">
										<?php
											$per_visit_value = $fundDetailsArr['per_visit'][0];
											echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 visit_headers bor_bottom"><h4 class="visit_price">'.$per_visit_value.'</h4></div>';
										?>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
										<div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section">
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payment_headers head bor_bottom">Co-payment</div>
										</div>
										<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">
										<?php
											$co_payment_value = $fundDetailsArr['co_payment'][0];
											echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payment_headers bor_bottom"><h4 class="payment">'.$co_payment_value.'</h4></div>';
										?>
										</div>
									</div>
								</div>
								<?php if ($type_cover=="Hospital Only" || $type_cover=="Hospital + Extras") { ?>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_header">HOSPITAL COVER</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 result_section">
										<?php 
											$ArrHospitalOptionsArr = array("pregnancy" =>"Pregnancy","fertility_treatment" =>"Fertility treatment (IVF)","heart_surgery" =>"Heart surgery (cardiac)","eye_surgery" =>"Eye surgery (cataract)","joints" =>"Joints (hip, knee, shoulder","renal_dialysis" =>"Renal dialysis (kidney)","weight_loss" =>"Weight loss (gastric)","sterilisation" =>"Sterilisation","plastic_surgery" =>"Plastic surgery","rehab_treatment" =>"Rehab treatment","psychatric" =>"Psychatric","palliative_care" =>"Palliative care");
											foreach ($resultArr['funds'][0]['hospital_cover'] as $hospital_cover_key => $hospital_cover_value) {
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section"><div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section "><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">'.$ArrHospitalOptionsArr[$hospital_cover_key].'</div></div>';
												echo '<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">';
												$cover_value = $hospital_cover_value;
												if ($cover_value=="Covered"){
													$cover_value = '<img src="images/tick.png">';
												} else if($cover_value=="Not Covered" || $cover_value=="n&#47a"){

													$cover_value = '<img src="images/cross.png">';
												} else{
													$cover_value = $cover_value;
												}
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers bor_bottom"><h4 class="payment">'.$cover_value.'</h4></div>';
												echo '</div></div>';
											}
										?>
									</div>
								<?php } 
								if ($type_cover=="Extras Only" || $type_cover=="Hospital + Extras") { ?>
									<div class="col-xs-12 section_header extra_header_main">
										<div class="col-xs-12 extra_header">EXTRAS COVER</div>
										<div class="col-xs-offset-3 col-xs-6">
											<select name="select_div" class="select_div result_header_btn filter_btn_1 more_btn">
												<!-- <option selected="selected" value="Covered">Covered</option> -->
												<option value="Annual limit">Annual limit</option>
												<option value="Per service limit">Per service limit</option>
											</select>
										</div>
									</div>
									<!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 header_section extra_section covered_section">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 result_section">
											<?php 
												$ArrextrasOptionsArr = array("dental_cleaning" =>"Dental (cleaning, fillings)","dental_major" =>"Dental major (crowns)","dental_root" =>"Dental (root canal)","dental_braces" =>"Dental (braces)","optical" =>"Optical","physio" =>"Physio","chiro" =>"Chiro","massages" =>"Massages","acupunture" =>"Acupunture","hearing_aids" =>"Hearing aids","blood_glucose_monitors" =>"Blood glucose monitors","pharmaceutical" =>"Pharmaceutical (non PBS)","naturopathy" =>"Naturopathy","podiatry" =>"Podiatry (feet)","psychology" =>"Psychology");
												foreach ($resultArr['funds'][0]['extras_cover']['covered'] as $extras_cover_key => $extras_cover_value) {
													echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section"><div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section "><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">'.$ArrextrasOptionsArr[$extras_cover_key].'</div></div>';
													echo '<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">';
													$extras_cover_value = $extras_cover_value;
													if ($extras_cover_value=="Covered"){
														$extras_cover_value = '<h4 class="payment_img"><img src="images/tick.png"></h4>';
													} else if($extras_cover_value=="Not Covered" || $extras_cover_value=="n&#47a"){
														$extras_cover_value = '<h4 class="payment_img"><img src="images/cross.png"></h4>';
													} else{
														$extras_cover_value = '<h4 class="cover_payment_text">'.$extras_cover_value.'</h4>';
													}
													echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cover_extra_headers_content bor_bottom">'.$extras_cover_value.'</div>';
													echo '</div></div>';
												}
											?>
										</div>
									</div> -->
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 header_section extra_section annual_limit_section">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 result_section">
										<?php
											foreach ($resultArr['funds'][0]['extras_cover']['annual_limit'] as $annual_limit_key => $annual_limit_value) {
												$cover_value=$resultArr['funds'][0]['extras_cover']['covered'][$annual_limit_key];
												if ($cover_value=="Covered"){
													$cover_value = '<span class="tick"></span>';
												} else {
													$cover_value = '<span class="cross"></span>';
												}
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section"><div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section "><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">'.$ArrextrasOptionsArr[$annual_limit_key].'</div></div>';
												echo '<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">';
												$annual_limit_value = $annual_limit_value;
												if ($annual_limit_value=="Covered"){
													$annual_limit_value = '<table class="payment_img"><tr><td><img src="images/tick.png"></td></tr></table>';
												} else if($annual_limit_value=="Not Covered"){
													$annual_limit_value = '<table class="payment_img"><tr><td><img src="images/cross.png"></td></tr></table>';
												} else{
													$annual_limit_value = '<table class="annual_content"><tr><td>'.$cover_value.$annual_limit_value.'</td></tr></table>';
												}
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 annual_extra_headers_content bor_bottom">'.$annual_limit_value.'</div>';
												echo '</div></div>';
											}
										?>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 header_section extra_section service_limit_section">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 result_section">
										<?php
											foreach ($resultArr['funds'][0]['extras_cover']['service_limit'] as $service_limit_key => $service_limit_value) {
												$cover_value = $resultArr['funds'][0]['extras_cover']['service_limit'];
												if ($cover_value=="Covered")
													$cover_value = '<table class="img_table col-lg-2 col-md-2 col-sm-2 col-xs-2"><tr><td><img class="img_per_class" src="images/tick_small.png"/></td></tr></table>';
												else
													$cover_value = '<table class="img_table col-lg-2 col-md-2 col-sm-2 col-xs-2"><tr><td><img class="img_per_class" src="images/cross_small.png" /></td></tr></table>';
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section"><div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section "><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">'.$ArrextrasOptionsArr[$service_limit_key].'</div></div>';
												echo '<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">';
												$last_service_limit_value = end(array_keys($service_limit_value));
												$service_limit_table = "";
												$service_limit_table = $cover_value.'<table class="col-lg-8 col-md-8 col-sm-8 col-xs-8 service_content">';
												foreach ($service_limit_value as $service_limit_key => $service_limit_value) {
													$service_limit_table .= '<tr><td align="left">'.$service_limit_key.'</td><td align="center">'.$service_limit_value.'</td></tr>';
												}
												$service_limit_table .= '</table>';
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 service_extra_headers_content bor_bottom">'.$service_limit_table.'</div>';
												echo '</div></div>';
											}
										?>
										</div>
									</div>
								<?php } ?>
							</div>
							<div class="col-xs-12 rslt_elements" data-attr="2" id="rslt_element_2">
								<div class="col-xs-12 result_section fixed_head">
									<div class="col-xs-12 rslt_section">
										<div class="col-xs-12 result_section fund_section">
										<?php
											$fund_provider_value = $fundDetailsArr['fund_provider'][1];
											$fund_provider_type = $fundDetailsArr['fund_type'][1]; 
											$fund_policy_url = $fundDetailsArr['policy_url'][1]; 
											echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_header"><div class="left_arrow col-xs-1"></div><div class="col-xs-10"><h4 class="bank_name"><a target="_blank" href="'.$fund_policy_url.'">'.$fund_provider_value.'</a></h4><p class="fund_type">'.$fund_provider_type.'</p></div><div class="right_arrow col-xs-1"></div></div>';
										?>
										</div>
									</div>
									<div class="col-xs-12 rslt_section">
										<div class="col-xs-6 header_section">
											<div class="col-xs-12 premium_headers premium head bor_bottom">Monthly premium<br>(after 25.657% rebate)</div>
										</div>
										<div class="col-xs-6 result_section fund_section">
										<?php
											$monthly_premium_value = $fundDetailsArr['monthly_premium'][1];
											echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 premium_headers bor_bottom"><h4 class="policy_price">'.$monthly_premium_value.'</h4></div>';
										?>
										</div>
									</div>
								</div>
								<div class="col-xs-12 result_section">	
									<div class="col-xs-12 rslt_section">
										<div class="col-xs-6 header_section">
											<div class="col-xs-12 policy_headers head bor_bottom">Policy name</div>
										</div>
										<div class="col-xs-6 result_section fund_section">
										<?php
											$policy_name_value = $fundDetailsArr['policy_name'][1];
											echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 policy_headers bor_bottom"><h4 class="policy_name">'.$policy_name_value.'</h4></div>';
										?>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
										<div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section">
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 visit_headers head bor_bottom">Excess per visit</div>
										</div>
										<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">
										<?php
											$per_visit_value = $fundDetailsArr['per_visit'][1];
											echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 visit_headers bor_bottom"><h4 class="visit_price">'.$per_visit_value.'</h4></div>';
										?>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
										<div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section">
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payment_headers head bor_bottom">Co-payment</div>
										</div>
										<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">
										<?php
											$co_payment_value = $fundDetailsArr['co_payment'][1];
											echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payment_headers bor_bottom"><h4 class="payment">'.$co_payment_value.'</h4></div>';
										?>
										</div>
									</div>
								</div>
								<?php if ($type_cover=="Hospital Only" || $type_cover=="Hospital + Extras") { ?>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_header">HOSPITAL COVER</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 result_section">
										<?php 
											$ArrHospitalOptionsArr = array("pregnancy" =>"Pregnancy","fertility_treatment" =>"Fertility treatment (IVF)","heart_surgery" =>"Heart surgery (cardiac)","eye_surgery" =>"Eye surgery (cataract)","joints" =>"Joints (hip, knee, shoulder","renal_dialysis" =>"Renal dialysis (kidney)","weight_loss" =>"Weight loss (gastric)","sterilisation" =>"Sterilisation","plastic_surgery" =>"Plastic surgery","rehab_treatment" =>"Rehab treatment","psychatric" =>"Psychatric","palliative_care" =>"Palliative care");
											foreach ($resultArr['funds'][1]['hospital_cover'] as $hospital_cover_key => $hospital_cover_value) {
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section"><div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section "><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">'.$ArrHospitalOptionsArr[$hospital_cover_key].'</div></div>';
												echo '<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">';
												$cover_value = $hospital_cover_value;
												if ($cover_value=="Covered"){
													$cover_value = '<img src="images/tick.png">';
												} else if($cover_value=="Not Covered" || $cover_value=="n&#47a"){

													$cover_value = '<img src="images/cross.png">';
												} else{
													$cover_value = $cover_value;
												}
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers bor_bottom"><h4 class="payment">'.$cover_value.'</h4></div>';
												echo '</div></div>';
											}
										?>
									</div>
								<?php } 
								if ($type_cover=="Extras Only" || $type_cover=="Hospital + Extras") { ?>
									<div class="col-xs-12 section_header extra_header_main">
										<div class="col-xs-12 extra_header">EXTRAS COVER</div>
										<div class="col-xs-offset-3 col-xs-6">
											<select name="select_div" class="select_div result_header_btn filter_btn_1 more_btn">
												<!-- <option value="Covered">Covered</option> -->
												<option value="Annual limit">Annual limit</option>
												<option value="Per service limit">Per service limit</option>
											</select>
										</div>
									</div>
									<!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 header_section extra_section covered_section">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 result_section">
											<?php 
												$ArrextrasOptionsArr = array("dental_cleaning" =>"Dental (cleaning, fillings)","dental_major" =>"Dental major (crowns)","dental_root" =>"Dental (root canal)","dental_braces" =>"Dental (braces)","optical" =>"Optical","physio" =>"Physio","chiro" =>"Chiro","massages" =>"Massages","acupunture" =>"Acupunture","hearing_aids" =>"Hearing aids","blood_glucose_monitors" =>"Blood glucose monitors","pharmaceutical" =>"Pharmaceutical (non PBS)","naturopathy" =>"Naturopathy","podiatry" =>"Podiatry (feet)","psychology" =>"Psychology");
												foreach ($resultArr['funds'][1]['extras_cover']['covered'] as $extras_cover_key => $extras_cover_value) {
													echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section"><div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section "><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">'.$ArrextrasOptionsArr[$extras_cover_key].'</div></div>';
													echo '<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">';
													$extras_cover_value = $extras_cover_value;
													if ($extras_cover_value=="Covered"){
														$extras_cover_value = '<h4 class="payment_img"><img src="images/tick.png"></h4>';
													} else if($extras_cover_value=="Not Covered" || $extras_cover_value=="n&#47a"){
														$extras_cover_value = '<h4 class="payment_img"><img src="images/cross.png"></h4>';
													} else{
														$extras_cover_value = '<h4 class="cover_payment_text">'.$extras_cover_value.'</h4>';
													}
													echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cover_extra_headers_content bor_bottom">'.$extras_cover_value.'</div>';
													echo '</div></div>';
												}
											?>
										</div>
									</div> -->
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 header_section extra_section annual_limit_section">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 result_section">
										<?php
											foreach ($resultArr['funds'][1]['extras_cover']['annual_limit'] as $annual_limit_key => $annual_limit_value) {
												$cover_value=$resultArr['funds'][1]['extras_cover']['covered'][$annual_limit_key];
												if ($cover_value=="Covered"){
													$cover_value = '<span class="tick"></span>';
												} else {
													$cover_value = '<span class="cross"></span>';
												}
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section"><div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section "><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">'.$ArrextrasOptionsArr[$annual_limit_key].'</div></div>';
												echo '<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">';
												$annual_limit_value = $annual_limit_value;
												if ($annual_limit_value=="Covered"){
													$annual_limit_value = '<table class="payment_img"><tr><td><img src="images/tick.png"></td></tr></table>';
												} else if($annual_limit_value=="Not Covered"){
													$annual_limit_value = '<table class="payment_img"><tr><td><img src="images/cross.png"></td></tr></table>';
												} else{
													$annual_limit_value = '<table class="annual_content"><tr><td>'.$cover_value.$annual_limit_value.'</td></tr></table>';
												}
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 annual_extra_headers_content bor_bottom">'.$annual_limit_value.'</div>';
												echo '</div></div>';
											}
										?>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 header_section extra_section service_limit_section">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 result_section">
										<?php
											foreach ($resultArr['funds'][1]['extras_cover']['service_limit'] as $service_limit_key => $service_limit_value) {
												$cover_value = $resultArr['funds'][1]['extras_cover']['service_limit'];
												if ($cover_value=="Covered")
													$cover_value = '<table class="img_table col-lg-2 col-md-2 col-sm-2 col-xs-2"><tr><td><img class="img_per_class" src="images/tick_small.png"/></td></tr></table>';
												else
													$cover_value = '<table class="img_table col-lg-2 col-md-2 col-sm-2 col-xs-2"><tr><td><img class="img_per_class" src="images/cross_small.png" /></td></tr></table>';
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section"><div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section "><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">'.$ArrextrasOptionsArr[$service_limit_key].'</div></div>';
												echo '<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">';
												$last_service_limit_value = end(array_keys($service_limit_value));
												$service_limit_table = "";
												$service_limit_table = $cover_value.'<table class="col-lg-8 col-md-8 col-sm-8 col-xs-8 service_content">';
												foreach ($service_limit_value as $service_limit_key => $service_limit_value) {
													$service_limit_table .= '<tr><td align="left">'.$service_limit_key.'</td><td align="center">'.$service_limit_value.'</td></tr>';
												}
												$service_limit_table .= '</table>';
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 service_extra_headers_content bor_bottom">'.$service_limit_table.'</div>';
												echo '</div></div>';
											}
										?>
										</div>
									</div>
								<?php } ?>
							</div>
							<div class="col-xs-12 rslt_elements" data-attr="3" id="rslt_element_3">
								<div class="col-xs-12 result_section fixed_head">
									<div class="col-xs-12 rslt_section">
										<div class="col-xs-12 result_section fund_section">
										<?php
											$fund_provider_value = $fundDetailsArr['fund_provider'][2];
											$fund_provider_type = $fundDetailsArr['fund_type'][2]; 
											$fund_policy_url = $fundDetailsArr['policy_url'][2]; 
											echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_header"><div class="left_arrow col-xs-1"></div><div class="col-xs-10"><h4 class="bank_name"><a target="_blank" href="'.$fund_policy_url.'">'.$fund_provider_value.'</a></h4><p class="fund_type">'.$fund_provider_type.'</p></div><div class="col-xs-1"></div></div>';
										?>
										</div>
									</div>
									<div class="col-xs-12 rslt_section">
										<div class="col-xs-6 header_section">
											<div class="col-xs-12 premium_headers premium head bor_bottom">Monthly premium<br>(after 25.657% rebate)</div>
										</div>
										<div class="col-xs-6 result_section fund_section">
										<?php
											$monthly_premium_value = $fundDetailsArr['monthly_premium'][2];
											echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 premium_headers bor_bottom"><h4 class="policy_price">'.$monthly_premium_value.'</h4></div>';
										?>
										</div>
									</div>
								</div>
								<div class="col-xs-12 result_section">	
									<div class="col-xs-12 rslt_section">
										<div class="col-xs-6 header_section">
											<div class="col-xs-12 policy_headers head bor_bottom">Policy name</div>
										</div>
										<div class="col-xs-6 result_section fund_section">
										<?php
											$policy_name_value = $fundDetailsArr['policy_name'][2];
											echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 policy_headers bor_bottom"><h4 class="policy_name">'.$policy_name_value.'</h4></div>';
										?>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
										<div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section">
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 visit_headers head bor_bottom">Excess per visit</div>
										</div>
										<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">
										<?php
											$per_visit_value = $fundDetailsArr['per_visit'][2];
											echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 visit_headers bor_bottom"><h4 class="visit_price">'.$per_visit_value.'</h4></div>';
										?>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
										<div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section">
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payment_headers head bor_bottom">Co-payment</div>
										</div>
										<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">
										<?php
											$co_payment_value = $fundDetailsArr['co_payment'][2];
											echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payment_headers bor_bottom"><h4 class="payment">'.$co_payment_value.'</h4></div>';
										?>
										</div>
									</div>
								</div>
								<?php if ($type_cover=="Hospital Only" || $type_cover=="Hospital + Extras") { ?>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_header">HOSPITAL COVER</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 result_section">
										<?php 
											$ArrHospitalOptionsArr = array("pregnancy" =>"Pregnancy","fertility_treatment" =>"Fertility treatment (IVF)","heart_surgery" =>"Heart surgery (cardiac)","eye_surgery" =>"Eye surgery (cataract)","joints" =>"Joints (hip, knee, shoulder","renal_dialysis" =>"Renal dialysis (kidney)","weight_loss" =>"Weight loss (gastric)","sterilisation" =>"Sterilisation","plastic_surgery" =>"Plastic surgery","rehab_treatment" =>"Rehab treatment","psychatric" =>"Psychatric","palliative_care" =>"Palliative care");
											foreach ($resultArr['funds'][2]['hospital_cover'] as $hospital_cover_key => $hospital_cover_value) {
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section"><div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section "><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">'.$ArrHospitalOptionsArr[$hospital_cover_key].'</div></div>';
												echo '<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">';
												$cover_value = $hospital_cover_value;
												if ($cover_value=="Covered"){
													$cover_value = '<img src="images/tick.png">';
												} else if($cover_value=="Not Covered" || $cover_value=="n&#47a"){

													$cover_value = '<img src="images/cross.png">';
												} else{
													$cover_value = $cover_value;
												}
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers bor_bottom"><h4 class="payment">'.$cover_value.'</h4></div>';
												echo '</div></div>';
											}
										?>
									</div>
								<?php } 
								if ($type_cover=="Extras Only" || $type_cover=="Hospital + Extras") { ?>
									<div class="col-xs-12 section_header extra_header_main">
										<div class="col-xs-12 extra_header">EXTRAS COVER</div>
										<div class="col-xs-offset-3 col-xs-6">
											<select name="select_div" class="select_div result_header_btn filter_btn_1 more_btn">
												<!-- <option value="Covered">Covered</option> -->
												<option value="Annual limit">Annual limit</option>
												<option value="Per service limit">Per service limit</option>
											</select>
										</div>
									</div>
									<!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 header_section extra_section covered_section">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 result_section">
											<?php 
												$ArrextrasOptionsArr = array("dental_cleaning" =>"Dental (cleaning, fillings)","dental_major" =>"Dental major (crowns)","dental_root" =>"Dental (root canal)","dental_braces" =>"Dental (braces)","optical" =>"Optical","physio" =>"Physio","chiro" =>"Chiro","massages" =>"Massages","acupunture" =>"Acupunture","hearing_aids" =>"Hearing aids","blood_glucose_monitors" =>"Blood glucose monitors","pharmaceutical" =>"Pharmaceutical (non PBS)","naturopathy" =>"Naturopathy","podiatry" =>"Podiatry (feet)","psychology" =>"Psychology");
												foreach ($resultArr['funds'][2]['extras_cover']['covered'] as $extras_cover_key => $extras_cover_value) {
													echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section"><div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section "><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">'.$ArrextrasOptionsArr[$extras_cover_key].'</div></div>';
													echo '<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">';
													$extras_cover_value = $extras_cover_value;
													if ($extras_cover_value=="Covered"){
														$extras_cover_value = '<h4 class="payment_img"><img src="images/tick.png"></h4>';
													} else if($extras_cover_value=="Not Covered" || $extras_cover_value=="n&#47a"){
														$extras_cover_value = '<h4 class="payment_img"><img src="images/cross.png"></h4>';
													} else{
														$extras_cover_value = '<h4 class="cover_payment_text">'.$extras_cover_value.'</h4>';
													}
													echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cover_extra_headers_content bor_bottom">'.$extras_cover_value.'</div>';
													echo '</div></div>';
												}
											?>
										</div>
									</div> -->
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 header_section extra_section annual_limit_section">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 result_section">
										<?php
											foreach ($resultArr['funds'][2]['extras_cover']['annual_limit'] as $annual_limit_key => $annual_limit_value) {
												$cover_value=$resultArr['funds'][2]['extras_cover']['covered'][$annual_limit_key];
												if ($cover_value=="Covered"){
													$cover_value = '<span class="tick"></span>';
												} else {
													$cover_value = '<span class="cross"></span>';
												}
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section"><div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section "><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">'.$ArrextrasOptionsArr[$annual_limit_key].'</div></div>';
												echo '<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">';
												$annual_limit_value = $annual_limit_value;
												if ($annual_limit_value=="Covered"){
													$annual_limit_value = '<table class="payment_img"><tr><td><img src="images/tick.png"></td></tr></table>';
												} else if($annual_limit_value=="Not Covered"){
													$annual_limit_value = '<table class="payment_img"><tr><td><img src="images/cross.png"></td></tr></table>';
												} else{
													$annual_limit_value = '<table class="annual_content"><tr><td>'.$cover_value.$annual_limit_value.'</td></tr></table>';
												}
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 annual_extra_headers_content bor_bottom">'.$annual_limit_value.'</div>';
												echo '</div></div>';
											}
										?>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 header_section extra_section service_limit_section">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 result_section">
										<?php
											foreach ($resultArr['funds'][2]['extras_cover']['service_limit'] as $service_limit_key => $service_limit_value) {
												$cover_value = $resultArr['funds'][2]['extras_cover']['service_limit'];
												if ($cover_value=="Covered")
													$cover_value = '<table class="img_table col-lg-2 col-md-2 col-sm-2 col-xs-2"><tr><td><img class="img_per_class" src="images/tick_small.png"/></td></tr></table>';
												else
													$cover_value = '<table class="img_table col-lg-2 col-md-2 col-sm-2 col-xs-2"><tr><td><img class="img_per_class" src="images/cross_small.png" /></td></tr></table>';
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section"><div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section "><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">'.$ArrextrasOptionsArr[$service_limit_key].'</div></div>';
												echo '<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">';
												$last_service_limit_value = end(array_keys($service_limit_value));
												$service_limit_table = "";
												$service_limit_table = $cover_value.'<table class="col-lg-8 col-md-8 col-sm-8 col-xs-8 service_content">';
												foreach ($service_limit_value as $service_limit_key => $service_limit_value) {
													$service_limit_table .= '<tr><td align="left">'.$service_limit_key.'</td><td align="center">'.$service_limit_value.'</td></tr>';
												}
												$service_limit_table .= '</table>';
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 service_extra_headers_content bor_bottom">'.$service_limit_table.'</div>';
												echo '</div></div>';
											}
										?>
										</div>
									</div>
								<?php } ?>
							</div>
						<?php } else if($fundArrCount == 2) { ?>
							<div class="col-xs-12 rslt_elements" data-attr="1" id="rslt_element_1">
								<div class="col-xs-12 result_section fixed_head">
									<div class="col-xs-12 rslt_section">
										<div class="col-xs-12 result_section fund_section">
										<?php
											$fund_provider_value = $fundDetailsArr['fund_provider'][0];
											$fund_provider_type = $fundDetailsArr['fund_type'][0]; 
											$fund_policy_url = $fundDetailsArr['policy_url'][0]; 
											echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_header"><div class="col-xs-1"></div><div class="col-xs-10"><h4 class="bank_name"><a target="_blank" href="'.$fund_policy_url.'">'.$fund_provider_value.'</a></h4><p class="fund_type">'.$fund_provider_type.'</p></div><div class="right_arrow col-xs-1"></div></div>';
										?>
										</div>
									</div>
									<div class="col-xs-12 rslt_section">
										<div class="col-xs-6 header_section">
											<div class="col-xs-12 premium_headers premium head bor_bottom">Monthly premium<br>(after 25.657% rebate)</div>
										</div>
										<div class="col-xs-6 result_section fund_section">
										<?php
											$monthly_premium_value = $fundDetailsArr['monthly_premium'][0];
											echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 premium_headers bor_bottom"><h4 class="policy_price">'.$monthly_premium_value.'</h4></div>';
										?>
										</div>
									</div>
								</div>
								<div class="col-xs-12 result_section">	
									<div class="col-xs-12 rslt_section">
										<div class="col-xs-6 header_section">
											<div class="col-xs-12 policy_headers head bor_bottom">Policy name</div>
										</div>
										<div class="col-xs-6 result_section fund_section">
										<?php
											$policy_name_value = $fundDetailsArr['policy_name'][0];
											echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 policy_headers bor_bottom"><h4 class="policy_name">'.$policy_name_value.'</h4></div>';
										?>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
										<div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section">
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 visit_headers head bor_bottom">Excess per visit</div>
										</div>
										<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">
										<?php
											$per_visit_value = $fundDetailsArr['per_visit'][0];
											echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 visit_headers bor_bottom"><h4 class="visit_price">'.$per_visit_value.'</h4></div>';
										?>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
										<div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section">
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payment_headers head bor_bottom">Co-payment</div>
										</div>
										<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">
										<?php
											$co_payment_value = $fundDetailsArr['co_payment'][0];
											echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payment_headers bor_bottom"><h4 class="payment">'.$co_payment_value.'</h4></div>';
										?>
										</div>
									</div>
								</div>
								<?php if ($type_cover=="Hospital Only" || $type_cover=="Hospital + Extras") { ?>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_header">HOSPITAL COVER</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 result_section">
										<?php 
											$ArrHospitalOptionsArr = array("pregnancy" =>"Pregnancy","fertility_treatment" =>"Fertility treatment (IVF)","heart_surgery" =>"Heart surgery (cardiac)","eye_surgery" =>"Eye surgery (cataract)","joints" =>"Joints (hip, knee, shoulder","renal_dialysis" =>"Renal dialysis (kidney)","weight_loss" =>"Weight loss (gastric)","sterilisation" =>"Sterilisation","plastic_surgery" =>"Plastic surgery","rehab_treatment" =>"Rehab treatment","psychatric" =>"Psychatric","palliative_care" =>"Palliative care");
											foreach ($resultArr['funds'][0]['hospital_cover'] as $hospital_cover_key => $hospital_cover_value) {
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section"><div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section "><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">'.$ArrHospitalOptionsArr[$hospital_cover_key].'</div></div>';
												echo '<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">';
												$cover_value = $hospital_cover_value;
												if ($cover_value=="Covered"){
													$cover_value = '<img src="images/tick.png">';
												} else if($cover_value=="Not Covered" || $cover_value=="n&#47a"){

													$cover_value = '<img src="images/cross.png">';
												} else{
													$cover_value = $cover_value;
												}
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers bor_bottom"><h4 class="payment">'.$cover_value.'</h4></div>';
												echo '</div></div>';
											}
										?>
									</div>
								<?php } 
								if ($type_cover=="Extras Only" || $type_cover=="Hospital + Extras") { ?>
									<div class="col-xs-12 section_header extra_header_main">
										<div class="col-xs-12 extra_header">EXTRAS COVER</div>
										<div class="col-xs-offset-3 col-xs-6">
											<select name="select_div" class="select_div result_header_btn filter_btn_1 more_btn">
												<!-- <option selected="selected" value="Covered">Covered</option> -->
												<option value="Annual limit">Annual limit</option>
												<option value="Per service limit">Per service limit</option>
											</select>
										</div>
									</div>
									<!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 header_section extra_section covered_section">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 result_section">
											<?php 
												$ArrextrasOptionsArr = array("dental_cleaning" =>"Dental (cleaning, fillings)","dental_major" =>"Dental major (crowns)","dental_root" =>"Dental (root canal)","dental_braces" =>"Dental (braces)","optical" =>"Optical","physio" =>"Physio","chiro" =>"Chiro","massages" =>"Massages","acupunture" =>"Acupunture","hearing_aids" =>"Hearing aids","blood_glucose_monitors" =>"Blood glucose monitors","pharmaceutical" =>"Pharmaceutical (non PBS)","naturopathy" =>"Naturopathy","podiatry" =>"Podiatry (feet)","psychology" =>"Psychology");
												foreach ($resultArr['funds'][0]['extras_cover']['covered'] as $extras_cover_key => $extras_cover_value) {
													echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section"><div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section "><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">'.$ArrextrasOptionsArr[$extras_cover_key].'</div></div>';
													echo '<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">';
													$extras_cover_value = $extras_cover_value;
													if ($extras_cover_value=="Covered"){
														$extras_cover_value = '<h4 class="payment_img"><img src="images/tick.png"></h4>';
													} else if($extras_cover_value=="Not Covered" || $extras_cover_value=="n&#47a"){
														$extras_cover_value = '<h4 class="payment_img"><img src="images/cross.png"></h4>';
													} else{
														$extras_cover_value = '<h4 class="cover_payment_text">'.$extras_cover_value.'</h4>';
													}
													echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cover_extra_headers_content bor_bottom">'.$extras_cover_value.'</div>';
													echo '</div></div>';
												}
											?>
										</div>
									</div> -->
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 header_section extra_section annual_limit_section">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 result_section">
										<?php
											foreach ($resultArr['funds'][0]['extras_cover']['annual_limit'] as $annual_limit_key => $annual_limit_value) {
												$cover_value=$resultArr['funds'][0]['extras_cover']['covered'][$annual_limit_key];
												if ($cover_value=="Covered"){
													$cover_value = '<span class="tick"></span>';
												} else {
													$cover_value = '<span class="cross"></span>';
												}
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section"><div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section "><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">'.$ArrextrasOptionsArr[$annual_limit_key].'</div></div>';
												echo '<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">';
												$annual_limit_value = $annual_limit_value;
												if ($annual_limit_value=="Covered"){
													$annual_limit_value = '<table class="payment_img"><tr><td><img src="images/tick.png"></td></tr></table>';
												} else if($annual_limit_value=="Not Covered"){
													$annual_limit_value = '<table class="payment_img"><tr><td><img src="images/cross.png"></td></tr></table>';
												} else{
													$annual_limit_value = '<table class="annual_content"><tr><td>'.$cover_value.$annual_limit_value.'</td></tr></table>';
												}
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 annual_extra_headers_content bor_bottom">'.$annual_limit_value.'</div>';
												echo '</div></div>';
											}
										?>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 header_section extra_section service_limit_section">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 result_section">
										<?php
											foreach ($resultArr['funds'][0]['extras_cover']['service_limit'] as $service_limit_key => $service_limit_value) {
												$cover_value = $resultArr['funds'][0]['extras_cover']['service_limit'];
												if ($cover_value=="Covered")
													$cover_value = '<table class="img_table col-lg-2 col-md-2 col-sm-2 col-xs-2"><tr><td><img class="img_per_class" src="images/tick_small.png"/></td></tr></table>';
												else
													$cover_value = '<table class="img_table col-lg-2 col-md-2 col-sm-2 col-xs-2"><tr><td><img class="img_per_class" src="images/cross_small.png" /></td></tr></table>';
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section"><div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section "><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">'.$ArrextrasOptionsArr[$service_limit_key].'</div></div>';
												echo '<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">';
												$last_service_limit_value = end(array_keys($service_limit_value));
												$service_limit_table = "";
												$service_limit_table = $cover_value.'<table class="col-lg-8 col-md-8 col-sm-8 col-xs-8 service_content">';
												foreach ($service_limit_value as $service_limit_key => $service_limit_value) {
													$service_limit_table .= '<tr><td align="left">'.$service_limit_key.'</td><td align="center">'.$service_limit_value.'</td></tr>';
												}
												$service_limit_table .= '</table>';
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 service_extra_headers_content bor_bottom">'.$service_limit_table.'</div>';
												echo '</div></div>';
											}
										?>
										</div>
									</div>
								<?php } ?>
							</div>
							<div class="col-xs-12 rslt_elements" data-attr="2" id="rslt_element_2">
								<div class="col-xs-12 result_section fixed_head">
									<div class="col-xs-12 rslt_section">
										<div class="col-xs-12 result_section fund_section">
										<?php
											$fund_provider_value = $fundDetailsArr['fund_provider'][1];
											$fund_provider_type = $fundDetailsArr['fund_type'][1]; 
											$fund_policy_url = $fundDetailsArr['policy_url'][1]; 
											echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_header"><div class="left_arrow col-xs-1"></div><div class="col-xs-10"><h4 class="bank_name"><a target="_blank" href="'.$fund_policy_url.'">'.$fund_provider_value.'</a></h4><p class="fund_type">'.$fund_provider_type.'</p></div><div class="col-xs-1"></div></div>';
										?>
										</div>
									</div>
									<div class="col-xs-12 rslt_section">
										<div class="col-xs-6 header_section">
											<div class="col-xs-12 premium_headers premium head bor_bottom">Monthly premium<br>(after 25.657% rebate)</div>
										</div>
										<div class="col-xs-6 result_section fund_section">
										<?php
											$monthly_premium_value = $fundDetailsArr['monthly_premium'][1];
											echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 premium_headers bor_bottom"><h4 class="policy_price">'.$monthly_premium_value.'</h4></div>';
										?>
										</div>
									</div>
								</div>
								<div class="col-xs-12 result_section">	
									<div class="col-xs-12 rslt_section">
										<div class="col-xs-6 header_section">
											<div class="col-xs-12 policy_headers head bor_bottom">Policy name</div>
										</div>
										<div class="col-xs-6 result_section fund_section">
										<?php
											$policy_name_value = $fundDetailsArr['policy_name'][1];
											echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 policy_headers bor_bottom"><h4 class="policy_name">'.$policy_name_value.'</h4></div>';
										?>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
										<div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section">
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 visit_headers head bor_bottom">Excess per visit</div>
										</div>
										<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">
										<?php
											$per_visit_value = $fundDetailsArr['per_visit'][1];
											echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 visit_headers bor_bottom"><h4 class="visit_price">'.$per_visit_value.'</h4></div>';
										?>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
										<div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section">
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payment_headers head bor_bottom">Co-payment</div>
										</div>
										<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">
										<?php
											$co_payment_value = $fundDetailsArr['co_payment'][1];
											echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payment_headers bor_bottom"><h4 class="payment">'.$co_payment_value.'</h4></div>';
										?>
										</div>
									</div>
								</div>
								<?php if ($type_cover=="Hospital Only" || $type_cover=="Hospital + Extras") { ?>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_header">HOSPITAL COVER</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 result_section">
										<?php 
											$ArrHospitalOptionsArr = array("pregnancy" =>"Pregnancy","fertility_treatment" =>"Fertility treatment (IVF)","heart_surgery" =>"Heart surgery (cardiac)","eye_surgery" =>"Eye surgery (cataract)","joints" =>"Joints (hip, knee, shoulder","renal_dialysis" =>"Renal dialysis (kidney)","weight_loss" =>"Weight loss (gastric)","sterilisation" =>"Sterilisation","plastic_surgery" =>"Plastic surgery","rehab_treatment" =>"Rehab treatment","psychatric" =>"Psychatric","palliative_care" =>"Palliative care");
											foreach ($resultArr['funds'][1]['hospital_cover'] as $hospital_cover_key => $hospital_cover_value) {
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section"><div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section "><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">'.$ArrHospitalOptionsArr[$hospital_cover_key].'</div></div>';
												echo '<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">';
												$cover_value = $hospital_cover_value;
												if ($cover_value=="Covered"){
													$cover_value = '<img src="images/tick.png">';
												} else if($cover_value=="Not Covered" || $cover_value=="n&#47a"){

													$cover_value = '<img src="images/cross.png">';
												} else{
													$cover_value = $cover_value;
												}
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers bor_bottom"><h4 class="payment">'.$cover_value.'</h4></div>';
												echo '</div></div>';
											}
										?>
									</div>
								<?php } 
								if ($type_cover=="Extras Only" || $type_cover=="Hospital + Extras") { ?>
									<div class="col-xs-12 section_header extra_header_main">
										<div class="col-xs-12 extra_header">EXTRAS COVER</div>
										<div class="col-xs-offset-3 col-xs-6">
											<select name="select_div" class="select_div result_header_btn filter_btn_1 more_btn">
												<!-- <option value="Covered">Covered</option> -->
												<option value="Annual limit">Annual limit</option>
												<option value="Per service limit">Per service limit</option>
											</select>
										</div>
									</div>
									<!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 header_section extra_section covered_section">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 result_section">
											<?php 
												$ArrextrasOptionsArr = array("dental_cleaning" =>"Dental (cleaning, fillings)","dental_major" =>"Dental major (crowns)","dental_root" =>"Dental (root canal)","dental_braces" =>"Dental (braces)","optical" =>"Optical","physio" =>"Physio","chiro" =>"Chiro","massages" =>"Massages","acupunture" =>"Acupunture","hearing_aids" =>"Hearing aids","blood_glucose_monitors" =>"Blood glucose monitors","pharmaceutical" =>"Pharmaceutical (non PBS)","naturopathy" =>"Naturopathy","podiatry" =>"Podiatry (feet)","psychology" =>"Psychology");
												foreach ($resultArr['funds'][1]['extras_cover']['covered'] as $extras_cover_key => $extras_cover_value) {
													echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section"><div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section "><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">'.$ArrextrasOptionsArr[$extras_cover_key].'</div></div>';
													echo '<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">';
													$extras_cover_value = $extras_cover_value;
													if ($extras_cover_value=="Covered"){
														$extras_cover_value = '<h4 class="payment_img"><img src="images/tick.png"></h4>';
													} else if($extras_cover_value=="Not Covered" || $extras_cover_value=="n&#47a"){
														$extras_cover_value = '<h4 class="payment_img"><img src="images/cross.png"></h4>';
													} else{
														$extras_cover_value = '<h4 class="cover_payment_text">'.$extras_cover_value.'</h4>';
													}
													echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cover_extra_headers_content bor_bottom">'.$extras_cover_value.'</div>';
													echo '</div></div>';
												}
											?>
										</div>
									</div> -->
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 header_section extra_section annual_limit_section">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 result_section">
										<?php
											foreach ($resultArr['funds'][1]['extras_cover']['annual_limit'] as $annual_limit_key => $annual_limit_value) {
												$cover_value=$resultArr['funds'][1]['extras_cover']['covered'][$annual_limit_key];
												if ($cover_value=="Covered"){
													$cover_value = '<span class="tick"></span>';
												} else {
													$cover_value = '<span class="cross"></span>';
												}
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section"><div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section "><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">'.$ArrextrasOptionsArr[$annual_limit_key].'</div></div>';
												echo '<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">';
												$annual_limit_value = $annual_limit_value;
												if ($annual_limit_value=="Covered"){
													$annual_limit_value = '<table class="payment_img"><tr><td><img src="images/tick.png"></td></tr></table>';
												} else if($annual_limit_value=="Not Covered"){
													$annual_limit_value = '<table class="payment_img"><tr><td><img src="images/cross.png"></td></tr></table>';
												} else{
													$annual_limit_value = '<table class="annual_content"><tr><td>'.$cover_value.$annual_limit_value.'</td></tr></table>';
												}
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 annual_extra_headers_content bor_bottom">'.$annual_limit_value.'</div>';
												echo '</div></div>';
											}
										?>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 header_section extra_section service_limit_section">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 result_section">
										<?php
											foreach ($resultArr['funds'][1]['extras_cover']['service_limit'] as $service_limit_key => $service_limit_value) {
												$cover_value = $resultArr['funds'][1]['extras_cover']['service_limit'];
												if ($cover_value=="Covered")
													$cover_value = '<table class="img_table col-lg-2 col-md-2 col-sm-2 col-xs-2"><tr><td><img class="img_per_class" src="images/tick_small.png"/></td></tr></table>';
												else
													$cover_value = '<table class="img_table col-lg-2 col-md-2 col-sm-2 col-xs-2"><tr><td><img class="img_per_class" src="images/cross_small.png" /></td></tr></table>';
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section"><div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section "><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">'.$ArrextrasOptionsArr[$service_limit_key].'</div></div>';
												echo '<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">';
												$last_service_limit_value = end(array_keys($service_limit_value));
												$service_limit_table = "";
												$service_limit_table = $cover_value.'<table class="col-lg-8 col-md-8 col-sm-8 col-xs-8 service_content">';
												foreach ($service_limit_value as $service_limit_key => $service_limit_value) {
													$service_limit_table .= '<tr><td align="left">'.$service_limit_key.'</td><td align="center">'.$service_limit_value.'</td></tr>';
												}
												$service_limit_table .= '</table>';
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 service_extra_headers_content bor_bottom">'.$service_limit_table.'</div>';
												echo '</div></div>';
											}
										?>
										</div>
									</div>
								<?php } ?>
							</div>
						<?php } else { ?>
							<div class="col-xs-12 rslt_elements" data-attr="1" id="rslt_element_1">
								<div class="col-xs-12 result_section fixed_head">
									<div class="col-xs-12 rslt_section">
										<div class="col-xs-12 result_section fund_section">
										<?php
											$fund_provider_value = $fundDetailsArr['fund_provider'];
											$fund_provider_type = $fundDetailsArr['fund_type']; 
											$fund_policy_url = $fundDetailsArr['policy_url']; 
											echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_header"><div class="col-xs-1"></div><div class="col-xs-10"><h4 class="bank_name"><a target="_blank" href="'.$fund_policy_url.'">'.$fund_provider_value.'</a></h4><p class="fund_type">'.$fund_provider_type.'</p></div><div class="col-xs-1"></div></div>';
										?>
										</div>
									</div>
									<div class="col-xs-12 rslt_section">
										<div class="col-xs-6 header_section">
											<div class="col-xs-12 premium_headers premium head bor_bottom">Monthly premium<br>(after 25.657% rebate)</div>
										</div>
										<div class="col-xs-6 result_section fund_section">
										<?php
											$monthly_premium_value = $fundDetailsArr['monthly_premium'];
											echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 premium_headers bor_bottom"><h4 class="policy_price">'.$monthly_premium_value.'</h4></div>';
										?>
										</div>
									</div>
								</div>
								<div class="col-xs-12 result_section">	
									<div class="col-xs-12 rslt_section">
										<div class="col-xs-6 header_section">
											<div class="col-xs-12 policy_headers head bor_bottom">Policy name</div>
										</div>
										<div class="col-xs-6 result_section fund_section">
										<?php
											$policy_name_value = $fundDetailsArr['policy_name'];
											echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 policy_headers bor_bottom"><h4 class="policy_name">'.$policy_name_value.'</h4></div>';
										?>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
										<div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section">
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 visit_headers head bor_bottom">Excess per visit</div>
										</div>
										<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">
										<?php
											$per_visit_value = $fundDetailsArr['per_visit'];
											echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 visit_headers bor_bottom"><h4 class="visit_price">'.$per_visit_value.'</h4></div>';
										?>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section">
										<div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section">
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payment_headers head bor_bottom">Co-payment</div>
										</div>
										<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">
										<?php
											$co_payment_value = $fundDetailsArr['co_payment'];
											echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 payment_headers bor_bottom"><h4 class="payment">'.$co_payment_value.'</h4></div>';
										?>
										</div>
									</div>
								</div>
								<?php if ($type_cover=="Hospital Only" || $type_cover=="Hospital + Extras") { ?>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_header">HOSPITAL COVER</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 result_section">
										<?php 
											$ArrHospitalOptionsArr = array("pregnancy" =>"Pregnancy","fertility_treatment" =>"Fertility treatment (IVF)","heart_surgery" =>"Heart surgery (cardiac)","eye_surgery" =>"Eye surgery (cataract)","joints" =>"Joints (hip, knee, shoulder","renal_dialysis" =>"Renal dialysis (kidney)","weight_loss" =>"Weight loss (gastric)","sterilisation" =>"Sterilisation","plastic_surgery" =>"Plastic surgery","rehab_treatment" =>"Rehab treatment","psychatric" =>"Psychatric","palliative_care" =>"Palliative care");
											foreach ($resultArr['funds'][0]['hospital_cover'] as $hospital_cover_key => $hospital_cover_value) {
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section"><div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section "><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">'.$ArrHospitalOptionsArr[$hospital_cover_key].'</div></div>';
												echo '<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">';
												$cover_value = $hospital_cover_value;
												if ($cover_value=="Covered"){
													$cover_value = '<img src="images/tick.png">';
												} else if($cover_value=="Not Covered" || $cover_value=="n&#47a"){

													$cover_value = '<img src="images/cross.png">';
												} else{
													$cover_value = $cover_value;
												}
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers bor_bottom"><h4 class="payment">'.$cover_value.'</h4></div>';
												echo '</div></div>';
											}
										?>
									</div>
								<?php } 
								if ($type_cover=="Extras Only" || $type_cover=="Hospital + Extras") { ?>
									<div class="col-xs-12 section_header extra_header_main">
										<div class="col-xs-12 extra_header">EXTRAS COVER</div>
										<div class="col-xs-offset-3 col-xs-6">
											<select name="select_div" class="select_div result_header_btn filter_btn_1 more_btn">
												<!-- <option selected="selected" value="Covered">Covered</option> -->
												<option value="Annual limit">Annual limit</option>
												<option value="Per service limit">Per service limit</option>
											</select>
										</div>
									</div>
									<!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 header_section extra_section covered_section">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 result_section">
											<?php 
												$ArrextrasOptionsArr = array("dental_cleaning" =>"Dental (cleaning, fillings)","dental_major" =>"Dental major (crowns)","dental_root" =>"Dental (root canal)","dental_braces" =>"Dental (braces)","optical" =>"Optical","physio" =>"Physio","chiro" =>"Chiro","massages" =>"Massages","acupunture" =>"Acupunture","hearing_aids" =>"Hearing aids","blood_glucose_monitors" =>"Blood glucose monitors","pharmaceutical" =>"Pharmaceutical (non PBS)","naturopathy" =>"Naturopathy","podiatry" =>"Podiatry (feet)","psychology" =>"Psychology");
												foreach ($resultArr['funds'][0]['extras_cover']['covered'] as $extras_cover_key => $extras_cover_value) {
													echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section"><div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section "><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">'.$ArrextrasOptionsArr[$extras_cover_key].'</div></div>';
													echo '<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">';
													$extras_cover_value = $extras_cover_value;
													if ($extras_cover_value=="Covered"){
														$extras_cover_value = '<h4 class="payment_img"><img src="images/tick.png"></h4>';
													} else if($extras_cover_value=="Not Covered" || $extras_cover_value=="n&#47a"){
														$extras_cover_value = '<h4 class="payment_img"><img src="images/cross.png"></h4>';
													} else{
														$extras_cover_value = '<h4 class="cover_payment_text">'.$extras_cover_value.'</h4>';
													}
													echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cover_extra_headers_content bor_bottom">'.$extras_cover_value.'</div>';
													echo '</div></div>';
												}
											?>
										</div>
									</div> -->
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 header_section extra_section annual_limit_section">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 result_section">
										<?php
											foreach ($resultArr['funds'][0]['extras_cover']['annual_limit'] as $annual_limit_key => $annual_limit_value) {
												$cover_value=$resultArr['funds'][0]['extras_cover']['covered'][$annual_limit_key];
												if ($cover_value=="Covered"){
													$cover_value = '<span class="tick"></span>';
												} else {
													$cover_value = '<span class="cross"></span>';
												}
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section"><div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section "><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">'.$ArrextrasOptionsArr[$annual_limit_key].'</div></div>';
												echo '<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">';
												$annual_limit_value = $annual_limit_value;
												if ($annual_limit_value=="Covered"){
													$annual_limit_value = '<table class="payment_img"><tr><td><img src="images/tick.png"></td></tr></table>';
												} else if($annual_limit_value=="Not Covered"){
													$annual_limit_value = '<table class="payment_img"><tr><td><img src="images/cross.png"></td></tr></table>';
												} else{
													$annual_limit_value = '<table class="annual_content"><tr><td>'.$cover_value.$annual_limit_value.'</td></tr></table>';
												}
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 annual_extra_headers_content bor_bottom">'.$annual_limit_value.'</div>';
												echo '</div></div>';
											}
										?>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 header_section extra_section service_limit_section">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 result_section">
										<?php
											foreach ($resultArr['funds'][0]['extras_cover']['service_limit'] as $service_limit_key => $service_limit_value) {
												$cover_value = $resultArr['funds'][0]['extras_cover']['service_limit'];
												if ($cover_value=="Covered")
													$cover_value = '<table class="img_table col-lg-2 col-md-2 col-sm-2 col-xs-2"><tr><td><img class="img_per_class" src="images/tick_small.png"/></td></tr></table>';
												else
													$cover_value = '<table class="img_table col-lg-2 col-md-2 col-sm-2 col-xs-2"><tr><td><img class="img_per_class" src="images/cross_small.png" /></td></tr></table>';
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rslt_section"><div class="col-lg-4 col-md-3 col-sm-4 col-xs-6 header_section "><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 section_headers head bor_bottom">'.$ArrextrasOptionsArr[$service_limit_key].'</div></div>';
												echo '<div class="col-lg-8 col-md-9 col-sm-8 col-xs-6 result_section fund_section">';
												$last_service_limit_value = end(array_keys($service_limit_value));
												$service_limit_table = "";
												$service_limit_table = $cover_value.'<table class="col-lg-8 col-md-8 col-sm-8 col-xs-8 service_content">';
												foreach ($service_limit_value as $service_limit_key => $service_limit_value) {
													$service_limit_table .= '<tr><td align="left">'.$service_limit_key.'</td><td align="center">'.$service_limit_value.'</td></tr>';
												}
												$service_limit_table .= '</table>';
												echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 service_extra_headers_content bor_bottom">'.$service_limit_table.'</div>';
												echo '</div></div>';
											}
										?>
										</div>
									</div>
								<?php } ?>
							</div>
						<?php } ?>
					</div>
				<?php } ?>
			</div>
		</div>
	</form>
</div>
<!-- <input type="text" id="os_type" name="os_type" value=''> -->
<script type="text/javascript">
	$(document).ready(function() {
		// $('.annual_limit_section').hide();
		$('.service_limit_section').hide();
		$('#rslt_element_2').hide();
		$('#rslt_element_3').hide();

		$( ".btn_header" ).on( "click", function() {
	    	var btn_name = $(this).find('.result_header_btn').text();
	    	$('.result_header_btn').removeClass('active');
	    	$(this).find('.result_header_btn').addClass('active');
	    	if (btn_name=='Covered') {
	    		$('.covered_section').show();
	    		$('.annual_limit_section').hide();
				$('.service_limit_section').hide();
	    	} else if(btn_name=='Annual limit') {
	    		$('.covered_section').hide();
	    		$('.annual_limit_section').show();
				$('.service_limit_section').hide();
	    	} else {
	    		$('.covered_section').hide();
	    		$('.annual_limit_section').hide();
				$('.service_limit_section').show();
	    	} 
		});

		$( ".right_arrow" ).on( "click", function() {
	    	var rslt_elements = parseInt($(this).closest('.rslt_elements').attr('data-attr'));
	    	var selected_option = $(this).closest('.rslt_elements').find('.select_div option:selected').val();
	    	rslt_elements = rslt_elements+1;
	    	$('.rslt_elements').hide();
	    	rslt_elements = '#rslt_element_'+rslt_elements;
	    	$(rslt_elements).show();
	    	$('.extra_section').hide();
	    	if (selected_option=='Covered') {
	    		$('.covered_section').show();
	    	} else if(selected_option=='Annual limit') {
	    		$('.annual_limit_section').show();
	    	} else {
				$('.service_limit_section').show();
	    	}
	    	var option = "select option[value='"+selected_option+"']";
	    	$('.select_div option').removeAttr('selected');
	    	$(option).prop('selected','selected'); 
	    	// console.log(option);
		});
		$( ".left_arrow" ).on( "click", function() {
	    	var rslt_elements = parseInt($(this).closest('.rslt_elements').attr('data-attr'));
	    	var selected_option = $(this).closest('.rslt_elements').find('.select_div option:selected').val();
	    	rslt_elements = rslt_elements-1;
	    	$('.rslt_elements').hide();
	    	rslt_elements = '#rslt_element_'+rslt_elements;
	    	$(rslt_elements).show();
	    	$('.extra_section').hide();
	    	if (selected_option=='Covered') {
	    		$('.covered_section').show();
	    	} else if(selected_option=='Annual limit') {
	    		$('.annual_limit_section').show();
	    	} else {
				$('.service_limit_section').show();
	    	}
	    	var option = "select option[value='"+selected_option+"']";
	    	$('.select_div option').removeAttr('selected');
	    	$(option).prop('selected','selected');
	    	// console.log(option);
		});

		$( ".pagination li a:not('.current')" ).on( "click", function() {
			var type = $(this).text();
			if (type=="Next") {
				var current_page_no = parseInt($('.pagination li .current').text());
				var page_no = current_page_no + 1;
			} else if (type=="Prev") {
				var current_page_no = parseInt($('.pagination li .current').text());
				var page_no = current_page_no - 1;
			} else {
				var page_no = parseInt($(this).text());
			}
			if (page_no>0) {
				page_no = page_no;
				
			} else {
				page_no = 0;
			}
			var request = $('#filter_selected').val();
			var requestJson = {};
			requestJson = JSON.parse(request);
			var order = $('.result_filter_btn.active').attr('id');
			var search = $('#fund_search').val();
			requestJson["order"] = order;
			requestJson["page_no"] = page_no;
			requestJson["fund_name"] = search;
			$('#filter_selected').val(JSON.stringify(requestJson));
			$('#result_form').submit();
		});
		$( ".result_filter_btn" ).on( "click", function() {
			$('.result_filter_btn').removeClass('active');
			$(this).addClass('active');
			var order = $(this).attr('id');
			var request = $('#filter_selected').val();
			var requestJson = {};
			requestJson = JSON.parse(request);
			var page_no = parseInt($('.pagination li .current').text());
			var search = $('#fund_search').val();
			requestJson["order"] = order;
			requestJson["page_no"] = page_no;
			requestJson["fund_name"] = search;
			// console.log(requestJson);
			$('#filter_selected').val(JSON.stringify(requestJson));
			$('#result_form').submit();
		});
		$(".fund_search").blur(function(){
		    var order = $('.result_filter_btn.active').attr('id');
			var request = $('#filter_selected').val();
			var requestJson = {};
			requestJson = JSON.parse(request);
			var page_no = parseInt($('.pagination li .current').text());
			var search = $(this).val();
			requestJson["order"] = order;
			requestJson["page_no"] = 0;
			requestJson["fund_name"] = search;
			// console.log(requestJson);
			$('#filter_selected').val(JSON.stringify(requestJson));
			$('#result_form').submit();
		});
		$(".fund_search").keyup(function(e){ 
		    var code = e.which; // recommended to use e.which, it's normalized across browsers
		    if(code==13)e.preventDefault();
		    if(code==32||code==13||code==188||code==186){
		        var order = $('.result_filter_btn.active').attr('id');
				var request = $('#filter_selected').val();
				var requestJson = {};
				requestJson = JSON.parse(request);
				var page_no = parseInt($('.pagination li .current').text());
				var search = $(this).val();
				requestJson["order"] = order;
				requestJson["page_no"] = 0;
				requestJson["fund_name"] = search;
				// console.log(requestJson);
				$('#filter_selected').val(JSON.stringify(requestJson));
				$('#result_form').submit();
		    } // missing closing if brace
		});

		$('.select_div').on('change', function (e) {
		    // var optionSelected = $("option:selected", this);
		    var valueSelected = this.value;
		    var parent = $(this).closest('.rslt_elements').attr('id');
		    // console.log(parent);
		    if (valueSelected=='Covered') {
	    		$("#"+parent).find('.covered_section').show();
	    		$("#"+parent).find('.annual_limit_section').hide();
				$("#"+parent).find('.service_limit_section').hide();
	    	} else if(valueSelected=='Annual limit') {
	    		$("#"+parent).find('.covered_section').hide();
	    		$("#"+parent).find('.annual_limit_section').show();
				$("#"+parent).find('.service_limit_section').hide();
	    	} else {
	    		$("#"+parent).find('.covered_section').hide();
	    		$("#"+parent).find('.annual_limit_section').hide();
				$("#"+parent).find('.service_limit_section').show();
	    	} 
		});

		//store the element
		var $cache = $('.fixed_head');

		//store the initial position of the element
		var vTop = $cache.offset().top - parseFloat($cache.css('margin-top').replace(/auto/, 0));
		$(window).scroll(function (event) {
		    // what the y position of the scroll is
		    var y = $(this).scrollTop();
		    // var width = $( document ).width();
		    // console.log(width);
		    // whether that's below the form
		    if (y > vTop) {
		    	$cache.addClass('stuck');
		    } else {
		      // otherwise remove it
		      $cache.removeClass('stuck');
		    }
		});		
	});
</script>
<?php
require_once("includes/footer.php");
?>		