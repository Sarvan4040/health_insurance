<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" href="images/favicon.png" type="image/png" sizes="16x16">
		<title>Health Care</title>

		<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="css/jquery.nstSlider.css" rel="stylesheet" type="text/css">
		<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<link href="css/style.css" rel="stylesheet" type="text/css">

		<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
		<script type="text/javascript" src="js/jquery.nstSlider.min.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
    </head>
    <body class="homepage">
		<div class="col-md-12 col-xs-12 col-sm-12 main_container">
   