		</div>
		<script type="text/javascript">
			$(document).ready(function() {
				$('.nstSlider').nstSlider({
					"rounding": {
				        '10' : '100'
				    },
					"left_grip_selector": ".leftGrip",
					"right_grip_selector": ".rightGrip",
					"value_bar_selector": ".bar",
					"value_changed_callback": function(cause, leftValue, rightValue) {
						var $container = $(this).parent();
						$container.find('.leftLabel').text('$'+leftValue);
						$container.find('.rightLabel').text('$'+rightValue);
					},
					"highlight": {
						"grip_class": "gripHighlighted",
						"panel_selector": ".highlightPanel"
					}
				});
			});
		</script>
   </body>
</html>