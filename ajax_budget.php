<?php
include("includes/configure.php");
require_once('api.php');
// print_r($_REQUEST);
if(isset($_POST["filter_options"])) {
  $lookingFor = $_POST["filter_options"]['looking_for'];
  $needCover = $_POST["filter_options"]['needs_class'];
  // echo $needCover;
  $typeCover = $_POST["filter_options"]['type_class'];
  $state = $_POST["filter_options"]['state_class'];
  if(isset($_POST["filter_options"]["popup_options"])) {
    $ArraypopupOptions = $_POST["filter_options"]["popup_options"];
  } else {
    $ArraypopupOptions = "empty";
  }
  //Toget Array of fields and value in covered
  $Arrlookingfor=array("Planning to have kids"=>"WS_Hos_PregnancyAndBirthRelatedServices_Covered","Basic dental"=>"PS_DentalGeneral_Covered","Major dental"=>"PS_DentalMajor_Covered");
  $ArrlookSearch=array("Planning to have kids"=>":planningkids","Basic dental"=>":basicdental","Major dental"=>":majordental");
  $Arrlookvalues=array("Planning to have kids"=>"Covered","Basic dental"=>"1","Major dental"=>"1");
  
  if ($lookingFor!="Best cover" && $lookingFor!="More options") {
    $lookingForFields=$Arrlookingfor[$lookingFor];
    $lookingForSeach=$ArrlookSearch[$lookingFor];
    $lookingForvalue=$Arrlookvalues[$lookingFor];
  }
  
  $ArrHospitalExtrasDetails = array(
      "Pregnancy"=>" AND WS_Hos_PregnancyAndBirthRelatedServices_Covered='Covered'",
      "Fertility treatment (IVF)"=>" AND WS_Hos_AssistedReproductiveServices_Covered ='Covered'",
      "Heart surgery (cardiac)"=>" AND WS_Hos_CardiacAndRelatedServices_Covered='Covered'",
      "Eye surgery (cataract)"=>" AND WS_Hos_CataractAndEyeLensProcedures_Covered='Covered'",
      "Joints (hip, knee, shoulder)"=>" AND WS_Hos_JointReplacement_HipKneeShoulderAndElbow_Covered='Covered'",
      "Renal dialysis (kidney)"=>" AND WS_Hos_DialysisForChronicRenalFailure_Covered='Covered'",
      "Weight Loss (gastric)"=>" AND WS_Hos_GastricBandingAndRelatedServices_Covered='Covered'",
      "Sterilisation"=>" AND WS_Hos_Sterilisation_Covered='Covered'",
      "Plastic surgery"=>" AND WS_Hos_NonCosmeticPlasticSurgery_Covered='Covered'",
      "Rehab treatment"=>"  AND WS_Hos_Rehabilitation_Covered='Covered'",
      "Psychatric"=>" AND WS_Hos_PsychiatricServices_Covered='Covered'",
      "Palliative"=>" AND WS_Hos_PalliativeCare_Covered='Covered'",
      "Dental (cleaning, filling)"=>"AND PS_DentalGeneral_Covered='1'",
      "Dental major"=>" AND PS_DentalMajor_Covered='1'",
      "Dental (root canal)"=>" AND PS_DentalEndodontic_Covered='1'",
      "Dental (braces)"=>" AND PS_DentalOrthodontic_Covered='1'",
      "Optical"=>" AND PS_Optical_Covered='1'",
      "Physio"=>" AND PS_Physiotherapy_Covered='1'",
      "Chiro"=>" AND PS_Chiropractic_Covered='1'",
      "Massage"=>" AND PS_RemedialMassage_Covered='1'",
      "Hearing aids"=>" AND PS_HearingAids_Covered='1'",
      "Actupuncture"=>" AND PS_Acupuncture_Covered='1'",
      "Blood Glucose Monitors"=>" AND  PS_BloodGlucoseMonitors_Covered='1'",
      "Pharmaceutical (non PBS)"=>" AND PS_NonPbsPharma_Covered='1'",
      "Naturopathy"=>" AND PS_Naturopathy_Covered='1'",
      "Podiatry (feet)"=>" AND PS_Podiatry_Covered='1'",
      "Psychology"=>" AND PS_Psychology_Covered='1'"
      );

  $condtionArr   = array();
  $QryCondition="";

   if (($lookingFor=='More options') && ($ArraypopupOptions!='empty')) {    
    if ($typeCover=="Combined") {
      foreach($ArraypopupOptions as $key=>$pop_options){
        $QryCondition.=$ArrHospitalExtrasDetails[$pop_options];
      }
    }
    if ($typeCover=="General") {
      foreach($ArraypopupOptions as $key=>$pop_options){
        $QryCondition.=$ArrHospitalExtrasDetails[$pop_options];
      }
    }
    if ($typeCover=="Hospital") {
      foreach($ArraypopupOptions as $key=>$pop_options){
        $QryCondition.=$ArrHospitalExtrasDetails[$pop_options];
      }
    }
  }
  
  if ($lookingFor!="Best cover" && $lookingFor!="More options") {
    $QryCondition.=" AND ".$lookingForFields."=".$lookingForSeach."";
    $condtionArr[$lookingForSeach]   = $lookingForvalue;
  }

  if ($lookingFor=="Best cover") {
    
    $QryCondition.=" AND WS_Hos_PregnancyAndBirthRelatedServices_Covered='Covered' AND WS_Hos_AssistedReproductiveServices_Covered ='Covered' AND WS_Hos_CardiacAndRelatedServices_Covered='Covered' AND WS_Hos_CataractAndEyeLensProcedures_Covered='Covered' AND WS_Hos_JointReplacement_HipKneeShoulderAndElbow_Covered='Covered' AND WS_Hos_DialysisForChronicRenalFailure_Covered='Covered' AND WS_Hos_GastricBandingAndRelatedServices_Covered='Covered' AND WS_Hos_NonCosmeticPlasticSurgery_Covered='Covered' AND WS_Hos_Sterilisation_Covered='Covered' AND WS_Hos_Rehabilitation_Covered='Covered' AND WS_Hos_PsychiatricServices_Covered='Covered' AND WS_Hos_PalliativeCare_Covered='Covered' AND PS_DentalGeneral_Covered='1' AND PS_DentalMajor_Covered='1' AND PS_DentalEndodontic_Covered='1' AND PS_DentalOrthodontic_Covered='1' AND PS_Optical_Covered='1' AND PS_Physiotherapy_Covered='1' AND PS_Chiropractic_Covered='1' AND PS_RemedialMassage_Covered='1' AND PS_Acupuncture_Covered='1' AND PS_HearingAids_Covered='1' AND  PS_BloodGlucoseMonitors_Covered='1' AND PS_NonPbsPharma_Covered='1' AND PS_Naturopathy_Covered='1' AND PS_Podiatry_Covered='1' AND PS_Psychology_Covered='1'";
  }

  if ($needCover!="") {
    // $needCoverArr = array();
    if (strpos($needCover, '-') !== false) {
      $needCoverArr = explode('-', $needCover);
      $QryCondition.=" AND (WS_Category=:WS_Category";
      $condtionArr[":WS_Category"]   = $needCoverArr[0];
      $QryCondition.=" OR WS_Category=:WS_Category_1)";
      $condtionArr[":WS_Category_1"]   = $needCoverArr[1];
    } else{
      $QryCondition.=" AND WS_Category=:WS_Category";
      $condtionArr[":WS_Category"]   = $needCover;
    }
  }
  if ($typeCover!="") {
      $QryCondition.=" AND WS_CoverType=:WS_CoverType";
      $condtionArr[":WS_CoverType"]   = $typeCover;
  }
  if ($state!="") {
      $QryCondition.=" AND WS_State=:WS_State";
      $condtionArr[":WS_State"]   = $state;
  }

  $price_listing="";
  $cur_min = "";
  $cur_max = "";
  $selPremiumQry = "SELECT ceil(MAX(WS_MonthlyPremium_StdLessThanAge65Rebate)) as max_value, floor(MIN(WS_MonthlyPremium_StdLessThanAge65Rebate)) as min_value from tbl_policies where insurance_id<>''".$QryCondition;
  $prepgetPremiumQry = $dbconn->prepare($selPremiumQry);
  $prepgetPremiumQry->execute($condtionArr);
  $rowCount = $prepgetPremiumQry->rowCount();
  if($rowCount>0) {
    $getResRow = $prepgetPremiumQry->fetchAll(PDO::FETCH_ASSOC);
    foreach($getResRow as $getResPremium) {
      $minvalue    =  $getResPremium["min_value"];
      $maxvalue    =  $getResPremium["max_value"];

      $Arraybudget["min"]=$minvalue;
      $Arraybudget["max"]=$maxvalue;
      //print_r($Arraybudget);
    }
    if (($minvalue=="" || $minvalue==null) && ($maxvalue=="" || $maxvalue==null)) {
      $cur_min = 0;
      $cur_max = 500;
    } else {
      $cur_min = $minvalue;
      $cur_max = $maxvalue;
    }
  }
}
?>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ques_1_ans price_limit" id="price_limit">
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 price_min">$<?php echo $cur_min;?></div>
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 price_max text-right">$<?php echo $cur_max;?></div>
  <div class="nstSlider" data-range_min="<?php echo $cur_min;?>" data-range_max="<?php echo $cur_max;?>" data-cur_min="<?php echo $cur_min;?>"  data-cur_max="<?php echo $cur_max;?>">
    <div class="highlightPanel"></div>
    <div class="bar"></div>
    <div class="leftGrip"></div>
    <div class="rightGrip"></div>
  </div>
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 price">Choose between &nbsp;<span class="leftLabel"><?php echo $cur_min;?></span>&nbsp;and&nbsp;<span class="rightLabel"><?php echo $cur_max;?></span>&nbsp;&nbsp;&nbsp;<span class="learn_more" data-toggle="modal" data-target="#tips" data-attr="tips">learn more</span></div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $('.nstSlider').nstSlider({
      "rounding": {
        '10' : '100'
      },
      "left_grip_selector": ".leftGrip",
      "right_grip_selector": ".rightGrip",
      "value_bar_selector": ".bar",
      "value_changed_callback": function(cause, leftValue, rightValue) {
        var $container = $(this).parent();
        $container.find('.leftLabel').text('$'+leftValue);
        $container.find('.rightLabel').text('$'+rightValue);
      },
      "highlight": {
        "grip_class": "gripHighlighted",
        "panel_selector": ".highlightPanel"
      }
    });
  });
</script>