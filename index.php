<?php
include('includes/configure.php');
require_once('api.php');

$getResultInitialHomePage = $returnHealthCareObject->funSearchDisplayDetails();
$getResultInitial = json_decode($getResultInitialHomePage,true);

require_once("includes/header.php");
?>
<div id="main_container_home">
	<form name="filter_form" id="filter_form" action="result.php" method="post">
		<!--Popup Modal -->
		<div id="popup_options" class="modal fade" role="dialog">
			<div class="modal-dialog popup_modal modal-lg col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="modal-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 modal-body popup_container">
						<div class="hospital_cover">
							<h2 class="col-lg-12 col-md-12 col-sm-12 col-xs-12 popup_header text-center">hospital cover</h2>
							<?php foreach ($getResultInitial['hospital_cover'] as $key => $hospital_cover_input) {
								echo '<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 popup_options"><button type="button" class="popup_btn popup_filter_btn_1" data-attr="'.$hospital_cover_input.'">'.$hospital_cover_input.'</button></div>';
							} ?>
						</div>
						<div class="extras_cover">
							<h2 class="col-lg-12 col-md-12 col-sm-12 col-xs-12 popup_header text-center">extras cover</h2>
							<?php foreach ($getResultInitial['extras_cover'] as $key => $extras_cover_input) {
								echo '<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 popup_options"><button type="button" class="popup_btn popup_filter_btn_2" data-attr="'.$extras_cover_input.'">'.$extras_cover_input.'</button></div>';
							} ?>
						</div>
						<div class="col-lg-offset-4 col-lg-4 col-md-offset-4 col-md-4 col-sm-offset-4 col-sm-4 col-xs-12 popup_btn_container"><button type="button" class="select_btn" data-dismiss="modal">select</button></div>
					</div>
				</div>
			</div>
		</div>

		<!-- Tips Modal -->
		<div id="tips" class="modal fade" role="dialog">
			<div class="tips modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<h2 class="text-center learn_more_pop">You cannot find any other products in the market cheaper or dearer than that of the above specified amount. Based on your selection, the only products available in the markets are the products available between this amounts.are the products between the above specified amounts.</h2>
				</div>
			</div>
		</div>

		<input type="hidden" id="filter_selected" name="filter_selected" value="">
		<!-- Filters  -->
		<div class="container-fluid filter_container">
			<div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8 col-sm-12 col-xs-12 home_container">
				<div class="text-center">
					<h2 class="home_header">All 36 Health Funds | 30,000+ policies</h2>
				</div>
				<div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 col-xs-offset-2 col-xs-8 text-center image_container">
					<img class="img-responsive" src="images/logo.png">
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 filters_container_ques_1">
					<div class="col-lg-4 col-md-4 col-sm-3 col-xs-12 ques">
						<h2 class="ques_1">what are you looking for?</h2>
					</div>
					<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12 selection_container">
						<?php foreach ($getResultInitial['looking_for'] as $key => $looking_for_input) {
							$looking_class="";
							if ($looking_for_input=='Planning to have kids')
								$looking_class = 'active'; 
							else 
								$looking_class = '';
							echo '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 ques_1_ans"><button type="button" class="filter_btn filter_btn_1 looking_for '.$looking_class.'" data-attr="'.$looking_for_input.'">'.$looking_for_input.'</button></div>';
						} ?>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 ques_1_ans"><button type="button" class="filter_btn filter_btn_1 looking_for more_btn" data-toggle="modal" data-target="#popup_options" data-attr="More options">More options</button></div>
					</div>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 filters_container_ques_2">
					<div class="col-lg-4 col-md-4 col-sm-3 col-xs-12 ques">
						<h2 class="ques_2">who needs cover?</h2>
					</div>
					<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12 selection_container">
						<?php 
						$arr = $getResultInitial['needs_cover'];
						$lastvalue = end($getResultInitial['needs_cover']);
						$lastkey = key($getResultInitial['needs_cover']);
						$ResArr = array($lastkey=>$lastvalue);
						array_pop($getResultInitial['needs_cover']);
						$ResArr = array_merge($ResArr,$arr);
						array_pop($ResArr);
						foreach ($ResArr as $key => $needs_cover_input) {
							$needs_class="";
							if ($needs_cover_input=='Couple')
								$needs_class = 'active'; 
							else 
								$needs_class = '';
							if ($needs_cover_input=="Family" || $needs_cover_input=="Sole Parent") {
								echo '<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 ques_2_ans"><button type="button" class="filter_btn filter_btn_2 needs_class '.$needs_class.'" data-attr="'.$needs_cover_input.' - '.$needs_cover_input.' Plus">'.$needs_cover_input.'</button></div>';
							} else {
								echo '<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 ques_2_ans"><button type="button" class="filter_btn filter_btn_2 needs_class '.$needs_class.'" data-attr="'.$needs_cover_input.'">'.$needs_cover_input.'</button></div>';
							}
						} ?>
					</div>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 filters_container_ques_3">
					<div class="col-lg-4 col-md-4 col-sm-3 col-xs-12 ques">
						<h2 class="ques_3">type of cover</h2>
					</div>
					<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12 selection_container">
						<?php foreach ($getResultInitial["type_of_cover"] as $key => $type_of_cover) {
						    $ArrCoverType=array("Hospital + Extras"=>"Combined","Hospital Only"=>"Hospital","Extras Only"=>"General");
						    $typeofcover=$ArrCoverType[$type_of_cover];
							$type_class="";
							if ($typeofcover=='Combined')
								$type_class = 'active'; 
							else 
								$type_class = '';
							echo '<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 ques_3_ans"><button type="button" class="filter_btn filter_btn_3 type_class '.$type_class.'" data-attr="'.$typeofcover.'">'.$type_of_cover.'</button></div>';
							
						} ?>
					</div>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 filters_container_ques_4">
					<div class="col-lg-4 col-md-4 col-sm-3 col-xs-12 ques">
						<h2 class="ques_4">your state</h2>
					</div>
					<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12 selection_container">
						<?php foreach ($getResultInitial['state'] as $key => $state_input) {
							$state_class="";
							if ($state_input=='QLD')
								$state_class = 'active'; 
							else 
								$state_class = '';
							echo '<div class="col-lg-1 col-md-1 col-sm-1 col-xs-3 ques_4_ans"><button type="button" class="filter_btn filter_btn_4 state_class '.$state_class.'" data-attr="'.$state_input.'">'.$state_input.'</button></div>';
						} ?>
					</div>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 filters_container_ques_5">
					<div class="col-lg-4 col-md-4 col-sm-3 col-xs-12 ques">
						<h2 class="ques_5">what is your monthly budget?</h2>
					</div>
					<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12 selection_container" id="selection_container">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ques_1_ans price_limit" id="price_limit">
							<?php 
								$cur_max = $getResultInitial['budget']['max'];
								$cur_min = $getResultInitial['budget']['min'];

								echo '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 price_min">$'.$cur_min.'</div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 price_max text-right">$'.$cur_max.'</div><div class="nstSlider" data-range_min="'.$cur_min.'" data-range_max="'.$cur_max.'" data-cur_min="'.$cur_min.'"  data-cur_max="'.$cur_max.'">';
							?>
							    <div class="bar"></div>
							    <div class="leftGrip"></div>
							    <div class="rightGrip"></div>
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 price">Choose between &nbsp;<span class="leftLabel"></span>&nbsp;and&nbsp;<span class="rightLabel"></span>&nbsp;&nbsp;&nbsp;<span class="learn_more" data-toggle="modal" data-target="#tips" data-attr="tips">learn more</span></div>
						</div>
					</div>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 filters_container_btn">
					<div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 col-xs-12">
						<div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 col-xs-12">
							<input type="button" name="search" value="search" class="search_btn" id="submit_btn" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>

<script type="text/javascript">
	$(document).ready(function() {

		function GetSelectedElements() {
			var filter_options = $('.filter_btn.active').map(function(){
			    return $(this).data('attr');
			}).get();

			var filter_class = $('.filter_btn.active').map(function(){
			    var classes = $(this).attr('class');
			    var class_result = classes.split(" ");
			    return class_result[2];
			}).get();

			var min = $('.leftLabel').text();
			min = min.slice(1,  min.length);
			filter_class.push("min");
			filter_options.push(min);
			
			var max = $('.rightLabel').text();
			max = max.slice(1,  max.length);
			filter_class.push("max");
			filter_options.push(max);
			
			var obj = {};
			for(var i=0,len=filter_class.length; i < len ;i++) {
			 	obj[filter_class[i]] = filter_options[i];
			}
			return obj;
		}

		function GetPopupElements() {
			var popup_options = $('.popup_btn.active').map(function(){
			    return $(this).data('attr');
			}).get();

			if (popup_options!="") {
				
				var obj = {};
				for(var i=0,len=popup_options.length; i < len ;i++) {
				 	obj[i] = popup_options[i];
				}
				return obj;
			} else {
				popup_options = "empty";
				return popup_options;
			}
		}

	    $( ".filter_btn , .select_btn" ).on( "click", function() {
	    	var filter_btn_text = $(this).text();
			if (filter_btn_text=="select") {
				var selectedArr = GetSelectedElements();
		 		selectedArr['popup_options'] = GetPopupElements();
			} else {
				var filter_btn_class = $(this).attr('class');
				var result = filter_btn_class.split(" ");
		 		$('.'+result[1]).removeClass('active');
		 		$(this).addClass('active');

		 		var selectedArr = GetSelectedElements();
		 		selectedArr['popup_options'] = GetPopupElements();
			}

			$("#selection_container").load('ajax_budget.php',{'filter_options':selectedArr}, function() {});
		});

		$( ".popup_btn" ).on( "click", function() {
			var pop_class = $(this).hasClass('active');
			if (pop_class==true) {
				$(this).removeClass('active');
			} else {
	 			$(this).addClass('active');
	 		}
		});

		$( ".search_btn" ).on( "click", function() {

			var selectedArr = JSON.parse(JSON.stringify(GetSelectedElements()));
		 	var popup_options = JSON.parse(JSON.stringify(GetPopupElements()));

		 	if (selectedArr['looking_for']=="More options") {
		 		selectedArr.popup_options = popup_options;
		 	} else {
		 		selectedArr.popup_options = "empty";
		 	}

			$('#filter_selected').val(JSON.stringify(selectedArr));

			$('#filter_form').submit();
		});

		$( ".more_btn" ).on( "click", function() {
	 		var selectedArr = GetSelectedElements();
			if(selectedArr['type_class']=='General'){
				$('.hospital_cover').hide();
				$('.hospital_cover .popup_btn').removeClass('active');
				$('.extras_cover').show();
			} else if(selectedArr['type_class']=='Hospital'){
				$('.extras_cover').hide();
				$('.extras_cover .popup_btn').removeClass('active');
				$('.hospital_cover').show();
			} else {
				$('.extras_cover').show();
				$('.hospital_cover').show();
			}
		});

	});
</script>
<?php
require_once("includes/footer.php");
?>		